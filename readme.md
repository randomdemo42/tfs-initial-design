Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Note that JsonCpp is licensed separately (files under the files jsoncpp), details can be found inside the relevant files but they are licensed under either Public Domain or MIT, or a combination therein.
 As far as I'm concerned, I'm licensing the JsonCpp library-source-code (and only that) under whichever is most permissive I distribute and is available in their region.

Project still needs a real name.

Probably need some formal decisions on design/documentation process.

2 things are going to be confirmed from the get-go:
1. it's a twin-stick shooter that involves fighter/pvp/ssb like game modes.
2. it involves online play.

I'm thinking initially we should probably focus on getting some of the basics of the mechanics sorted out though, getting it feel "nice" to move, and to shoot on a very basic level enough to use as a base I suppose.
