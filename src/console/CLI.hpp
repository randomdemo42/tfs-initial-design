/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "../definitions.hpp"
#include <vector>
#include <list>
#include <algorithm>
#include <utility>
#include <SFML/System/String.hpp>
#include <SFML/Window/Event.hpp>
#include <map>
#include <array>
#include <functional>
#include <SFML/Graphics/Color.hpp>
#include "../event/EventListener.hpp"
namespace __GAME_NAMESPACE__
{
	/*
	Does not alert to the failure of a command by throwing an error,
	or with any return values but rather only by adding a message to the end of the history.
	In fact almost all feedback given by this class is simply via the history.
	Because it is purely to be a human interface?


	NOTE: example on input distinguishing input in the history is to be preceded by a >. all outputs should instead precede with a space when logged normally for constrast/reference.

	NOTE: there is no cursor as of yet. TODO: add cursor. probably optimise history to use vectors and use index instead of an iterator to track position, etc.
	*/

	//invariants: has a log which contains all input and output, an input history, and a 'current' input line. these invariants are for i/o purposes.
	class CLI
	{
	public:
		enum LogEntryType
		{
			Normal,
			Input,
			Error
		};
        //should change this to deque probably
		typedef std::vector< std::pair<sf::String, LogEntryType> > Log;
		typedef std::map<sf::String, std::function<void(sf::String const &)> > CommandSet;

		static const CommandSet EMPTY_COMMAND_SET;
	private:
		const CommandSet* commands;
		//NOTE: one  restriction of this class is that we need to continously track which item in the history container is being "focused" on, and have the items ordered in terms of that focu.
		//The current solution for this uses iterators and as such requires that updating the container (insert/remove) never invalidates the iterators, as such the ItemsContainer type must, currently be a list.
		//a potentionally more efficient solution may be to use vectors and size-type indexes instead of iterators, as we can ensure that accuracy is maintained whenever updates occur, but this is an optimisation that can occur later.
		typedef std::vector<sf::String> History;

		Log log; //only edit internally, but access freely externally?
		History inputHistory;
		History::size_type inputHistoryPosition; //the current position in the input history.

        sf::String tempLastInput;
    protected:
        sf::String input; //the line for the current input.
    private:
		void addLogEntry(const sf::String& msg, LogEntryType type = LogEntryType::Normal);
    protected:
		/*
		pre: none.
		post: the inputHistoryPosition will be moved one closer to the beginning and the content at that new position will be set to the current input
		note: if the initial inputHistoryPosition is at the beginning of the inputHistory container, the position will be set to one position past the end and the current tempLastInput will be used as the new current input.
		in all other cases if the data in the container at the current inputHistoryPosition does not match the current input value, the current value will be stored in the temporary input.
		*/
		void ascendHistory();

		/*
		pre: none
		post: the inputHistoryPosition will be moved one closer to the end and the content at that new position will be set to the current input
		note: if the initial inputHistoryPosition is after the end of the inputHistory container nothing will happen, else:
		if the data in the container at the current inputHistoryPosition does not match the current input value, the current value will be stored in the temporary input.
		if the resulting position is after the end, the current temporary input will be used as the new current input.
		*/
		void descendHistory();

	public:

		//default constructor.
		CLI();

		/* NOTE: the supplied CommandSet is NOT copied - instead a pointer to the supplied object is held, it must exist and remain valid for the life of this CLI object - and any copies of this object */
		CLI(const CommandSet&);

		/* NOTE: the supplied CommandSet is NOT copied - instead a pointer to the supplied object is held, it must exist and remain valid for the life of this CLI object - and any copies of this object */
		void setCommands(const CommandSet&);

		const CommandSet& getCommands(); //maybe make this not a const reference, so commands can be modified?

		/*
		pre: none
		Adds the supplied message to the log (does not appear like user input)
		note: virtual because it has an impact on output content, so derived classes may need to intercept the call.
		*/
		virtual void logMessage(const sf::String& msg);

		/*
		pre: none
		Adds the supplied message to the log as an error.
		note: virtual because it has an impact on output content, so derived classes may need to intercept the call.
		*/
		virtual void logError(const sf::String& details);
		/*
		pre: none
		post: produces a reference to the log of input and output, which can then be displayed.
		*/
		const Log& getLog() const;

        /*
        pre: none
        post: the current input line will be updated to match the provided argument
        */
        void setInput(const sf::String&);

        /*
        returns a reference to the content of the current input line.
        pre-condition: none.
        */
        sf::String getInput()  const;

        /*
        pre: none
        post: an attempt will be made to process the current input line as a command with arguments. The line itself and the result will be appended to the history (input will be ommited if empty), and both input and tempLastInput will be cleared.
        note: the user input line should contain something that seperates it from non-input

        */
        void processInput();

		//virtual destructors ftw
		virtual ~CLI();
	};
}
