/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "GUIConsole.hpp"
#include <cmath>
namespace __GAME_NAMESPACE__
{
	const sf::Vector2f GUIConsole::PADDING = sf::Vector2f(5.f, 5.f);
	const sf::Color GUIConsole::BG_COLOR = sf::Color(0, 0, 0, 180);

	const float GUIConsole::OUTLINE_THICKNESS = 1;
	const sf::Color GUIConsole::OUTLINE_COLOR = sf::Color(255, 255, 255, 255);

	const sf::Color GUIConsole::INPUT_COLOR = sf::Color(180, 180, 180);
	const sf::Color GUIConsole::RESPONSE_COLOR = sf::Color(255, 255, 255);
	const sf::Color GUIConsole::ERROR_COLOR = sf::Color(255, 0, 0);

	void GUIConsole::initialize()
	{
		backgroundBox.setFillColor(BG_COLOR);

		logBorder.setFillColor(sf::Color::Transparent);
		inputBorder.setFillColor(sf::Color::Transparent);

		logBorder.setOutlineColor(OUTLINE_COLOR);
		inputBorder.setOutlineColor(OUTLINE_COLOR);

		logBorder.setOutlineThickness(OUTLINE_THICKNESS);
		inputBorder.setOutlineThickness(OUTLINE_THICKNESS);

		updateGeometry();
	}

	GUIConsole::GUIConsole(const sf::Font& defaultFont, const sf::Vector2u& size) : CLI(), defaultFont(defaultFont), _open(false), size(float(size.x), float(size.y)), logSprite(), inputSprite()
	{
		initialize();
	}

	GUIConsole::GUIConsole(const CommandSet& commands, const sf::Font& defaultFont, const sf::Vector2u& size) : CLI(commands), defaultFont(defaultFont), _open(false), size(float(size.x), float(size.y)), logSprite(), inputSprite()
	{
		initialize();
	}


	GUIConsole::~GUIConsole()
	{
	}

	void GUIConsole::onEvent(const MenuInputEvent& event)
	{
		// sf::String oldInput = getInput();
        switch(MenuInputEvent::SubType _subType = event.subType)
        {
            case MenuInputEvent::SubType::Up:
                ascendHistory();
                break;
            case MenuInputEvent::SubType::Down:
                descendHistory();
                break;
            case MenuInputEvent::SubType::Activate:
                processInput();
                break;
            case MenuInputEvent::Cancel:
                if(!input.isEmpty())
                {
                    input.erase(input.getSize() - 1);
                }
                break;
        }
        // if (oldInput != getInput())
		// {
        // }
		outdateInputSprite();
    }
    void GUIConsole::onEvent(const TextInputEvent& event)
    {
        if (event.unicode != 0x00000008 && event.unicode != 0x0000000D) //backspace and carriage return respectively
        {
            input.insert(input.getSize(), event.unicode);
            outdateInputSprite();
        }
    }

    //     //NOTE: there are probably some non-printable characters that we may have to filter out before sending it to the input string, on top of those we are using as control characters, but we'll worry about that later.
    //
    //     if (event.type == sf::Event::KeyPressed)
    //     {
    //         if (event.key.code == sf::Keyboard::BackSpace && input.getSize() > 0)
    //         {
    //             input.erase(input.getSize() - 1);
    //         }
    //         else if (event.key.code == sf::Keyboard::Return)
    //         {
    //
    //         }
    //         else if (event.key.code == sf::Keyboard::Up)
    //         {
    //
    //         }
    //         else if (event.key.code == sf::Keyboard::Down)
    //         {
    //             descendHistory();
    //         }
    //
    //     }
    //     else if (event.type == sf::Event::TextEntered)
    //     {
    //         if (event.text.unicode != 0x00000008 && event.text.unicode != 0x0000000D) //backspace and carriage return respectively
    //         {
    //             input.insert(input.getSize(), event.text.unicode);
    //         }
    //
    //     }
    //
	// 	//capture events and decide whether or not geometry/visuals need to be updated, etc.
	// 	//TODO: account for window-resize event (needs to occur in other places too when it ends up supported?), possibly other events?
	// 	//TODO: if this is merged with CLI update several of the methods existing in cli so that where relivant they trigger updating of visuals etc.
    //
	// 	//don't need to account for when the log is updated as that is already covered.
    //
	// 	if (oldInput != getInput())
	// 	{
	// 		outdateInputSprite();
	// 	}
    //
	// }

	//inline for potential optimisation maybe?, though it's likely to get ignored by modern compilers anyway.
	void GUIConsole::outdateLogSprite()
	{
		logSpriteOutdated = true;
	}

	void GUIConsole::outdateInputSprite()
	{
		inputSpriteOutdated = true;
	}

	bool GUIConsole::isOpen() const
	{
		return _open;
	}

	void GUIConsole::open()
	{
		_open = true;
	}

	void GUIConsole::close()
	{
		_open = false;
	}

	void GUIConsole::logMessage(const sf::String& msg)
	{
		CLI::logMessage(msg);
		outdateLogSprite();
	}

	void GUIConsole::logError(const sf::String& msg)
	{
		CLI::logError(msg);
		outdateLogSprite();
	}

	void GUIConsole::updateGeometry()
	{

		sf::Vector2f inputBoxSize;
		sf::Vector2f logBoxSize;
		sf::FloatRect logBounds;
		sf::IntRect logTextureRect;
		sf::IntRect inputTextureRect;


		backgroundBox.setSize(size);


		//determine the size of each border box first

		//width for both boxes is the same and is is relative only the size of the console
		//the input box has a constant height which based on the font size and padding of TextBoxes, which we can then use to determine the height of the log box.
		inputBoxSize = sf::Vector2f((size.x - (PADDING.x * 2) - OUTLINE_THICKNESS * 2), (TextBox::DEFAULT_FONT_SIZE + TextBox::PADDING.y * 2));

		//the log box take the same width as the input box, and the rest of the available height after the input box, and padding between the boxes, as well ason the top and bottom of the console.
		logBoxSize = sf::Vector2f(inputBoxSize.x, (size.y - PADDING.y * 4 - inputBoxSize.y));

		//set the sizes of the actual rectangles.

		inputBorder.setSize(inputBoxSize);
		logBorder.setSize(logBoxSize);


		//set the position of the border boxes.
		logBorder.setPosition(sf::Vector2f(PADDING.x + OUTLINE_THICKNESS, PADDING.y + OUTLINE_THICKNESS));

		logBounds = logBorder.getGlobalBounds();

		inputBorder.setPosition(sf::Vector2f(PADDING.x + OUTLINE_THICKNESS, logBounds.top + logBounds.height + PADDING.y + OUTLINE_THICKNESS));

		//set up the render sprites for the input and log texts
		logTextureRect = sf::IntRect(0, 0, int(std::ceil(logBoxSize.x)), int(std::ceil(logBoxSize.y)));
		inputTextureRect = sf::IntRect(0, 0, int(std::ceil(inputBoxSize.x)), int(std::ceil(inputBoxSize.y)));
		//set the texture rect for the sprites to use to build our textures, rather than storing seperately and updating it when we build our textures? optimisation trade-off ocurs here, not sure which is better though.

		logSprite.setTextureRect(logTextureRect);

		inputSprite.setTextureRect(inputTextureRect);

		//reposition the sprites.
		logSprite.setPosition(logBorder.getPosition());
		inputSprite.setPosition(inputBorder.getPosition());

		//recreate the rendertextures and supply the sprite with the texture they maintain.

		logRenderTexture.create(logTextureRect.width, logTextureRect.height);
		inputRenderTexture.create(inputTextureRect.width, inputTextureRect.height);

		logSprite.setTexture(logRenderTexture.getTexture());
		inputSprite.setTexture(inputRenderTexture.getTexture());
		//update the textures used by the sprites

		updateLogSprite();
		updateInputSprite();
	}

	void GUIConsole::updateLogSprite()
	{
		sf::FloatRect textBoxRect;
		sf::IntRect spriteTextureRect(logSprite.getTextureRect());

		//clear before we start drawing
		logRenderTexture.clear(BG_COLOR);

		const CLI::Log& log = getLog();

		CLI::Log::value_type eachEntry;

		TextBox textBox(defaultFont); //we need at least one entry, but just use an empty string if the log is empty.

		//recursively extend the text in the textbox until the size is at least our maximum. (there will be no content if the log is empty).
		//it would likely be more efficient to calculate how many lines of text we can fit on screen using raw data and then recursively add to a string until that many lines is full THEN put it in a text box
		//but this is a lot easier and that's really just an optimisation detail.

		float sumLogHeight = 0;
		for (auto It = log.rbegin(); It != log.rend() && (spriteTextureRect.height > sumLogHeight); It++) //note that this iterator is a const_iterator
		{
			switch (CLI::LogEntryType type = It->second)
			{
			case CLI::LogEntryType::Normal:
				textBox.setString(It->first);
				textBox.setColor(RESPONSE_COLOR);
				break;
			case CLI::LogEntryType::Input:
				textBox.setString(">" + It->first);
				textBox.setColor(INPUT_COLOR);
				break;
			case CLI::LogEntryType::Error:
				textBox.setString("Error: " + It->first);
				textBox.setColor(ERROR_COLOR);
			}

			textBoxRect = textBox.getBounds();
			sumLogHeight += textBoxRect.height;

			textBox.setPosition(sf::Vector2f(0, spriteTextureRect.height - sumLogHeight));
			logRenderTexture.draw(textBox);
		}

		logRenderTexture.display();

		logSpriteOutdated = false;
	}

	void GUIConsole::updateInputSprite()
	{
		TextBox textBox(defaultFont, getInput());
		sf::FloatRect textBoxBounds = textBox.getBounds();
		sf::IntRect spriteTextureRect(inputSprite.getTextureRect());
		textBox.setPosition(sf::Vector2f(0, spriteTextureRect.height - textBoxBounds.height));
		inputRenderTexture.clear(BG_COLOR);
		inputRenderTexture.draw(textBox);
		inputRenderTexture.display();

		inputSpriteOutdated = false;
	}


	sf::FloatRect GUIConsole::getBounds() const
	{
		return sf::FloatRect(getPosition(), size);
	}

	sf::Vector2f GUIConsole::getPosition() const
	{
		return sf::Vector2f();
	}

	void GUIConsole::setSize(const sf::Vector2f& size)
	{
		this->size = size;
		updateGeometry();
	}

	void GUIConsole::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		target.draw(backgroundBox, states);
		target.draw(logSprite, states);
		target.draw(logBorder, states);
		target.draw(inputSprite, states);
		target.draw(inputBorder, states);

#if RENDER_DEBUGGING
		GUIObject::drawDebug(target, states);
#endif
	}

	void GUIConsole::checkForUpdates()
	{
		if (logSpriteOutdated)
		{
			updateLogSprite();
		}

		if (inputSpriteOutdated)
		{
			updateInputSprite();
		}
	}
}
