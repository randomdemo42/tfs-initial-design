/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "CLI.hpp"
#include <cstddef>
namespace __GAME_NAMESPACE__
{
	const CLI::CommandSet CLI::EMPTY_COMMAND_SET = CLI::CommandSet();

	CLI::CLI() : commands(&EMPTY_COMMAND_SET), log(), inputHistory(), inputHistoryPosition(-1), tempLastInput(), input()
	{
	}
	CLI::CLI(const CommandSet& commands) : commands(&commands), log(), inputHistory(), inputHistoryPosition(-1), tempLastInput(), input()
	{
	}

	void CLI::setCommands(const CommandSet& commands)
	{
		this->commands = &commands;
	}

	const CLI::CommandSet& CLI::getCommands()
	{
		return *commands;
	}


	void CLI::ascendHistory()
	{
        if (inputHistoryPosition == -1)
		{
            if(!inputHistory.empty())
            {
                inputHistoryPosition = inputHistory.size()-1;
			    input = inputHistory[inputHistoryPosition];
            }
		}
		else if(inputHistoryPosition != 0)
		{
			if (inputHistory[inputHistoryPosition] != input)
			{
				tempLastInput = input;
			}

			inputHistoryPosition--;
			input = inputHistory[inputHistoryPosition];
		}
	}

	void CLI::descendHistory()
	{
        if (inputHistoryPosition == inputHistory.size()-1)
        {
            if(!inputHistory.empty())
            {
                inputHistoryPosition = -1;
                input = tempLastInput;
            }
        }
        else if (inputHistoryPosition != -1)
		{
			if (inputHistory[inputHistoryPosition] != input)
			{
				tempLastInput = input;
			}

            inputHistoryPosition++;
			input = inputHistory[inputHistoryPosition];
		}
	}



	const CLI::Log& CLI::getLog() const
	{
		return log;
	}

	void CLI::addLogEntry(const sf::String& msg, LogEntryType type)
	{
		log.push_back(Log::value_type(msg, type)); //vector may not actually be the best to use, and anyway its probably more like push_front in terms of logical usage, remember to check usage if we decide to change it?
	}

	void CLI::logMessage(const sf::String& msg)
	{
		addLogEntry(msg);
	}

	void CLI::logError(const sf::String& details)
	{
		addLogEntry(details, LogEntryType::Error);
	}

    sf::String CLI::getInput() const
    {
        return input;
    }

    void CLI::setInput(const sf::String& input)
    {
        this->input = input;
    }

    void CLI::processInput()
    {
        std::size_t first_space = input.find(' ');
        sf::String commandStr = input.substring(0, first_space);
        CommandSet::const_iterator commandIt;
        sf::String args;
        if(input.isEmpty())
        {
            logMessage("");
        }
        else
        {
            inputHistory.push_back(input);
            addLogEntry(input, LogEntryType::Input);
            commandIt = commands->find(commandStr);
            if (commandIt == commands->end())
            {
                logError("Error: Invalid command \"" + commandStr + "\"");
            }
            else
            {
                if (first_space != sf::String::InvalidPos)
                {
                    args = input.substring(first_space + 1); //else defaults already to empty string
                }

                (commandIt->second)(args);
            }

            input.clear();
            tempLastInput.clear();
        }
    }

	CLI::~CLI(){}; //does nothing atm
}
