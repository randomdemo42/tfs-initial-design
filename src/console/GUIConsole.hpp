/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "../definitions.hpp"
#include "CLI.hpp"
#include "../gui/GUIObject.hpp"
#include <queue>
#include "../gui/TextBox.hpp"
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/RenderTexture.hpp>
#include "../event/EventListener.hpp"
#include "../input/MenuInputEvent.hpp"
#include "../input/TextInputEvent.hpp"
namespace __GAME_NAMESPACE__
{
	//NOTE: MAY MERGE WITH THE CLI CLASS AT SOME STAGE

	class GUIConsole :
		public CLI, virtual public GUIObject, public EventListener<MenuInputEvent>, public EventListener<TextInputEvent>
	{
	public:
		static const sf::Vector2f PADDING;
		static const sf::Color BG_COLOR;

		static const sf::Color INPUT_COLOR;
		static const sf::Color RESPONSE_COLOR;
		static const sf::Color ERROR_COLOR;
	private:
		static const float OUTLINE_THICKNESS;
		static const sf::Color OUTLINE_COLOR;


		const sf::Font& defaultFont;

		//whether or not the console is currently open.
		bool _open;
		sf::Vector2f size;

		sf::RenderTexture logRenderTexture;
		sf::RenderTexture inputRenderTexture;

		sf::Sprite logSprite;
		sf::Sprite inputSprite;

		sf::RectangleShape backgroundBox;
		sf::RectangleShape logBorder;
		sf::RectangleShape inputBorder;

		//whether or not each of the sprites are out of date (i.e. something happened that changed the content to be displayed or the geometry, but it hasn't been updated yet.
		bool logSpriteOutdated;
		bool inputSpriteOutdated;
		//called whenever the log box content that gets displayed on the screen needs to be updated. (to be called by relevant internal methods only). logSprite will no longer be out of date
		void updateLogSprite();

		//called whenever the input box content that gets displayed on screen may need to be updated. (to be called by relevant internal methods only). inputSprite will no longer be out of date
		void updateInputSprite();

		//updates geometry as neccesary to adjust to changes that may have occured internally. (to be called by relevant internal methods only)
		void updateGeometry();

		//must be called only by constructors.
		virtual void initialize();

		void outdateLogSprite();
		void outdateInputSprite();
	protected:
		virtual void draw(sf::RenderTarget&, sf::RenderStates) const;
	public:
		GUIConsole(const sf::Font& defaultFont, const sf::Vector2u& size);
		/* NOTE: the supplied CommandSet is NOT copied - instead a pointer to the supplied object is held, it must exist and remain valid for the life of this CLI object - and any copies of this object */
		GUIConsole(const CommandSet& commands, const sf::Font& defaultFont, const sf::Vector2u& size);
		bool isOpen() const;
		/*
			pre: none, though redundant if already open.
		*/
		void open();
		/*
		pre: none, though redundant if already closed.
		*/
		void close();
		virtual ~GUIConsole();

		virtual sf::FloatRect getBounds() const;

		/*
			NOTE: the console is, currently, locked to 0,0 of the viewport at all times.
		*/
		virtual sf::Vector2f getPosition() const;

		virtual void setSize(const sf::Vector2f& size);

		/*
			See CLI::logMessage for details
		*/
		virtual void logMessage(const sf::String&);

		virtual void logError(const sf::String&);

		//checks whether or not the sprites etc are out of date and updates them if neccessary.
		void checkForUpdates();

        /*
        post: the event will be reognised or responded to, etc, often resulting in things like updating the current input.
        */
        virtual void onEvent(const MenuInputEvent&);

        virtual void onEvent(const TextInputEvent&);
    };
}
