#pragma once
#include "BindingSet.hpp"
#include <vector>
#include <map>
#include "GamepadButtonBinding.hpp"
#include "GamepadAxisBinding.hpp"

namespace __GAME_NAMESPACE__
{
	class GamepadBindingSet : public BindingSet
	{

		InputBinding<>* pendingBindingSlot;

		unsigned int joystickId;
		float deadZone;
		bool useSticksForNav;
		//in game controls
		GamepadButtonBinding pause; //pause/unpause.
		GamepadAxisBinding thrustX;
		GamepadAxisBinding thrustY;
		GamepadAxisBinding aimX;
		GamepadAxisBinding aimY;

		//menu bindings!


		GamepadButtonBinding confirm; //confirm/accept/etc
		GamepadButtonBinding back; //back, cancel, etc.
		GamepadButtonBinding navLeft;
		GamepadButtonBinding navUp;
		GamepadButtonBinding navDown;
		GamepadButtonBinding navRight;

		std::vector<InputBinding<>*> inGameCustomizableBindings; //for external use by input settings menus, etc, subset of allBindings (could contain all)
		std::vector<InputBinding<>*> menuCustomizableBindings; //for external use by input settings menus, etc, subset of allBindings (could contain all)
		std::vector<InputBinding<>*> allBindings; //for internal use


		bool buttonStates[BindingSet::ButtonBindingCodeCount];
		std::map<BindingSet::ButtonBindingCode, InputBinding<>&> eventCompatibleBindings;

		InputBinding<>* pendingBindingSlot;
	public:
		GamepadBindingSet();
		virtual ~GamepadBindingSet() = default;

		//want to be able to get them seperately for UI purposes, etc, but also still use them together, we'll see how use the seperation is..
		std::vector<InputBinding<>*> getCustomizableInGameBindings(); //non-const intentionally

		std::vector<InputBinding<>*> getCustomizableMenuBindings();  //non-const intentionally

		//has basically 1.5-2 times the execution time complexity of just inserting them into the one vector, but eh, less likely to get bugs, etc.
		virtual std::vector<InputBinding<>*> getCustomizableBindings();

		float getDeadzone() const;


		//note it is not advisable for the deadzone to be > 70.f, as this could cause some axes to be ignored (e.g. xbox 360 povhat, where the value is never between 0 and 70.f, only == 0 or >=70
		void setDeadZone(float newZone);

		//whether or not the joystick inputs for movement and aim should be usable for navigation, defaults to true;
		bool getCanUseSticksForNav() const;

		void setCanUseSticksForNav(bool newValue);

		virtual bool retreiveEvent(const sf::Event& source, UserControlsEvent& destination);

		sf::Vector2f getThrust() const;
		sf::Vector2f getAim() const;

		bool isTriggerPulled() const;
	};

}