#pragma once
#include "InputSource.hpp"
namespace __GAME_NAMESPACE__
{

	//TODO: tbh I have no use for this, should probably just deprecate/remove it.

	class MouseCursorSource :
		public InputSource
	{
		bool matches(const MouseCursorSource& other) const;
	protected:
		/*
		MouseCursorSource(const TypeID& comparableTypeId);
		*/
	public:
		MouseCursorSource();
		//Important: semi-cheating double-dispatch, see InputSource.hpp for implimentation details.
		virtual bool matches(const InputSource& other) const;
		
		//returns whether or not the object is in a valid state.
		virtual bool hasValidData() const;

		/*
		returns whether or not the source of the event was compatible with this sourceinfo type.
		*/
		static bool isCompatibleWith(const sf::Event& event);
		
		/*
		returns whether or not the source of the event was the exact input source represented by this object
		*/
		virtual bool matches(const sf::Event&) const;

		/*
		if the supplied event is compatible, this takes source information from an event, such that the button/stick/key associated with that event becomes the source.
		if the event was not compatable, the data remains unchanged.

		returns whether or not the event was compatible.
		*/
		//dummy method in this case that really just checks if the event type is correct, included to stop the class being an interface, and for completeness
		virtual bool bindToEventSource(const sf::Event& event);

		//post: nothing actually happens, the source remains valid as there can only be one mousecursorsource.
		virtual void clear();
		
		virtual ~MouseCursorSource() = default;
	};
}