#pragma once
#include "definitions.hpp"
#include "BindingSet.hpp"
#include "KBMButtonBinding.hpp"
#include <map>
namespace __GAME_NAMESPACE__
{
	class KBMBindingSet : public BindingSet
	{
		//note that the next 5 are not event compatible.
		//not going to hardcode, though it would be more efficient probably, as we introduce more controls we will at least have this infastructure.
		KBMButtonBinding left;
		KBMButtonBinding up;
		KBMButtonBinding right;
		KBMButtonBinding down;
		KBMButtonBinding trigger; //otherwise trigger would be always on pretty much for mouse users, causing spam/lag 

		//these are hidden from the list of customizable bindings
		KBMButtonBinding pause; //pause is escape, that's all a kbm user would want right?

		//menu nav etc, also hidden from customisation
		KBMButtonBinding confirm; //enter
		KBMButtonBinding back; //back/cancel: backspace?
		KBMButtonBinding navLeft;
		KBMButtonBinding navUp;
		KBMButtonBinding navRight;
		KBMButtonBinding navDown;

		std::vector<InputBinding<>*> customizableBindings; //for external use by input settings menus, etc, subset of all bindings (could contain all)
		std::vector<InputBinding<>*> allBindings; //for internal use

		bool buttonStates[BindingSet::ButtonBindingCodeCount];
		std::map<BindingSet::ButtonBindingCode, InputBinding<>&> eventCompatibleBindings;

		InputBinding<>* pendingBindingSlot;
	public:
		KBMBindingSet();
		virtual ~KBMBindingSet() = default;

		virtual std::vector<InputBinding<>*> getCustomizableBindings();

		virtual bool retreiveEvent(const sf::Event& source, UserControlsEvent& destination);

		sf::Vector2f getThrust() const;

		bool isTriggerPulled() const;

		virtual InputBinding<>*& getPendingBindingSlot();
	};

}