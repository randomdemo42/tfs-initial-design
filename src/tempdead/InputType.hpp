#pragma once
#include "definitions.hpp"
#include <SFML/Config.hpp>
#include <SFML/System/Vector2.hpp>
namespace __GAME_NAMESPACE__
{
	//note consider replacing the enum system with just types, all lined up under inputType, check that it would be possible?
	enum InputType
	{
		Unknown = 0, //this isn't really used, but is used for classes that take InputType as a param, but want to share an abstract base class?
		Axis,
		Button,
		Text,
		Coord 
	};

	//REINTRODUCE THE 2-PARAM VERSION MAYBE?

	

	//this input type Traits along with the way I plan to do input sources is
	//I guess a curious attempt at providing extensibility for input types and possibly reducing bioler plate code/hardcoded values like ranges?
	//also should be able to provide type-dynamic value lookup on inputs to an extent, possibly, via double dispatch.
	//Not sure how useful it will really be, using it to create lists of inputs of arabtray type will probably also require something like a visitor to impliment double dispatch but thats fine
	//anyway I want to see how it goes..

	//NOTE: VALUE TYPES MUST HAVE A DEFAULT VALUE, it should be valid and exists, used in case polling fails
	//this underscore stuff is a template for how Traitss should be created

	//input types whose value can't be used as a non-type template param.
	template<InputType _type, typename vType, bool _hasTotalOrder> //min and max /should/ be equal to default if they are non-linear, they should be ignored in all cases, but for consistancies sakes 
	struct __InputTypeTraitsInnerBase__
	{
		static const InputType type = _type;
		typedef vType ValueType;
		static const bool hasTotalOrder = _hasTotalOrder;
		static const ValueType defaultValue;
	};

	template<InputType _type, typename vType, bool linear> struct InputTypeTraitsBase;

	//I would consider getting these traits bases to inherit from one another but it seems like increased overhead? deal with that when it's an issue?

	template<InputType _type, typename vType>
	struct InputTypeTraitsBase < _type, vType, false > :
		public __InputTypeTraitsInnerBase__ < _type, vType, false >
	{};

	//linear types should probably define a < compare method later and possibly all types should declare or reference etc an == method later on?

	//not all linear inputs may find max and min valuable, but if its problematic to have it well deal with it then
	//ranges are inclusive
	template<InputType _type, typename vType>
	struct InputTypeTraitsBase < _type, vType, true >:
		public __InputTypeTraitsInnerBase__< _type, vType, true>
	{
		
		static const ValueType minValue;
		static const ValueType maxValue;
	};

	template<InputType _type> struct InputTypeTraits;

	template<> struct InputTypeTraits < Axis >: public InputTypeTraitsBase<Axis, float, true> {};

	template<> struct InputTypeTraits< Button> : public InputTypeTraitsBase < Button, bool, true> {};
	
	//text should only be used for events in theory, but I guess the potential doesn't hurt? 
	template<> struct InputTypeTraits < Text > : public InputTypeTraitsBase < Text, sf::Uint32, false > {};

	template<> struct InputTypeTraits<Coord>: public InputTypeTraitsBase<Coord, sf::Vector2i, false> {};
	
	template<> struct InputTypeTraits<Unknown> : public InputTypeTraitsBase<Unknown, bool, false> {};

#if __THIS_IS_THE_INPUT_DOT_CPP_FILE__
	/*
		The definitions of the default values,
		I think that they should be viewed and modified in the same file as where their types are set,
		given their nature,
		so lets use some pre-compiler macro tricks!
	*/

	const float InputTypeTraits < Axis >::defaultValue = 0.f;
	const float InputTypeTraits < Axis >::minValue = -100.f;
	const float InputTypeTraits < Axis >::maxValue = 100.f;

	const bool InputTypeTraits<Button>::defaultValue = 0;
	const sf::Uint32 InputTypeTraits<Text>::defaultValue = 0;

	const sf::Vector2i __DEFAULT_COORD_VALUE__;
	const sf::Vector2i InputTypeTraits<InputType::Coord>::defaultValue = __DEFAULT_COORD_VALUE__;
#endif
}
