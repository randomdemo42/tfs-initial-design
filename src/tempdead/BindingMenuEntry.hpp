#pragma once
#include "MenuItem.hpp"
#include "TextBox.hpp"
#include "SimpleTextButton.hpp"
#include "InputBinding.hpp"
namespace __GAME_NAMESPACE__
{
	class BindingMenuEntry :
		public MenuItem
	{
		InputBinding<>& target;
		InputBinding<>*& pendingSlot;
		TextBox nameBox;
		SimpleTextButton rebindButton;
	protected:
		virtual void draw(sf::RenderTarget&, sf::RenderStates) const;
	public:
		//note that both targetBinding pendingSlot must be valid and remain so for the lifetime of this object.
		BindingMenuEntry(const sf::Font& defaultFont, InputBinding<>& targetBinding, InputBinding<>*& pendingSlot, const sf::Vector2f& position = sf::Vector2f());
		virtual sf::FloatRect getBounds() const;
		virtual void setPosition(const sf::Vector2f&);
		virtual void onEvent(const MenuEvent&);
		virtual void update();
		virtual sf::Vector2f getPosition() const;
		virtual ~BindingMenuEntry() = default;
	};
}