#include "InputWatchDemo.hpp"
#include <sstream>
#include <SFML/Graphics/RenderTarget.hpp>
#include <cmath>
namespace __GAME_NAMESPACE__
{
	InputWatchDemo::InputWatchDemo(const sf::Font& defaultFont) : movement(sf::Vector2<InputTypeTraits<InputType::Axis>::ValueType>()), aim(sf::Vector2<InputTypeTraits<InputType::Axis>::ValueType>()), isPauseTriggerPulled(false), bindingAInfo(defaultFont), movementDisplay(defaultFont), aimDisplay(defaultFont), isPauseTriggerPulledDisplay(defaultFont), latestInputDisplay(defaultFont), hasPendingBindDisplay(defaultFont)
	{
		//this whole section is written innefficiently since most gets duplicated in onEvent, but this is only a demo class so don't worry about that.
		sf::FloatRect eachBounds;
		sf::Vector2f position(600, 200);

		

		std::stringstream stream;
		stream << movement.x << ", " << movement.y;
		movementDisplay.setString("Movement: " + stream.str());
		stream.str("");

		stream << aim.x << ", " << aim.y;
		aimDisplay.setString("Aim: " + stream.str());
		stream.str("");

		stream << (isPauseTriggerPulled ? "true" : "false");
		isPauseTriggerPulledDisplay.setString("Pause Trigger Pulled: " + stream.str());
		stream.str("");

		if (bindings.bindingA.getSource().hasSourceInfo())
		{
			stream << bindings.bindingA.getSource().getValueRetriever()();
			bindingAInfo.setString("Binding Name: " + bindings.bindingA.getName() + ";\nSource Name: " + bindings.bindingA.getSource().getSourceInfo().getName() + "; Current Value: " + stream.str());
			stream.str("");
		}
		else
		{
			bindingAInfo.setString("Binding Name: " + bindings.bindingA.getName() + ";\nSource: N/A (unbound);");
		}

		stream << (hasPendingBinding() ? "true" : "false");
		hasPendingBindDisplay.setString("Bind pending: " + stream.str());
		stream.str("");

		latestInputDisplay.setString("latest input is:\n\tsource: N/A; value: N/A");

		movementDisplay.setPosition(position);

		eachBounds = movementDisplay.getBounds();

		aimDisplay.setPosition(sf::Vector2f(position.x, eachBounds.top + eachBounds.height + TextBox::PADDING.y));

		eachBounds = aimDisplay.getBounds();

		isPauseTriggerPulledDisplay.setPosition(sf::Vector2f(position.x, eachBounds.top + eachBounds.height + TextBox::PADDING.y));

		eachBounds = isPauseTriggerPulledDisplay.getBounds();

		latestInputDisplay.setPosition(sf::Vector2f(position.x, eachBounds.top + eachBounds.height + TextBox::PADDING.y));

		eachBounds = latestInputDisplay.getBounds();
		
		bindingAInfo.setPosition(sf::Vector2f(position.x, eachBounds.top + eachBounds.height + TextBox::PADDING.y));

		eachBounds = bindingAInfo.getBounds(); 

		hasPendingBindDisplay.setPosition(sf::Vector2f(position.x, eachBounds.top + eachBounds.height + TextBox::PADDING.y));
	}

	void InputWatchDemo::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{

		target.draw(movementDisplay, states);
		target.draw(aimDisplay, states);
		target.draw(isPauseTriggerPulledDisplay, states);
		target.draw(latestInputDisplay, states);
		target.draw(bindingAInfo, states);
		target.draw(hasPendingBindDisplay, states);

#if RENDER_DEBUGGING
		GUIObject::drawDebug(target, states);
#endif
	}

	void InputWatchDemo::onEvent(const sf::Event& event)
	{
		std::stringstream stream;
		if (hasPendingBinding())
		{
			if (pendingBinding->getSource().bindToEventSource(event))
			{
				pendingBinding = nullptr;
			}
		}
		/*
		else
		{
			if (event.type == sf::Event::JoystickMoved && std::abs(event.joystickMove.position) > 50.f) //using arbatrary 50% deadzone for now
			{
				stream << "Joystick" << event.joystickMove.joystickId << ".Axis(" << getAxisName(event.joystickMove.axis) << ");\n\tvalue: " << event.joystickMove.position;
				latestInputDisplay.setString("latest input is:\n\tsource: " + stream.str());
			}
			else if (event.type == sf::Event::JoystickButtonPressed || event.type == sf::Event::JoystickButtonReleased)
			{
				stream << "Joystick" << event.joystickButton.joystickId << ".Button" << event.joystickButton.button << ";\n\tvalue: " << (event.type == sf::Event::JoystickButtonPressed ? "pressed" : "released");
				latestInputDisplay.setString("latest input is:\n\tsource: " + stream.str());
			}
		}
		*/
	}

	void InputWatchDemo::update()
	{
		std::stringstream stream;
		/*
		
		
		movement.x = bindings.movementX.getValue();
		movement.y = bindings.movementY.getValue();
		aim.x = bindings.aimX.getValue();
		aim.y = bindings.aimY.getValue();
		isPauseTriggerPulled = bindings.pauseTrigger.getValue();

		
		stream << movement.x << ", " << movement.y;
		movementDisplay.setString("Movement: " + stream.str());
		stream.str("");

		stream << aim.x << ", " << aim.y;
		aimDisplay.setString("Aim: " + stream.str());
		stream.str("");

		stream << (isPauseTriggerPulled ? "true" : "false");
		isPauseTriggerPulledDisplay.setString("Trigger Pulled: " + stream.str());
		stream.str("");
		*/

		if (bindings.bindingA.getSource().hasSourceInfo())
		{
			stream << bindings.bindingA.getSource().getValueRetriever()();
			bindingAInfo.setString("Binding Name: " + bindings.bindingA.getName() + ";\nSource Name: " + bindings.bindingA.getSource().getSourceInfo().getName() + "; Current Value: " + stream.str());
			stream.str("");
		}
		else
		{
			bindingAInfo.setString("Binding Name: " + bindings.bindingA.getName() + ";\nSource: N/A (unbound);");
		}

		stream << (hasPendingBinding() ? "true" : "false");
		hasPendingBindDisplay.setString("Bind pending: " + stream.str());
		stream.str("");


	}

	InputWatchDemo::~InputWatchDemo()
	{
	}

	//not going to bother with real implimentations for now, probably should is the class doesn't end up getting scrapped
	sf::FloatRect InputWatchDemo::getBounds() const
	{
		return sf::FloatRect();
	}

	sf::Vector2f InputWatchDemo::getPosition() const
	{
		return sf::Vector2f();
	}

	bool InputWatchDemo::hasPendingBinding() const
	{
		return pendingBinding != nullptr;
	}

	void InputWatchDemo::setPendingBinding(InputBinding<>* ptr)
	{
		pendingBinding = ptr;
	}
}