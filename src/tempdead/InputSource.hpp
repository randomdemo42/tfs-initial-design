#pragma once
#include "definitions.hpp"
#include "InputType.hpp"
#include "UniqueTypeID.hpp"
#include <SFML/System/String.hpp>
#include <SFML/Window/Event.hpp>
namespace __GAME_NAMESPACE__
{
	//NOTICE: DEPRECATED CLASS!

	/*
	constraints on all classes that extend InputSource (IMPORTANT):
	all classes that are derived directly or indirectly from InputSource<> must be implimented such that for any two InputSource a,b, if the exact same values would be obtained via both InputSource's valueGetter's (i.e they correspond the same user input, possibly interpreted differently)


	note: this restriction is impossible without doing casting, or implimenting a full visitor pattern, which would kind of defeat the point of even trying. So casting has been used, via typechecking and re-interpret cast, to minimise runtime overhead.
	const TypeID& typeId;
	for now, enforce via double dispatch?, reduce boilerplate for this by using our TypeID:
	a protected constructor with a TypeID as the first param needs to be provided by all classes, that passes it to the same contructor in it's direct base class.

	where a derived type's matches should differ from their base's (and ONLY if):
	a public constructor must provide to it's base's contructor the TypeID for it's own type (not a paramater of the public constructor)
	bool matches() must be overloaded as non-virtual and private to accept the exact type of the derived class.
	bool matches(const InputSource<>& other) with that exact signature must be overloaded as virtual and public, with the exact code shown in comments in InputSource<>

	if one-or-more-but-not-all of the the above 3 are performed, the class will not be usable for comparison (which voilates)

	NOTE: the reason for the complex setup is the neccessity that different InputSources can be compared accurately through the most base pointer/reference type,
	such that bindings can masqerade as types different to the actual source, or more importantly,
	sources with the same result types but distinct sets of hardware sources may be used in the same binding, etc.
	(we should check how useful this is in reality probably)

	ON SECOND THOUGHT IT MAY NOT BE USEFUL TO EXTEND MOST OF THESE IN THE FIRST PLACE (when it comes to the physical input source, virtual input sources/translators may be useful, but will basically just be wrappers), BUT THE INTERFACE WITH DOUBLE-DISPATCH IS STILL QUITE HELPFUL?
	*/

	class InputSource
	{
	public:
		static const sf::String NULLSOURCE_NAME;
	private:
		TypeID comparableTypeId;
		sf::String name; //the name of the source, changes when matched to an event.
	
	protected:
		bool isSameTypeAs(const InputSource& other) const;
		
		InputSource(const TypeID& comparableTypeId, const sf::String& name = "");
	public:
		//gets the human-readable name for the source, not garaunteed to be unique.
		//meaningful if hasValidData()
		sf::String getName() const;

		void setName(const sf::String& name);
		/*
		InputSource(const InputSource&) = delete;
		InputSource(InputSource&&) = delete;
		InputSource& operator=(const InputSource&) = delete;
		InputSource& operator=(InputSource&&) = delete;
		*/

		InputSource();
		//Important: semi-cheating double-dispatch, see below for implimentation instructions
		virtual bool matches(const InputSource& other) const = 0;

		/*
		returns whether or not the source of the event was the exact input source represented by this object
		*/
		virtual bool matches(const sf::Event&) const = 0;

		/*
		this is the implimentation and signature that should be copied where matches should separate.
		virtual bool matches(const InputSource& other) const
		{
			return isSameTypeAs(other) ? false : reinterpret_cast<decltype(*this)>(other).matches(*this);
		}
		*/

		//returns whether or not the object is in a valid state.
		virtual bool hasValidData() const = 0;

		/*
		if the supplied event is compatible, this takes source information from an event, such that the button/stick/key associated with that event becomes the source.
		if the event was not compatable, the data remains unchanged.

		returns whether or not the event was compatible.
		*/
		virtual bool bindToEventSource(const sf::Event&) = 0;
		
		//post: the source will no longer have valid data
		virtual void clear() = 0;

		virtual ~InputSource() = default;
	};
}