#include "UserControlsEvent.hpp"
namespace __GAME_NAMESPACE__
{
	UserControlsEvent::UserControlsEvent(BindingSet::ButtonBindingCode boundButtonCode, bool isPressedEvent)
		: type(isPressedEvent ? BoundButtonPressed : BoundButtonReleased)
	{
		boundButton.code = boundButtonCode;
	}

	UserControlsEvent::UserControlsEvent(sf::Event::MouseMoveEvent eventData)
		: type(MouseMoved), mouseMove(eventData)
	{}

	UserControlsEvent::UserControlsEvent(sf::Event::MouseButtonEvent eventData, bool isPressEvent)
		: type(isPressEvent ? MouseButtonPressed : MouseButtonReleased), mouseButton(eventData)
	{}
}