#pragma once
#pragma once
#include "definitions.hpp"
#include "InputSource.hpp"
namespace __GAME_NAMESPACE__
{
	struct DemoBindingMap
	{


		const InputSource<Axis>* movementX;
		const InputSource<Axis>* movementY;

		const InputSource<Axis>* aimX;
		const InputSource<Axis>* aimY;

		const InputSource<Button>* weaponTrigger;
		const InputSource<Button>* pauseTrigger;

		
	public:

		

		DemoBindingMap();
		virtual ~DemoBindingMap();
	};

}