#pragma once
#include "Inputbinding.hpp"
namespace __GAME_NAMESPACE__
{
	class GamepadAxisBinding :
		public InputBinding < Axis >
	{
	public:
		typedef sf::Joystick::Axis GamepadAxisSource;
	private:
		

		/*
		note: despite being a const the joystickId must be point to valid, existing data for the lifetime of this object.
		if it bound to a temporary, or the data is deleted, this class becomes permanantly invalid and all usage will be undefined behaviour.
		*/
		//the ID may change, but this class garauntees not to change it.
		const unsigned int& joystickId;
		GamepadAxisSource source;

		//pre: hasSource()
		virtual InputSource& getSource();
	public:
		/*
		note: despite being a const the joystickId must be point to valid, existing data for the lifetime of this object.
		if it bound to a temporary, or the data is deleted, this class becomes permanantly invalid and all usage will be undefined behaviour.
		*/
		GamepadAxisBinding(const sf::String& name, const unsigned int& joystickId);
		
		//not neccessarily impossible (although probably, due to the joystickID reference), but disabling now due to non-triviality, will impliment if it becomes appropriate or neccessary. Not sure if the default move is appropriate, but it probably is.
		GamepadAxisBinding(const GamepadAxisBinding&) = delete;
		GamepadAxisBinding(GamepadAxisBinding&&) = delete;
		GamepadAxisBinding& operator=(const GamepadAxisBinding&) = delete;
		GamepadAxisBinding& operator=(GamepadAxisBinding&&) = delete;

		//pre: hasSource()
		virtual const InputSource& getSource() const;

		virtual bool hasSource() const;

		//defaults to constantly giving the default-value of the type if the source info isn't existing and valid.
		//virtual std::function<InputTypeTraits < type >::ValueType()> getValueRetriever() const;

		//if the source isn't valid, returns the default value.
		virtual InputTypeTraits < type >::ValueType getValue() const;

		static bool isCompatibleWith(const sf::Event& event);

		virtual bool bindToEventSource(const sf::Event& event);

		virtual void clear();

		virtual ~GamepadAxisBinding();
	};

}