#pragma once
#include "definitions.hpp"
#include "BindingSet.hpp"
#include <SFML/Window/Event.hpp>
namespace __GAME_NAMESPACE__
{
	struct UserControlsEvent //note: does not include mouseMove, etc?
	{
		enum EventType
		{
			BoundButtonPressed,
			BoundButtonReleased,
			MouseMoved,
			MouseButtonPressed,
			MouseButtonReleased
		};
		EventType type;

		struct BoundButtonEvent
		{
			BindingSet::ButtonBindingCode code;
		};

		//IMPORTANT: THE ONLY MOUSE BUTTON THAT SHOULD FIRE THIS EVENT AT TIME OF WRITING IS THE LEFT BUTTON
		//IT IS EQUALLY IMPORTANT THAT THE LEFT MOUSE NEVER GETS BOUND TO PAUSE, CONFIRM, BACK OR ANY NAV BINDINGS, AS THIS WOULD PRODUCE BUGGY MENU BEHAVIOUR.
		union{
			BoundButtonEvent boundButton;
			sf::Event::MouseMoveEvent mouseMove; //only valid to access if type == MouseMove
			sf::Event::MouseButtonEvent mouseButton; //valid if type == MouseButtonPressed or MouseButtonReleased
		};
		
		UserControlsEvent(BindingSet::ButtonBindingCode boundButtonCode, bool isPressedEvent);
		UserControlsEvent(sf::Event::MouseMoveEvent eventData);
		UserControlsEvent(sf::Event::MouseButtonEvent eventData, bool isPressEvent);
	};
}