#pragma once
#include "DeviceComponentID.hpp"
namespace __GAME_NAMESPACE__
{


	template<typename SourceIDType, typename ValueType>
	ValueType getSourceValue(const SourceIDType);

	//indices?
	template< std::size_t... Ns >
	struct indices
	{
		using next = indices < Ns..., sizeof...(Ns) > ;
	};

	template< std::size_t N >
	struct make_indices
	{
		using type = typename make_indices< N - 1 >::type::next;
	};

	template<>
	struct make_indices < 0 >
	{
		using type = indices < > ;
	};

	// we need something to find a type's index within a list of types
	template<typename T, typename U, std::size_t = 0>
	struct index {};

	template<typename T, typename... Us, std::size_t N>
	struct index<T, std::tuple<T, Us...>, N>
		: std::integral_constant < std::size_t, N > {};

	template<typename T, typename U, typename... Us, std::size_t N>
	struct index<T, std::tuple<U, Us...>, N>
		: index < T, std::tuple<Us...>, N + 1 > {};

	//hopefully this indicies thing above works for us.

	template<typename SourceIDType, typename ValueType>
	ValueType getSourceValue(const SourceIDType);

	template < typename... Ts >
	class DemoVariant
	{
		std::size_t currentTypeIndex = -1;
		struct AbstractWrap
		{};
		std::unique_ptr<AbstractWrap> dataWrapp;
		template<typename T>
		struct Wrap : public AbstractComponentWrap
		{
			T data;
		};

		template<typename T>
		T& get()
		{
			if (currentTypeIndex == index<T, std::tuple<Ts...>>::value)
			{
				return (static_cast<Wrap<T>*> dataWrap.get()).data;
			}
			else
			{
				throw std::string("WRONG COMOMPONENT TYPE");
			}
		}

			template<typename T>
		void set(T data)
		{
			ComponentWrap<T> currentComponentIdStore;
			if (currentComponentIDTypeIndex == index<T, std::tuple<Ts...>>::value)
			{
				currentComponentIdStore = (static_cast<ComponentWrap<T>*> ComponentIdStore.get());
			}
			else
			{
				currentComponentIdStore = new ComponentWrap < T > ;
				ComponentIdStore.reset(currentComponentIdStore);
			}
			currentComponentIdStore.data = data;
		}
		std::size_t getTypeIndex()
		{
			return currentTypeIndex;
		}
	};


	//VERY INCOMPLETE.
	/*
	trying yet another way to do this.
	MultipleDevicesOfTypeSupported denotes whether or not to expect multiple devices of a given type.
	*/
	struct InputSourceIDConstraintBase{}; //constraint base exists for std::enable_if?

	template<typename DeviceIDType, bool SupportMultipleDevicesOfType, typename... ComponentIDType>
	class InputSourceID;

	template<typename DeviceIDType, typename... ComponentIDType>
	class InputSourceID<DeviceIDType, true, ComponentIDType...> : public InputSourceIDConstraintBase
	{
	public:
		static const bool SupportsMultipleDevicesOfType = true;
	private:
		std::reference_wrapper<const DeviceIDType> deviceId; //reference wrapper such that a collection of components can be restricted to one device without.. Actually. Disabling for now. push that behaviour to the binding. 
		DemoVariant<ComponentIDType...> componentId;
	public:
		template<typename InComponentIDType>
		InputSourceID(const DeviceIDType&, const InComponentIDType&);

		InputSourceID(const InputSourceID&);
		InputSourceID(InputSourceID&&);
		InputSourceID& operator=(InputSourceID);
		InputSourceID& operator=(InputSourceID&&);

		template<typename EventType>
		bool isCompatibleWith(const EventType&); //defaults to false, non-static, override where possible, checks the deviceId too if applicable

		template<typename EventType>
		bool setToMatch(const EventType&);  //this has no default!, only overload where possibly compatible

		template<typename EventType>
		//whether or not the current data matches the supplied event
		bool matches(const EventType&);  //no default!, only overload where possibly compatible

		//whether or not the current data matches the supplied InputSourceID. uses == operator such that SourceIDs can be recycled as DeviceIDs and ComponentIDs, though not sure if that's useful, does seem interesting?
		bool operator==(const InputSourceID&); //this has full default

		//gets the current value of the source.
		template<typename ValueType>
		friend ValueType getSourceValue(const InputSourceID&); //only one will exist, doesn't matter what, kind of like an auto I guess.

	};

template<typename DeviceIDType, typename... ComponentIDType>
	InputSourceID<DeviceIDType, true, ComponentIDType...>::InputSourceID(const InputSourceID& src) : deviceId(src.deviceId), componentId(src.componentId)
	{}

	template<typename DeviceIDType, typename... ComponentIDType >
	template<typename InComponentIDType>
	InputSourceID<DeviceIDType, true, ComponentIDType...>::InputSourceID(const DeviceIDType& deviceID, InComponentIDType& specificComponentId): deviceId(deviceId), componentId()
	{
		this->componentId.set(componentId);
	}

	

	template<typename DeviceIDType, typename... ComponentIDType>
	InputSourceID<DeviceIDType, ComponentIDType, true>& InputSourceID<DeviceIDType, ComponentIDType, true>::operator=(InputSourceID src)
	{
		deviceId = std::swap(deviceId, src.deviceId);
		componentId = std::swap(componentId, src.componentId);
	}

	template<typename DeviceIDType, typename ComponentIDType>
	InputSourceID<DeviceIDType, ComponentIDType, true>& InputSourceID<DeviceIDType, ComponentIDType, true>::operator=(InputSourceID&& src)
	{
		deviceId = std::move(src.deviceId);
		componentId = std::move(src.componentId);
	}

	//default to false, override where true
	template<typename DeviceIDType, typename ComponentIDType>
	template<typename EventType>
	bool InputSourceID<DeviceIDType, ComponentIDType, true>::isCompatibleWith(const EventType& event)
	{
		return false;
	}

	template<typename DeviceIDType, typename ComponentIDType>
	bool InputSourceID<DeviceIDType, ComponentIDType, true>::operator==(const InputSourceID& other)
	{
		return deviceId == other.deviceId && componentId == other.componentId;
	}

	template<typename DeviceIDType, typename ComponentIDType>
	class InputSourceID<DeviceIDType, ComponentIDType, false> : public InputSourceIDConstraintBase, public DeviceComponentID<DeviceIDType, ComponentIDType>
	{
	public:
		static const bool SupportsMultipleDevicesOfType = false;
	private:
		ComponentIDType componentId;
	public:
		InputSourceID(const ComponentIDType&);

		InputSourceID(const InputSourceID&); //copy constructor
		InputSourceID(InputSourceID&&); //copy assignment
		InputSourceID& operator=(InputSourceID);
		InputSourceID& operator=(InputSourceID&&);

		template<typename EventType>
		//default to false, override where true?
		static bool isCompatibleWith(const EventType&); //defaults to false, non-static, override where possible, 

		template<typename EventType>
		bool setToMatch(const EventType&);  //this has no default!, only overload where possibly compatible

		template<typename EventType>
		//whether or not the current data matches the supplied event
		bool matches(const EventType&);  //no default!, only overload where possibly compatible

		//whether or not the current data matches the supplied InputSourceID. uses == operator such that SourceIDs can be recycled as DeviceIDs and ComponentIDs, though not sure if that's useful, does seem interesting?
		bool matches(const InputSourceID<DeviceIDType, ComponentIDType..., false>& ); //this has full default

		//whether or not the current data matches the supplied InputSourceID. uses == operator such that SourceIDs can be recycled as DeviceIDs and ComponentIDs, though not sure if that's useful, does seem interesting?
		bool operator==(const InputSourceID<DeviceIDType, ComponentIDType, false>& ); //this has full default

		//gets the current value of the source.
		template<typename ValueType>
		friend ValueType getSourceValue(const InputSourceID&); //only one will exist, doesn't matter what, kind of like an auto I guess.
	};



	template<typename DeviceIDType, typename ComponentIDType>
	InputSourceID<DeviceIDType, ComponentIDType, false>::InputSourceID(const ComponentIDType& componentId) : componentId(componentId)
	{}


	template<typename DeviceIDType, typename ComponentIDType>
	InputSourceID<DeviceIDType, ComponentIDType, false>::InputSourceID(const InputSourceID& src) : componentId(src.componentId)
	{}

	template<typename DeviceIDType, typename ComponentIDType>
	InputSourceID<DeviceIDType, ComponentIDType, false>& InputSourceID<DeviceIDType, ComponentIDType, false>::operator=(InputSourceID src)
	{
		componentId = std::swap(componentId, src.componentId);
	}

	template<typename DeviceIDType, typename ComponentIDType>
	InputSourceID<DeviceIDType, ComponentIDType, false>& InputSourceID<DeviceIDType, ComponentIDType, false>::operator=(InputSourceID&& src)
	{
		componentId = std::move(src.componentId);
	}

	//default to false, override where true
	template<typename DeviceIDType, typename ComponentIDType>
	template<typename EventType>
	bool InputSourceID<DeviceIDType, ComponentIDType, false>::isCompatibleWith(const EventType& event)
	{
		return false;
	}

	template<typename DeviceIDType, typename ComponentIDType>
	bool InputSourceID<DeviceIDType, ComponentIDType, false>::operator==(const InputSourceID& other)
	{
		return componentId == other.componentId;
	}

	/*template<typename DeviceIDType, typename ComponentIDType>
	template<>
	bool InputSourceID<DeviceIDType, ComponentIDType>::isCompatibleWith(const sf::Event& event)
	{
	switch (sf::Event::EventType eventType = event.type)
	{
	case sf::Event::EventType::JoystickButtonPressed:
	case sf::Event::JoystickButtonReleased:
	return isCompatibleWith(event.joystickButton);
	break;
	case sf::Event::JoystickMoved:
	return isCompatibleWith(event.joystickMove);
	break;
	case sf::Event::KeyPressed:
	case sf::Event::KeyReleased:
	return isCompatibleWith(event.key);
	break;
	case sf::Event::MouseButtonPressed:
	case sf::Event::MouseButtonReleased:
	return isCompatibleWith(event.mouseButton);
	break;
	case sf::Event::MouseMoved:
	return isCompatibleWith(event.mouseMove);
	break;
	case sf::Event::MouseWheelMoved:
	return isCompatibleWith(event.mouseWheel);
	break;
	default:
	return false;
	}
	}*/

	template<bool head, bool... tail>
	struct var_or
	{
		static const bool value = true;
	};

	template<bool... tail> struct var_or<false, tail...>
	{
		static const bool value = var_or<tail...>::value;
	};

	template<> struct var_or<false>
	{
		static const bool value = false;
	};

	//variable component types test
	template<typename DeviceIDType, typename ... ComponentIDType>
	template<typename EventType>
	bool InputSourceID<DeviceIDType, DemoVariant<ComponentIDType...>, true>::isCompatibleWidth(const EventType& event)
	{
		return var_or < InputSourceID<DeviceIDType, ComponentIDType, false>::isCompatibleWith(event)... >::value ;
	}

	template<typename DeviceIDType, typename ... ComponentIDType>
	template<typename EventType>
	bool InputSourceID<DeviceIDType, DemoVariant<ComponentIDType...>, false>::isCompatibleWidth(const EventType& event)
	{
		return var_or < InputSourceID<DeviceIDType, ComponentIDType, false>::isCompatibleWith(event)... >::value;
	}

	/*specific implimentation starts here?*/

	//MOUSEBUTTON

	//dummy class to distinguish mouse sources, there can only be one.
	struct MouseSourceID
	{};

	typedef InputSourceID<MouseSourceID, sf::Mouse::Button, false> MouseButtonID;

	template<>
	template<>
	bool MouseButtonID::isCompatibleWith(const sf::Event::MouseButtonEvent&)
	{
		return true;
	}

	template<>
	template<>
	bool MouseButtonID::setToMatch(const sf::Event::MouseButtonEvent& event)
	{
		componentId = event.button;
		return true;
	}

	template<>
	template<>
	bool MouseButtonID::matches(const sf::Event::MouseButtonEvent& event)
	{
		return componentId == event.button;
	}

	template<>
	bool getSourceValue(const MouseButtonID& srcId)
	{
		return sf::Mouse::isButtonPressed(srcId.componentId);
	}

	//KEYBOARD KEY
	struct KeyboardSourceID
	{};

	typedef InputSourceID<KeyboardSourceID, sf::Keyboard::Key, false> KeyboardKeyID;

	template<>
	template<>
	bool KeyboardKeyID::isCompatibleWith(const sf::Event::KeyEvent&)
	{
		return true;
	}

	template<>
	template<>
	bool KeyboardKeyID::setToMatch(const sf::Event::KeyEvent& event)
	{
		componentId = event.code;
		return true;
	}

	template<>
	template<>
	bool KeyboardKeyID::matches(const sf::Event::KeyEvent& event)
	{
		return componentId == event.code;
	}

	template<>
	bool getSourceValue(const KeyboardKeyID& srcId)
	{
		return sf::Keyboard::isKeyPressed(srcId.componentId);
	}

	//JOYSTICK STUFF
	//template<typename PrimitiveType>
	//class PrimitiveWrapper
	//{
	//	PrimitiveType data;
	//public:
	//	operator PrimitiveType() const
	//	{
	//		return data;
	//	}

	//	PrimitiveWrapper& operator=(PrimitiveType& data)
	//	{
	//		this->data = data;
	//	}
	//};

	//class JoystickID: public PrimitiveWrapper<unsigned int>
	//{};

	//JOYSTICK AXIS

	//JOYSTICK BUTTON

	typedef InputSourceID<unsigned int, unsigned int, false> JoystickButtonID;

	template<>
	template<>
	bool JoystickAxisID::isCompatibleWith(const sf::Event::JoystickButtonEvent& event)
	{
		return deviceId.get() == event.joystickId;
	}

	template<>
	template<>
	bool JoystickButtonID::setToMatch(const sf::Event::JoystickButtonEvent& event)
	{
		if (isCompatibleWith(event))
		{
			componentId = event.button;
			return true;
		}
		else
		{
			return false;
		}
	}

	template<>
	template<>
	bool JoystickButtonID::matches(const sf::Event::JoystickButtonEvent& event)
	{
		return deviceId.get() == event.joystickId && componentId == event.button;
	}

	template<>
	bool getSourceValue(const JoystickButtonID& srcId)
	{
		return sf::Joystick::isButtonPressed(srcId.deviceId.get(), srcId.componentId);
	}
}