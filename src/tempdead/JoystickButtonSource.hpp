#pragma once
#include "InputSource.hpp"
namespace __GAME_NAMESPACE__
{
	//note: this class is niave to which joystick it is associated with, control over that is left to classes that use this.
	class JoystickButtonSource :
		public InputSource
	{
	public:
		static std::string getButtonCodeName(unsigned int buttonId);
	private:
		unsigned int buttonId;
		bool _buttonIsValid;

		bool matches(const JoystickButtonSource&) const;
	protected:
		/*
		JoystickButtonSource(const TypeID& comparableTypeId);
		//JoystickButtonSource(const TypeID& comparableTypeId, unsigned int joystickId);
		JoystickButtonSource(const TypeID& comparableTypeId, unsigned int joystickId, unsigned int buttonId);
		JoystickButtonSource(const TypeID& comparableTypeId, unsigned int joystickId, unsigned int buttonId, const sf::String& name);
		*/
	public:
		JoystickButtonSource();
		JoystickButtonSource(unsigned int buttonId);
		JoystickButtonSource(unsigned int buttonId, const sf::String& name);
		
		//returns whether or not the object is in a valid state.
		virtual bool hasValidData() const;

		//pre-condition: hasValidData()
		unsigned int getButtonId() const;

		void setButton(unsigned int buttonId);
		void setData(unsigned int buttonId, const sf::String& name);

		//Important: semi-cheating double-dispatch, see InputSource.hpp for implimentation details.
		virtual bool matches(const InputSource& other) const;

		/*
		returns whether or not the source of the event was the exact input source represented by this object
		*/
		virtual bool matches(const sf::Event&) const;

		//NOTE: CURRENT IMPLIMENTATION ONLY ATTEMPTS TO BIND/BE COMPATIBLE WITH PRESS EVENTS, NOT RELEASE, CONSIDER WHETHER OR NOT THAT MAY NEED CHANGING

		/*
		returns whether or not the source of the event was compatible with this sourceinfo type.
		*/
		static bool isCompatibleWith(const sf::Event& event);
		/*
		if the supplied event is compatible, this takes source information from an event, such that the button/stick/key associated with that event becomes the source.
		if the event was not compatable, the data remains unchanged.

		returns whether or not the event was compatible.
		*/
		virtual bool bindToEventSource(const sf::Event& event);
		
		//post: the source will no longer have valid data
		virtual void clear();

		virtual ~JoystickButtonSource() = default;
	};
}