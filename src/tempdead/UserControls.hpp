#pragma once
#include "InputType.hpp"
#include "definitions.hpp"
#include "KBMBindingSet.hpp"
#include "GamepadBindingSet.hpp"
#include <functional>
namespace __GAME_NAMESPACE__
{
	//class for the controls mapped to particular user input (lighter than the binding map itself)
	class UserControls: public EventListener<sf::Event>
	{
	public:
		enum InputMode
		{
			KBM,
			Gamepad
		};
	private:
		InputMode currentInputMode;
		KBMBindingSet KBMBindings;
		GamepadBindingSet gamepadBindings;
	public:
		//pre: none, indicates how the user is giving inputs.
		InputMode getMode();
		//these methods are for in-game use.
		//pre-condition: none, works in kbm and gamepad modes.
		sf::Vector2f getThrust() const; //movement velocity, etc. not sure if I want to use SFML's -100/100 max/mins yet. probably will by default.

		//pre-condition: mode != KBM, in KMB mode the aim should be calculated in the engine as: the vector representing the mouse cursor position relative to center of the player's ship, normalised to the range for the Axis input type.
		sf::Vector2f getAim() const;

		//true if the fire/trigger button is held in kbm mode, or if the magnitude of thrust is greater than 0.
		bool isTriggerPulled() const;

		void onEvent(const sf::Event&); //will call retreive event and then actually call back onevent(usercontrolevent) of other classes, but also processes the event itself, as it dictates the current input mode, which is used to get values of inputs etc.

		UserControls();
		~UserControls();
	};

}