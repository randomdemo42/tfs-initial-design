#include "BindingUpdateManager.hpp"

namespace __GAME_NAMESPACE__
{
	BindingUpdateManager::BindingUpdateManager() : currentTarget(nullptr)
	{}

	bool BindingUpdateManager::isPendingBind() const
	{
		return currentTarget != nullptr;
	}

	const InputBinding<>* BindingUpdateManager::getCurrentTarget() const
	{
		BindingUpdateManager::
	}

	void BindingUpdateManager::setPendingBind(InputBinding<>& newTarget)
	{
		currentTarget = &newTarget;
	}

	void BindingUpdateManager::cancelPendingBind()
	{
		currentTarget = nullptr;
	}

	bool BindingUpdateManager::bindToEventSource(const sf::Event&);
}