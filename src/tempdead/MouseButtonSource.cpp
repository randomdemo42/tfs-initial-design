#include "MouseButtonSource.hpp"

namespace __GAME_NAMESPACE__
{
	/*
	MouseButtonSource::MouseButtonSource(const TypeID& comparableTypeId)
		: InputSource(comparableTypeId), button(), _buttonIsValid(false)
	{}

	MouseButtonSource::MouseButtonSource(const TypeID& comparableTypeId, sf::Mouse::Button button)
		: InputSource(comparableTypeId, "MouseButton_" + getButtonCodeName(button)), button(button), _buttonIsValid(true)
	{}

	MouseButtonSource::MouseButtonSource(const TypeID& comparableTypeId, sf::Mouse::Button, const sf::String name)
		: InputSource(comparableTypeId, name), button(button), _buttonIsValid(true)
	{}
	*/
	MouseButtonSource::MouseButtonSource()
		: InputSource(getTypeId<MouseButtonSource>()), button(), _buttonIsValid(false)
	{}

	MouseButtonSource::MouseButtonSource(sf::Mouse::Button button)
		: InputSource(getTypeId<MouseButtonSource>(), "MouseButton." + getButtonCodeName(button)), button(button), _buttonIsValid(true)
	{}
	MouseButtonSource::MouseButtonSource(sf::Mouse::Button, const sf::String name)
		: InputSource(getTypeId<MouseButtonSource>(), name), button(button), _buttonIsValid(true)
	{}

	bool MouseButtonSource::matches(const MouseButtonSource& other)
	{
		//if one or both aren't valid, just return false, we want to enable infinite unbound sources? and a source can be checked for being unbound by just checking if it is invalid
		return hasValidData() && other.hasValidData() ? button == other.button : false;
	}

	bool MouseButtonSource::matches(const InputSource& other) const
	{
		return isSameTypeAs(other) ? false : reinterpret_cast<decltype(*this)>(other).matches(*this);
	}

	bool MouseButtonSource::matches(const sf::Event& event) const
	{
		if (isCompatibleWith(event))
		{
			return hasValidData() && button == event.mouseButton.button;
		}
		else
		{
			return false;
		}
	}

	bool MouseButtonSource::hasValidData() const
	{
		return _buttonIsValid;
	}

	void MouseButtonSource::clear()
	{
		_buttonIsValid = false;
	}

	sf::Mouse::Button MouseButtonSource::getButton() const
	{
		return button;
	}

	void MouseButtonSource::setButton(sf::Mouse::Button button)
	{
		this->button = button;
		setName("MouseButton." + getButtonCodeName(button));

		if (!_buttonIsValid)
		{
			_buttonIsValid = true;
		}
	}

	void MouseButtonSource::setData(sf::Mouse::Button button, sf::String name)
	{
		this->button = button;
		setName(name);
	}

	bool MouseButtonSource::isCompatibleWith(const sf::Event& event)
	{
		return event.type == sf::Event::MouseButtonPressed || sf::Event::MouseButtonReleased;
	}

	bool MouseButtonSource::bindToEventSource(const sf::Event& event)
	{
		if (isCompatibleWith(event))
		{
			setButton(event.mouseButton.button);
			return true;
		}
		else
		{
			return false;
		}
	}

	std::string MouseButtonSource::getButtonCodeName(sf::Mouse::Button button)
	{
		switch (sf::Mouse::Button buttonCode = button)
		{
		case sf::Mouse::Button::Left:
			return "Left";
			break;
		case sf::Mouse::Button::Right:
			return "Right";
			break;
		case sf::Mouse::Button::Middle:
			return "Middle";
			break;
		case sf::Mouse::Button::XButton1:
			return "XButton1";
			break;
		case sf::Mouse::Button::XButton2:
			return "XButton2";
			break;
		default:
			return "N/A";
			break;
		}
	}

}