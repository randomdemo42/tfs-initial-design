#include "InputSource.hpp"

//NOTICE: DEPRECATED CLASS
namespace __GAME_NAMESPACE__
{
	const sf::String InputSource::NULLSOURCE_NAME = "N/A";
	sf::String InputSource::getName() const
	{
		return name;
	}
	
	void InputSource::setName(const sf::String& name)
	{
		this->name = name;
	}

	bool InputSource::isSameTypeAs(const InputSource& other) const
	{
		return comparableTypeId == other.comparableTypeId;
	}

	InputSource::InputSource(const TypeID& comparableTypeId, const sf::String& name) : comparableTypeId(comparableTypeId), name(name)
	{}

	InputSource::InputSource() : comparableTypeId(getTypeId<InputSource>()), name("") //create and store the TypeID on construction for efficiency purposes?
	{}
}
*/