#pragma once
#include "BackSteppingMenuState.hpp"
#include "StateMachine.hpp"
#include "DemoBindingSet.hpp"
namespace __GAME_NAMESPACE__
{
	class ControlsMenuState :
		public BackSteppingMenuState
	{
		StateMachine<MenuState>& menuMachine;

		bool hasPendingBinding();
	protected:
		virtual void draw(sf::RenderTarget&, sf::RenderStates) const;
	public:
		ControlsMenuState(const sf::Font& defaultFont, StateMachine<MenuState>& menuMachine, const std::function<void()>& returnFunc);
		virtual ~ControlsMenuState();
		virtual void onEvent(const sf::Event&);
	};
}