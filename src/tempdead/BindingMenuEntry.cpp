#include "BindingMenuEntry.hpp"

namespace __GAME_NAMESPACE__
{
	BindingMenuEntry::BindingMenuEntry(const sf::Font& defaultFont, InputBinding<>& target, InputBinding<>*& pendingSlot, const sf::Vector2f& position)
		: target(target), pendingSlot(pendingSlot), nameBox(defaultFont, target.getName()), rebindButton(defaultFont, "[unbound]")
	{
		setPosition(position);
		rebindButton.setActivateFunc([&]{ pendingSlot = &target; rebindButton.setString("[waiting for input]");  });
	}

	sf::FloatRect BindingMenuEntry::getBounds() const
	{
		sf::FloatRect eachBounds = nameBox.getBounds();
		sf::FloatRect result = eachBounds; //initial bounds of the text box

		//add the size of the button.
		eachBounds = rebindButton.getBounds();
		if (eachBounds.width > result.width)
		{
			result.width = eachBounds.width;
		}
		result.height += eachBounds.height;
		return result;
	}

	void BindingMenuEntry::setPosition(const sf::Vector2f& position)
	{
		sf::FloatRect nameBounds;
		nameBox.setPosition(position);
		nameBounds = nameBox.getBounds();
		rebindButton.setPosition(sf::Vector2f(nameBounds.left, nameBounds.top + nameBounds.height));
	}

	void BindingMenuEntry::onEvent(const MenuEvent& event)
	{
		rebindButton.onEvent(event);
	}
	
	void BindingMenuEntry::update()
	{
		if (pendingSlot != &target)
		{
			rebindButton.setString(target.hasValidData() ? target.getSourceName() : "[unbound]");
		}
	}
	
	sf::Vector2f BindingMenuEntry::getPosition() const
	{
		return nameBox.getPosition();
	}

	void BindingMenuEntry::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		target.draw(nameBox, states);
		target.draw(rebindButton, states);
#if RENDER_DEBUGGING
		GUIObject::drawDebug(target, states);
#endif	
	}

}