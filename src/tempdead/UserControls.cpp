#include "UserControls.hpp"

namespace __GAME_NAMESPACE__
{
	UserControls::UserControls()
	{
	}

	UserControls::~UserControls()
	{
	}

	sf::Vector2f UserControls::getThrust() const
	{
		return currentInputMode == InputMode::KBM ? KBMBindings.getThrust() : gamepadBindings.getThrust();
	}

	sf::Vector2f UserControls::getAim() const
	{
		return gamepadBindings.getAim();
	}

	bool UserControls::isTriggerPulled() const
	{
		return currentInputMode == InputMode::KBM ? KBMBindings.isTriggerPulled() : gamepadBindings.isTriggerPulled();
	}
}