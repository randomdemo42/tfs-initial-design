#include "KBMBindingSet.hpp"
namespace __GAME_NAMESPACE__
{

	KBMBindingSet::KBMBindingSet() : left("Move Left"), up("Move Up"), right("Move Right"), down("Move Down"), trigger("Weapon Trigger"), pause("Pause"), confirm("Confirm"), back("Back"), navLeft("Navigate Left"), navUp("Navigate Up"), navRight("Navigate Right"), navDown("Navigate Down"),
		buttonStates()
	{

		for (int i = 0; i < BindingSet::ButtonBindingCodeCount; i++)
		{
			buttonStates[i] = false;
		}

		//start with customizable bindings, fill it first, then copy-assign to, and extend allBindings.
		customizableBindings.push_back(&left);
		customizableBindings.push_back(&up);
		customizableBindings.push_back(&right);
		customizableBindings.push_back(&down);
		customizableBindings.push_back(&trigger);
		allBindings = customizableBindings;
		allBindings.push_back(&pause);
		allBindings.push_back(&confirm);
		allBindings.push_back(&back);

		eventCompatibleBindings.insert(std::pair<BindingSet::ButtonBindingCode, InputBinding<>&>(BindingSet::ButtonBindingCode::Pause, pause));
		eventCompatibleBindings.insert(std::pair<BindingSet::ButtonBindingCode, InputBinding<>&>(BindingSet::ButtonBindingCode::Confirm, confirm));
		eventCompatibleBindings.insert(std::pair<BindingSet::ButtonBindingCode, InputBinding<>&>(BindingSet::ButtonBindingCode::Back, back));
		eventCompatibleBindings.insert(std::pair<BindingSet::ButtonBindingCode, InputBinding<>&>(BindingSet::ButtonBindingCode::NavLeft, navLeft));
		eventCompatibleBindings.insert(std::pair<BindingSet::ButtonBindingCode, InputBinding<>&>(BindingSet::ButtonBindingCode::NavUp, navUp));
		eventCompatibleBindings.insert(std::pair<BindingSet::ButtonBindingCode, InputBinding<>&>(BindingSet::ButtonBindingCode::NavDown, navDown));
		eventCompatibleBindings.insert(std::pair<BindingSet::ButtonBindingCode, InputBinding<>&>(BindingSet::ButtonBindingCode::NavRight, navRight));
	}

	std::vector<InputBinding<>*> KBMBindingSet::getCustomizableBindings()
	{
		return customizableBindings;
	}

	bool KBMBindingSet::retreiveEvent(const sf::Event& source, UserControlsEvent& destination)
	{
		BindingSet::ButtonBindingCode eachCode;
		for (auto It = eventCompatibleBindings.begin(); It != eventCompatibleBindings.end(); It++)
		{
			if ((*It).second.matches(source))
			{
				eachCode = (*It).first;

				switch (sf::Event::EventType eventType = source.type)
				{
				case sf::Event::KeyPressed:
				case sf::Event::MouseButtonPressed:
					if (!buttonStates[eachCode])
					{
						buttonStates[eachCode] = true;
						
						destination.type = UserControlsEvent::EventType::BoundButtonPressed;
						destination.boundButton.code = eachCode;
						return true;
					}
					break;
				case sf::Event::KeyReleased:
				case sf::Event::MouseButtonReleased:
					buttonStates[eachCode] = false;
					
					destination.type = UserControlsEvent::EventType::BoundButtonReleased;
					destination.boundButton.code = eachCode;
					return true;

					break;
				}

				//break out of the for loop anyway, since we are disallowing duplicate binds, where it's relivant.
				break;
			}
		}

		return false;
	}

	sf::Vector2f KBMBindingSet::getThrust() const
	{
		return sf::Vector2f(float(right.getValue() - left.getValue()), float(down.getValue() - up.getValue()))*100.f;
	}

	bool KBMBindingSet::isTriggerPulled() const
	{
		return trigger.getValue();
	}
}