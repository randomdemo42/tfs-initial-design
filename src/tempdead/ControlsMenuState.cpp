#include "ControlsMenuState.hpp"
#include "BindingMenuEntry.hpp"
namespace __GAME_NAMESPACE__
{

	ControlsMenuState::ControlsMenuState(const sf::Font& defaultFont, StateMachine<MenuState>& menuMachine, const std::function<void()>& backFunc) : BackSteppingMenuState(defaultFont, "Input Settings", backFunc, sf::Vector2f(20, 20)), menuMachine(menuMachine)
	{
		std::vector<InputBinding<>*> temp = bindings.getBindings();
		for (auto It = temp.begin(); It != temp.end(); It++)
		{
			menu.addItem<BindingMenuEntry>(defaultFont, **It, pendingBindingSlot);
		}
	}


	ControlsMenuState::~ControlsMenuState()
	{
	}

	void ControlsMenuState::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		MenuState::draw(target, states);
	}

	bool ControlsMenuState::hasPendingBinding()
	{
		return pendingBindingSlot != nullptr;
	}

	void ControlsMenuState::onEvent(const sf::Event& event)
	{
		if (hasPendingBinding())
		{
			if (event.type == sf::Event::EventType::KeyPressed && event.key.code == sf::Keyboard::Escape)
			{
				pendingBindingSlot = nullptr;
			}
			else if (pendingBindingSlot->bindToEventSource(event))
			{
				
				pendingBindingSlot = nullptr;			
			}
		}
		else
		{
			MenuState::onEvent(event);
		}
	}
}