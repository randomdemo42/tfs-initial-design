#include "JoystickAxisSource.hpp"

namespace __GAME_NAMESPACE__
{

	JoystickAxisSource::JoystickAxisSource()
		: InputSource(getTypeId<JoystickAxisSource>()), axis(), _axisIsValid(false)
	{}
	JoystickAxisSource::JoystickAxisSource(sf::Joystick::Axis axis)
		: InputSource(getTypeId<JoystickAxisSource>(), "JoystickAxis." + getAxisCodeName(axis)), axis(axis), _axisIsValid(true)
	{}
	JoystickAxisSource::JoystickAxisSource(sf::Joystick::Axis axis, const sf::String& name)
		: InputSource(getTypeId<JoystickAxisSource>(), name), axis(axis), _axisIsValid(true)
	{}

	//returns whether or not the object is in a valid state.
	bool JoystickAxisSource::hasValidData() const
	{
		return _axisIsValid;
	}

	//pre-condition: hasValidData()
	sf::Joystick::Axis JoystickAxisSource::getAxis() const
	{
		return axis;
	}

	void JoystickAxisSource::clear()
	{
		_axisIsValid = false;
	}

	void JoystickAxisSource::setAxis(sf::Joystick::Axis axis)
	{
		this->axis = axis;
		setName("JoystickAxis." + getAxisCodeName(axis));
		if (!hasValidData())
		{
			_axisIsValid = true;
		}
	}
	void JoystickAxisSource::setData(sf::Joystick::Axis axis, const sf::String& name)
	{
		this->axis = axis;
		setName(name);
		if (!hasValidData())
		{
			_axisIsValid = true;
		}
	}


	bool JoystickAxisSource::matches(const JoystickAxisSource& other) const
	{
		//if one or both aren't valid, just return false, we want to enable infinite unbound sources? and a source can be checked for being unbound by just checking if it is invalid
		return hasValidData() && other.hasValidData() ? axis == other.axis : false;
	}

	
	bool JoystickAxisSource::matches(const InputSource& other) const
	{
		return isSameTypeAs(other) ? false : reinterpret_cast<decltype(*this)>(other).matches(*this);
	}

	bool JoystickAxisSource::matches(const sf::Event& event) const
	{
		if (isCompatibleWith(event))
		{
			return hasValidData() && axis == event.joystickMove.axis;
		}
		else
		{
			return false;
		}
	}

	bool JoystickAxisSource::isCompatibleWith(const sf::Event& event)
	{
		return event.type == sf::Event::EventType::JoystickMoved;
	}
	
	bool JoystickAxisSource::bindToEventSource(const sf::Event& event)
	{
		if (isCompatibleWith(event))
		{
			setAxis(event.joystickMove.axis);
			return true;
		}
		else
		{
			return false;
		}
	}

	std::string JoystickAxisSource::getAxisCodeName(sf::Joystick::Axis axis)
	{
		switch (auto axisCode = axis)
		{
		case sf::Joystick::X:
			return "X";
			break;
		case sf::Joystick::Y:
			return "Y";
			break;
		case sf::Joystick::Z:
			return "Z";
			break;
		case sf::Joystick::R:
			return "R";
			break;
		case sf::Joystick::U:
			return "U";
			break;
		case sf::Joystick::V:
			return "V";
			break;
		case sf::Joystick::PovX:
			return "PovX";
			break;
		case sf::Joystick::PovY:
			return "PovY";
			break;
		default:
			return "N/A";
			break;
		}
	}
}