#pragma once
#include "definitions.hpp"
#include "InputType.hpp"
#include <SFML/System/String.hpp>
#include <SFML/Window/Event.hpp>
#include "InputSource.hpp"
#include <functional>
#include <memory>
#include "EventListener.hpp"
namespace __GAME_NAMESPACE__
{
	
	//the inputsource class differs from the source-information class in that it can be used to capture input sources of different types and translate them into a different type? dunno if we'll end up using it like that though, it would have mainly been useful for events. 
	template<typename InputType type = InputType::Unknown>
	class InputBinding;

	template<>
	class InputBinding<>
	{
		sf::String name;
		sf::String sourceName;

	public:
		InputBinding(const sf::String& name);
		virtual bool hasSource() const = 0;

		sf::String getName() const;
		void setName(const sf::String& name);

		//pre: hasSource()
		virtual sf::String getSourceName() const;

		/*
			returns whether or not the source of the event was the exact input source represented by this object
		*/
		virtual bool matches(const sf::Event&) const;

		virtual bool bindToEventSource(const sf::Event&) = 0;

		//post: the source will no longer be in a valid state. useful if you want bindingsets with every binding to a unique source, etc
		virtual void clear() = 0; 

		virtual ~InputBinding<>() = default;
	};

	template<typename InputType type>
	class InputBinding : public InputBinding<>
	{
	public:
		static const InputType type = type;
		
		InputBinding(const sf::String& name);

		virtual bool matches(const InputBinding& other) const;

		//if no source currently exists, the default value will be returned
		virtual typename InputTypeTraits<type>::ValueType getValue() const = 0;
		virtual ~InputBinding() = default;
	};

	template<typename InputType type>
	InputBinding<type>::InputBinding(const sf::String& name)
		: InputBinding<>(name)
	{}
}