#pragma once
#include "InputSource.hpp"
namespace __GAME_NAMESPACE__
{
	//deprecated class.
	//differs from the other "JoystickAxisSource" in that this object keeps track of the joystick id individually (but this is actually impractical just from the ID as it requires tracking device order etc)
	class JoystickSpecificAxisInformation :
		public InputSource
	{
	public:
		static std::string getAxisCodeName(sf::Joystick::Axis axis);
	private:
		unsigned int joystickId;
		sf::Joystick::Axis axis;
		bool _joystickIdIsValid;
		bool _axisIsValid;

		bool matches(const JoystickSpecificAxisInformation&) const;
	public:
		JoystickSpecificAxisInformation();
		JoystickSpecificAxisInformation(unsigned int joystickId, sf::Joystick::Axis axis);
		JoystickSpecificAxisInformation(unsigned int joystickId, sf::Joystick::Axis axis, const sf::String& name);

		//returns whether or not the object is in a valid state.
		virtual bool hasValidData() const;

		//pre-condition: hasValidData()
		unsigned int getJoystickId() const;

		//pre-condition: hasValidData()
		sf::Joystick::Axis getAxis() const;

		void setData(unsigned int joystickId, sf::Joystick::Axis axis);
		void setData(unsigned int joystickId, sf::Joystick::Axis axis, const sf::String& name);

		//Important: semi-cheating double-dispatch, see InputSource.hpp for implimentation details.
		virtual bool matches(const InputSource& other) const;

		//NOTE: CURRENT IMPLIMENTATION ONLY ATTEMPTS TO MATCH IF THE JOYSTICK IS MORE THAN 50% IN A GIVEN DIRECTION (NOT NECCESSARILY THE DEADZONE FOR REAL INPUT)

		/*
		returns whether or not the source of the event was the exact input source represented by this object
		*/
		virtual bool matches(const sf::Event&) const;

		/*
		returns whether or not the source of the event was compatible with this sourceinfo type.
		*/
		static bool isCompatibleWith(const sf::Event& event);
		/*
		if the supplied event is compatible, this takes source information from an event, such that the button/stick/key associated with that event becomes the source.
		if the event was not compatable, the data remains unchanged.

		returns whether or not the event was compatible.
		*/
		virtual bool bindToEventSource(const sf::Event& event);

		//post: the source will no longer have valid data
		virtual void clear();

		virtual ~JoystickSpecificAxisInformation() = default;
	};
}
