#include "GamepadBindingSet.hpp"
#include <cmath>
namespace __GAME_NAMESPACE__
{
	GamepadBindingSet::GamepadBindingSet()
		: deadZone((InputTypeTraits<Button>::maxValue - InputTypeTraits<Button>::defaultValue) / 3.f /*one third*/), joystickId(-1), useSticksForNav(true), pause("Pause", joystickId), thrustX("Thrust X-Axis (Left/Right)", joystickId), thrustY("Thrust Y-Axis (Up/Down)", joystickId), aimX("Aim X-Axis (Left/Right)", joystickId), aimY("Aim Y-Axis (Up/Down)", joystickId),
		confirm("Confirm", joystickId), back("Back/Cancel", joystickId),
		navLeft("Navigate Left", joystickId), navUp("Navigate Up", joystickId), navRight("Navigate Right", joystickId), navDown("Navigate Down", joystickId),
		buttonStates()
	{
		for (int i = 0; i < BindingSet::ButtonBindingCodeCount; i++)
		{
			buttonStates[i] = false;
		}

		inGameCustomizableBindings.push_back(&pause);
		inGameCustomizableBindings.push_back(&thrustX);
		inGameCustomizableBindings.push_back(&thrustY);
		inGameCustomizableBindings.push_back(&aimX);
		inGameCustomizableBindings.push_back(&aimY);

		menuCustomizableBindings.push_back(&confirm);
		menuCustomizableBindings.push_back(&back);
		menuCustomizableBindings.push_back(&navLeft);
		menuCustomizableBindings.push_back(&navUp);
		menuCustomizableBindings.push_back(&navRight);
		menuCustomizableBindings.push_back(&navDown);

		allBindings.reserve(inGameCustomizableBindings.size() + menuCustomizableBindings.size());
		allBindings.insert(allBindings.end(), inGameCustomizableBindings.begin(), inGameCustomizableBindings.end());
		allBindings.insert(allBindings.end(), menuCustomizableBindings.begin(), menuCustomizableBindings.end());

		eventCompatibleBindings.insert(std::pair<BindingSet::ButtonBindingCode, InputBinding<>&>(BindingSet::ButtonBindingCode::Pause, pause));
		eventCompatibleBindings.insert(std::pair<BindingSet::ButtonBindingCode, InputBinding<>&>(BindingSet::ButtonBindingCode::Confirm, confirm));
		eventCompatibleBindings.insert(std::pair<BindingSet::ButtonBindingCode, InputBinding<>&>(BindingSet::ButtonBindingCode::Back, back));
		eventCompatibleBindings.insert(std::pair<BindingSet::ButtonBindingCode, InputBinding<>&>(BindingSet::ButtonBindingCode::NavLeft, navLeft));
		eventCompatibleBindings.insert(std::pair<BindingSet::ButtonBindingCode, InputBinding<>&>(BindingSet::ButtonBindingCode::NavUp, navUp));
		eventCompatibleBindings.insert(std::pair<BindingSet::ButtonBindingCode, InputBinding<>&>(BindingSet::ButtonBindingCode::NavDown, navDown));
		eventCompatibleBindings.insert(std::pair<BindingSet::ButtonBindingCode, InputBinding<>&>(BindingSet::ButtonBindingCode::NavRight, navRight));
	}

	//want to be able to get them seperately for UI purposes, etc, but also still use them together, we'll see how use the seperation is..
	std::vector<InputBinding<>*> GamepadBindingSet::getCustomizableInGameBindings()
	{
		return inGameCustomizableBindings;
	}

	std::vector<InputBinding<>*> GamepadBindingSet::getCustomizableMenuBindings()
	{
		return menuCustomizableBindings;
	}

	//has basically 1.5-2 the execution time complexity of just inserting them into the one vector, but eh, less likely to get bugs, etc.
	std::vector<InputBinding<>*> GamepadBindingSet::getCustomizableBindings()
	{
		std::vector<InputBinding<>*> inGameBindings = getCustomizableInGameBindings();
		std::vector<InputBinding<>*> menuBindings = getCustomizableMenuBindings();
		inGameBindings.reserve(inGameBindings.size() + menuBindings.size());
		inGameBindings.insert(inGameBindings.end(), menuBindings.begin(), menuBindings.end());
		return inGameBindings;
	}

	float GamepadBindingSet::getDeadzone() const
	{
		return deadZone;
	}

	void GamepadBindingSet::setDeadZone(float newZone)
	{
		deadZone = newZone;
	}

	//whether or not the joystick inputs for movement and aim should be usable for navigation, defaults to true;
	bool GamepadBindingSet::getCanUseSticksForNav() const
	{
		return useSticksForNav;
	}

	void GamepadBindingSet::setCanUseSticksForNav(bool newValue)
	{
		useSticksForNav = newValue;
	}

	bool GamepadBindingSet::retreiveEvent(const sf::Event& source, UserControlsEvent& destination)
	{
		BindingSet::ButtonBindingCode eachCode;
		for (auto It = eventCompatibleBindings.begin(); It != eventCompatibleBindings.end(); It++)
		{
			if ((*It).second.matches(source))
			{
				eachCode = (*It).first;

				switch (sf::Event::EventType eventType = source.type)
				{
				case sf::Event::JoystickMoved:
					if (std::abs(source.joystickMove.position) > deadZone)
					{
						if (!buttonStates[eachCode])
						{
							buttonStates[eachCode] = true;

							destination.type = UserControlsEvent::EventType::BoundButtonPressed;
							destination.boundButton.code = eachCode;
							return true;
						}
					}
					else
					{
						if (buttonStates[eachCode])
						{
							buttonStates[eachCode] = false;
							destination.type = UserControlsEvent::EventType::BoundButtonReleased;
							destination.boundButton.code = eachCode;
							return true;
						}
					}
				case sf::Event::JoystickButtonPressed:
					if (!buttonStates[eachCode])
					{
						buttonStates[eachCode] = true;

						destination.type = UserControlsEvent::EventType::BoundButtonPressed;
						destination.boundButton.code = eachCode;
						return true;
					}
					break;
				case sf::Event::JoystickButtonReleased:
					buttonStates[eachCode] = false;

					destination.type = UserControlsEvent::EventType::BoundButtonReleased;
					destination.boundButton.code = eachCode;
					return true;

					break;
				}

				//break out of the for loop anyway, since we are disallowing duplicate binds, where it's relivant.
				break;
			}
		}
		return false;
	}


	sf::Vector2f GamepadBindingSet::getThrust() const
	{
		sf::Vector2f thrust(thrustX.getValue(), thrustY.getValue());
		return std::abs(thrust.x) > deadZone || std::abs(thrust.y) > deadZone ? thrust : sf::Vector2f();
	}

	sf::Vector2f GamepadBindingSet::getAim() const
	{
		sf::Vector2f aim(aimX.getValue(), aimY.getValue());
		return std::abs(aim.x) > deadZone || std::abs(aim.y) > deadZone ? aim : sf::Vector2f();
	}

	bool  GamepadBindingSet::isTriggerPulled() const
	{
		sf::Vector2f aim(aimX.getValue(), aimY.getValue());
		return std::abs(aim.x) > deadZone || std::abs(aim.y) > deadZone;
	}
}