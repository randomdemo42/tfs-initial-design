#include "MouseCursorSource.hpp"

namespace __GAME_NAMESPACE__
{
	/*
	MouseCursorSource::MouseCursorSource(const TypeID& comparableTypeId)
	: InputSource(comparableTypeId)
	{}
	*/
	MouseCursorSource::MouseCursorSource()
		: InputSource(getTypeId<MouseCursorSource>(), "Mouse Cursor")
	{
	}

	bool MouseCursorSource::matches(const MouseCursorSource& other) const
	{
		return true;
	}

	bool MouseCursorSource::matches(const InputSource& other) const
	{
		return isSameTypeAs(other) ? false : reinterpret_cast<decltype(*this)>(other).matches(*this);
	}

	bool MouseCursorSource::matches(const sf::Event& event) const
	{
		return isCompatibleWith(event);
	}

	bool MouseCursorSource::isCompatibleWith(const sf::Event& event)
	{
		return event.type == sf::Event::EventType::MouseMoved;
	}

	bool MouseCursorSource::bindToEventSource(const sf::Event& event)
	{
		return isCompatibleWith(event);
	}

	bool MouseCursorSource::hasValidData() const
	{
		return true;
	}

	void MouseCursorSource::clear()
	{}
}
