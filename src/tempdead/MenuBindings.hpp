#pragma once
#include "InputType.hpp"
#include "definitions.hpp"
#include <SFML/Window/Event.hpp>
#include "MenuEvent.hpp"
#include "Bindings.hpp"
#include "foreach_macro.hpp"

#define DEFER(...) __VA_ARGS__
#define ADD_KEY_TO_ENUM(KEY, TYPE) KEY,

//note that this currently assumes binding classes should all default construct (and for now they should)
#define MakeBindingClass(Name, ...)\
enum Name##Keys{ FOR_EACH(ADD_KEY_TO_ENUM, __VA_ARGS__)};\
class Name##BindingBase\
{\
public:\
	typedef Name##Keys Keys;\
	FOR_EACH(INSTANTIATE_BIND, DEFER(__VA_ARGS__))\
public:\
	FOR_EACH(SPECIALISE_GETTER, __VA_ARGS__)\
	Name##BindingBase(){};\
};


namespace __GAME_NAMESPACE__
{
	
	
}