#pragma once
#include "definitions.hpp"
#include "InputType.hpp"
#include <functional>
#include <SFML/System/String.hpp>
#include "InputSource.hpp"
namespace __GAME_NAMESPACE__
{
	//DEPRECATED version of a class that was merged into another instead.
	template< >
	class InputBinding;
	template<>

	class InputBinding <>
	{
		
	public:
		InputBinding(const sf::String& name);

		InputBinding(const InputBinding&) = delete;
		InputBinding(InputBinding&&) = delete;
		InputBinding& operator=(const InputBinding&) = delete;
		InputBinding& operator=(InputBinding&&) = delete;

		sf::String getName() const;

		virtual bool hasValidSource() const = 0;
		virtual bool bindToEventSource(const sf::Event& event) = 0;

		//post: the source will no longer be in a valid state. useful if you want bindingsets with every binding to a unique source, etc
		virtual void clear() = 0;
	};

	template<typename _SourceType>
	class InputBinding : public InputBinding < >
	{
	public:
		using SourceType = _SourceType ;
	private:
		std::enable_if_t<std::is_base_of<InputSource<>, SourceType>::value, SourceType> source;
	public:
		InputBinding(const sf::String& name);

		virtual bool hasValidSource() const;
		virtual SourceType& getSource();
		virtual const SourceType& getSource() const;
		bool bindToEventSource(const sf::Event& event);
		virtual ~InputBinding() = default;
		virtual void clear();
	};


	template<typename _SourceType>
	InputBinding<_SourceType>::InputBinding(const sf::String& name) : InputBinding<>(name)
	{}

	template<typename _SourceType>
	bool InputBinding<_SourceType>::hasValidSource() const
	{
		return source.hasValidData();
	}

	template<typename _SourceType>
	_SourceType& InputBinding<_SourceType>::getSource()
	{
		return source;
	}

	template<typename _SourceType>
	const _SourceType& InputBinding<_SourceType>::getSource() const
	{
		return source;
	}

	template<typename _SourceType>
	bool InputBinding<_SourceType>::bindToEventSource(const sf::Event& event)
	{
		return source.bindToEventSource(event);
	}

	template<typename _SourceType>
	void InputBinding<_SourceType>::clear()
	{
		source.clear();
	}



}
