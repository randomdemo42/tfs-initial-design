#pragma once
#include "InputBinding.hpp"
#include "JoystickButtonSource.hpp"
#include "JoystickAxisSource.hpp"
namespace __GAME_NAMESPACE__
{

	//note: this class is intended to be a non-dynamically allocated fixture in a bindingset which stores and manages the joystickId; other usage may be difficult to maintain.
	/*
		letting each of these objects maintain there own  joystickId may be perfectly doable, any efficient enough, but there don't seem to be any logical benifits. Note: it would decrease coupling, but this is more closely related to a binding than the hardware, so the coupling makes sense.
		note: the class naming/structure could possibly use improving, to make this a binding class instead of a source class, rename SourceInfo to source, etc. consider doing this!
	*/
	class GamepadButtonBinding
		: public InputBinding < Button >
	{
	private:
		enum SourceType
		{
			JoystickButton,
			JoystickAxis,
			Unknown
		};
		union Source
		{
			JoystickButtonSource* button;
			JoystickAxisSource* axis;
		};
		Source source;
		SourceType activeType;

		/*
			note: despite being a const the joystickId must be point to valid, existing data for the lifetime of this object.
			if it bound to a temporary, or the data is deleted, this class becomes permanantly invalid and all usage will be undefined behaviour.
		*/
		//the ID may change, but this class garauntees not to change it, other than through the ass
		std::reference_wrapper<const unsigned int> joystickId;


		void setSourceType(SourceType newType);

		//note the duplicated code with the two getSourceInfo() here, and to be careful!
		//pre: hasSourceInfo()
		virtual InputSource& getSource();

		//pre: hasSourceInfo()
		virtual const InputSource& getSource() const;
	public:
		/*
		note: despite being a const the joystickId must be point to valid, existing data for the lifetime of this object.
		if it bound to a temporary, or the data is deleted, this class becomes permanantly invalid and all usage will be undefined behaviour.
		*/
		GamepadButtonBinding(const sf::String& name, const unsigned int& joystickId);

		//not neccessarily impossible (although probably, due to the joystickID reference), but disabling now due to non-triviality, will impliment if it becomes appropriate or neccessary. Not sure if the default move is appropriate, but it probably is.
		GamepadButtonBinding(const GamepadButtonBinding&) = delete;
		GamepadButtonBinding& operator=(const GamepadButtonBinding&) = delete;

		GamepadButtonBinding(GamepadButtonBinding&&);
		GamepadButtonBinding& operator=(GamepadButtonBinding&&);

		virtual bool hasSource() const;

		//defaults to constantly giving the default-value of the type if the source info isn't existing and valid.
		//virtual std::function<InputTypeTraits < type >::ValueType()> getValueRetriever() const;

		virtual InputTypeTraits < type >::ValueType getValue() const;

		static bool isCompatibleWith(const sf::Event& event);

		virtual bool bindToEventSource(const sf::Event& event);

		virtual void clear();

		virtual ~GamepadButtonBinding();
	};
}