#pragma once
#include <type_traits>
#include "definitions.hpp"
#include <SFML/Window/Event.hpp>
namespace __GAME_NAMESPACE__
{

	template<typename DeviceIDType, typename ComponentIDType>
	class DeviceComponentID
	{
	private:
		ComponentIDType data;
	public:
		template<typename EventType>
		//default to false, override where true?
		struct IsCompatibleWith
			: public std::false_type
		{}; //defaults to false, override where possible, 

		DeviceComponentID(const ComponentIDType&);

		DeviceComponentID(const DeviceComponentID&); //copy constructor
		DeviceComponentID(DeviceComponentID&&); //copy assignment
		DeviceComponentID& operator=(DeviceComponentID);
		DeviceComponentID& operator=(DeviceComponentID&&);


		template<typename EventType>
		bool setToMatch(const EventType&);  //this has no default!, only overload where possibly compatible

		template<typename EventType>
		//whether or not the current data matches the supplied event
		bool matches(const EventType&);  //no default!, only overload where possibly compatible

		//whether or not the current data matches the supplied DeviceComponentID. uses == operator such that SourceIDs can be recycled as DeviceIDs and ComponentIDs, though not sure if that's useful, does seem interesting?
		bool matches(const DeviceComponentID&); //this has full default

		//whether or not the current data matches the supplied DeviceComponentID. uses == operator such that SourceIDs can be recycled as DeviceIDs and ComponentIDs, though not sure if that's useful, does seem interesting?
		bool operator==(const DeviceComponentID&); //this has full default

		//gets the current value of the source.
		template<typename ValueType>
		friend ValueType getSourceValue(const DeviceComponentID&); //only one will exist, doesn't matter what, kind of like an auto I guess.
	};

	template<typename DeviceIDType, typename ComponentIDType>
	DeviceComponentID<DeviceIDType, ComponentIDType>::DeviceComponentID(const ComponentIDType& data) : data(data)
	{}


	template<typename DeviceIDType, typename ComponentIDType>
	DeviceComponentID<DeviceIDType, ComponentIDType>::DeviceComponentID(const DeviceComponentID& src) : data(src.data)
	{}

	template<typename DeviceIDType, typename ComponentIDType>
	DeviceComponentID<DeviceIDType, ComponentIDType>& DeviceComponentID<DeviceIDType, ComponentIDType>::operator=(DeviceComponentID src)
	{
		data = std::swap(data, src.data);
	}

	template<typename DeviceIDType, typename ComponentIDType>
	DeviceComponentID<DeviceIDType, ComponentIDType>& DeviceComponentID<DeviceIDType, ComponentIDType>::operator=(DeviceComponentID&& src)
	{
		data = std::move(src.data);
	}

	template<typename DeviceIDType, typename ComponentIDType>
	bool DeviceComponentID<DeviceIDType, ComponentIDType>::operator==(const DeviceComponentID& other)
	{
		return data == other.data;
	}
}