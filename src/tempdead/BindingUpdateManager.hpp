#pragma once
#include "definitions.hpp"
#include "InputBinding.hpp"
namespace __GAME_NAMESPACE__
{

	//note that in reality this is just a reference to a pointer's storage point when it comes down to it.                       
	class BindingUpdateManager
	{
		InputBinding<>* currentTarget;
	public:
		BindingUpdateManager();

		//whether or not there is a binding waiting to updated through this manager.
		bool isPendingBind() const;

		//note: will be nullptr if there is no pending bind.
		const InputBinding<>* getCurrentTarget() const;

		//note: if there was already a bending bind, it will be forgotten in favour of the argument supplied.
		//note: the target sent as an argument must be and remain valid for the lifetime this object. 
		void setPendingBind(InputBinding<>&);

		//if there is bind pending, it will be forgotten. i.e. there will be no pending bind afterwards.
		void cancelPendingBind();

		/*
			if the supplied event is compatible, this takes source information from an event, such that the button/stick/key associated with that event becomes the source.
			if the event was not compatable, the data remains unchanged.

			returns whether or not the event was compatible.
		*/
		bool bindToEventSource(const sf::Event&);
		~BindingUpdateManager() = default;
	};
}
