#pragma once
#include "InputSource.hpp"
namespace __GAME_NAMESPACE__
{

	//TODO: should probably make the deadzone for input adjustable, etc, and utilise that deadzone for a lot of input/binding related things rather than just using 50% in either direction.
	//note: this class is niave to which joystick it is associated with, control over that is left to classes that use this.
	class JoystickAxisSource :
		public InputSource
	{
	public:
		static std::string getAxisCodeName(sf::Joystick::Axis axis);
	private:
		sf::Joystick::Axis axis;
		bool _axisIsValid;

		bool matches(const JoystickAxisSource&) const;
	public:
		JoystickAxisSource();
		JoystickAxisSource(sf::Joystick::Axis axis);
		JoystickAxisSource(sf::Joystick::Axis axis, const sf::String& name);

		//returns whether or not the object is in a valid state.
		virtual bool hasValidData() const;

		//pre-condition: hasValidData()
		sf::Joystick::Axis getAxis() const;

		void setAxis(sf::Joystick::Axis axis);
		void setData(sf::Joystick::Axis axis, const sf::String& name);

		//Important: semi-cheating double-dispatch, see InputSource.hpp for implimentation details.
		virtual bool matches(const InputSource& other) const;

		//NOTE: CURRENT IMPLIMENTATION ONLY ATTEMPTS TO MATCH IF THE JOYSTICK IS MORE THAN 50% IN A GIVEN DIRECTION (NOT NECCESSARILY THE DEADZONE FOR REAL INPUT)

		/*
		returns whether or not the source of the event was the exact input source represented by this object
		*/
		virtual bool matches(const sf::Event&) const;

		/*
		returns whether or not the source of the event was compatible with this sourceinfo type.
		*/
		static bool isCompatibleWith(const sf::Event& event);
		/*
		if the supplied event is compatible, this takes source information from an event, such that the button/stick/key associated with that event becomes the source.
		if the event was not compatable, the data remains unchanged.

		returns whether or not the event was compatible.
		*/
		virtual bool bindToEventSource(const sf::Event& event);

		//post: the source will no longer have valid data
		virtual void clear();

		virtual ~JoystickAxisSource() = default;
	};
}
