#include "GamepadAxisBinding.hpp"

namespace __GAME_NAMESPACE__
{
	GamepadAxisBinding::GamepadAxisBinding(const sf::String& name, const unsigned int& joystickId) : InputBinding<Axis>(name), joystickId(joystickId), source()
	{}

	GamepadAxisBinding::~GamepadAxisBinding()
	{}

	InputSource& GamepadAxisBinding::getSource()
	{
		return source;
	}

	const InputSource& GamepadAxisBinding::getSource() const
	{
		return source;
	}

	bool GamepadAxisBinding::hasSource() const
	{
		return true;
	}

	//defaults to constantly giving the default-value of the type if the source info isn't existing and valid.
	InputTypeTraits < GamepadAxisBinding::type >::ValueType GamepadAxisBinding::getValue() const
	{
		if (hasValidData())
		{
			return sf::Joystick::getAxisPosition(joystickId, source.getAxis());
		}
		else
		{
			return InputTypeTraits<type>::defaultValue;
		}
	}

	bool GamepadAxisBinding::isCompatibleWith(const sf::Event& event)
	{
		return JoystickAxisSource::isCompatibleWith(event);
	}

	bool GamepadAxisBinding::bindToEventSource(const sf::Event& event)
	{
		if ((event.type == sf::Event::EventType::JoystickMoved && event.joystickMove.joystickId != joystickId))
		{
			return false;
		}
		else
		{
			return source.bindToEventSource(event);
		}
	}

	void GamepadAxisBinding::clear()
	{
		source.clear();
	}
}