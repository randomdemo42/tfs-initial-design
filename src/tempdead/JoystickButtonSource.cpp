#include "JoystickButtonSource.hpp"
#include <sstream>
namespace __GAME_NAMESPACE__
{
	/*
	JoystickButtonSource::JoystickButtonSource(const TypeID& comparableTypeId)
	: InputSource(comparableTypeId), joystickId(), buttonId(), _joystickIsValid(false), _buttonIsValid(false)
	{}

	JoystickButtonSource::JoystickButtonSource(const TypeID& comparableTypeId, unsigned int joystickId, unsigned int buttonId)
	: InputSource(comparableTypeId, "JoystickButton_" + getButtonCodeName(buttonId)), joystickId(joystickId), buttonId(buttonId), _joystickIsValid(true), _buttonIsValid(true)
	{}

	JoystickButtonSource::JoystickButtonSource(const TypeID& comparableTypeId, unsigned int joystickId, unsigned int buttonId, const sf::String& name)
	: InputSource(comparableTypeId, name), joystickId(joystickId), buttonId(buttonId), _joystickIsValid(true), _buttonIsValid(true)
	{}
	*/
	JoystickButtonSource::JoystickButtonSource()
		: InputSource(getTypeId<JoystickButtonSource>()), buttonId(), _buttonIsValid(false)
	{}

	JoystickButtonSource::JoystickButtonSource(unsigned int buttonId)
		: InputSource(getTypeId<JoystickButtonSource>(), "JoystickButton." + getButtonCodeName(buttonId)), buttonId(buttonId), _buttonIsValid(true)
	{}

	JoystickButtonSource::JoystickButtonSource(unsigned int buttonId, const sf::String& name)
		: InputSource(getTypeId<JoystickButtonSource>(), name), buttonId(buttonId), _buttonIsValid(true)
	{}

	bool JoystickButtonSource::hasValidData() const
	{
		return _buttonIsValid;
	}

	void JoystickButtonSource::clear()
	{
		_buttonIsValid = false;
	}

	unsigned int JoystickButtonSource::getButtonId() const
	{
		return buttonId;
	}

	void JoystickButtonSource::setButton(unsigned int buttonId)
	{
		this->buttonId = buttonId;
		setName("JoystickButton." + getButtonCodeName(buttonId));
		if (!hasValidData())
		{
			_buttonIsValid = true;
		}
	}
	void JoystickButtonSource::setData(unsigned int buttonId, const sf::String& name)
	{
		this->buttonId = buttonId;
		setName(name);
		if (!hasValidData())
		{
			_buttonIsValid = true;
		}
	}

	bool JoystickButtonSource::matches(const JoystickButtonSource& other) const
	{
		//if one or both aren't valid, just return false, we want to enable infinite unbound sources? and a source can be checked for being unbound by just checking if it is invalid
		return hasValidData() && other.hasValidData() ? buttonId == other.buttonId : false;
	}

	bool JoystickButtonSource::matches(const InputSource& other) const
	{
		return isSameTypeAs(other) ? false : reinterpret_cast<decltype(*this)>(other).matches(*this);
	}

	bool JoystickButtonSource::matches(const sf::Event& event) const
	{
		if (isCompatibleWith(event))
		{
			return hasValidData() && buttonId == event.joystickButton.button;
		}
		else
		{
			return false;
		}
	}

	bool JoystickButtonSource::isCompatibleWith(const sf::Event& event)
	{
		return event.type == sf::Event::EventType::JoystickButtonPressed || event.type == sf::Event::JoystickButtonReleased;
	}

	bool JoystickButtonSource::bindToEventSource(const sf::Event& event)
	{
		if (isCompatibleWith(event))
		{
			setButton(event.joystickButton.button);
			return true;
		}
		else
		{
			return false;
		}
	}

	std::string JoystickButtonSource::getButtonCodeName(unsigned int buttonId)
	{
		std::stringstream stream;
		std::cout << "hi";
		std::cout << buttonId;

		stream << buttonId;
		return stream.str();
	}
}