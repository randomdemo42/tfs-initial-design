#pragma once
#include "InputSource.hpp"
namespace __GAME_NAMESPACE__
{

	class MouseButtonSource :
		public InputSource
	{
	public:
		static std::string getButtonCodeName(sf::Mouse::Button button);
	private:
		sf::Mouse::Button button;
		bool _buttonIsValid;

		bool matches(const MouseButtonSource&);
	protected:
		/*
		MouseButtonSource(const TypeID& comparableTypeId);
		MouseButtonSource(const TypeID& comparableTypeId, sf::Mouse::Button button);
		MouseButtonSource(const TypeID& comparableTypeId, sf::Mouse::Button button, const sf::String name);
		*/
	public:
		MouseButtonSource();
		MouseButtonSource(sf::Mouse::Button button);
		MouseButtonSource(sf::Mouse::Button, const sf::String name);
		
		//returns whether or not the object is in a valid state.
		virtual bool hasValidData() const;
		
		//pre-condition: hasValidButton()
		sf::Mouse::Button getButton() const;
	
		void setButton(sf::Mouse::Button button);
		void setData(sf::Mouse::Button button, sf::String name);
		
		//Important: semi-cheating double-dispatch, see InputSource.hpp for implimentation details.
		virtual bool matches(const InputSource& other) const;

		//NOTE: CURRENT IMPLIMENTATION ONLY ATTEMPTS TO BIND/BE COMPATIBLE WITH PRESS EVENTS, NOT RELEASE, CONSIDER WHETHER OR NOT THAT MAY NEED CHANGING
		
		/*
		returns whether or not the source of the event was the exact input source represented by this object
		*/
		virtual bool matches(const sf::Event&) const;

		/*
		returns whether or not the source of the event was compatible with this sourceinfo type.
		*/
		static bool isCompatibleWith(const sf::Event& event);
		/*
		if the supplied event is compatible, this takes source information from an event, such that the button/stick/key associated with that event becomes the source.
		if the event was not compatable, the data remains unchanged.

		returns whether or not the event was compatible.
		*/
		virtual bool bindToEventSource(const sf::Event& event);

		//post: the source will no longer have valid data
		virtual void clear();

		virtual ~MouseButtonSource() = default;
	};

}