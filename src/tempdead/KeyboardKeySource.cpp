#include "KeyboardKeySource.hpp"

namespace __GAME_NAMESPACE__
{
	/*
	KeyboardKeySource::KeyboardKeySource(const TypeID& comparableTypeId, sf::Keyboard::Key key)
		: InputSource(comparableTypeId, "Key_" + getKeyCodeName(key)), key(key)
	{
	}

	KeyboardKeySource::KeyboardKeySource(const TypeID& comparableTypeId, sf::Keyboard::Key key, const sf::String& name)
		: InputSource(getTypeId<KeyboardKeySource>(), name), key(key)
	{}
	*/

	KeyboardKeySource::KeyboardKeySource(sf::Keyboard::Key key)
		: InputSource(getTypeId<KeyboardKeySource>(), "Key." + getKeyCodeName(key)), key(key)
	{}

	KeyboardKeySource::KeyboardKeySource(sf::Keyboard::Key key, const sf::String& name)
		: InputSource(getTypeId<KeyboardKeySource>(), "Key." + getKeyCodeName(key)), key(key)
	{}


	bool KeyboardKeySource::hasValidData() const
	{
		return key != sf::Keyboard::Unknown;
	}

	void KeyboardKeySource::clear()
	{
		key = sf::Keyboard::Unknown;
	}

	sf::Keyboard::Key KeyboardKeySource::getKey() const
	{
		return key;
	};

	void KeyboardKeySource::setKey(sf::Keyboard::Key key)
	{
		this->key = key;
		setName("Key." + getKeyCodeName(key));
	}

	void KeyboardKeySource::setData(sf::Keyboard::Key key, const sf::String& name)
	{
		this->key = key;
		setName(name);
	}

	bool KeyboardKeySource::matches(const InputSource& other) const
	{
		return isSameTypeAs(other) ? false : reinterpret_cast<decltype(*this)>(other).matches(*this);
	}

	bool KeyboardKeySource::matches(const KeyboardKeySource& other) const
	{
		//if one or both aren't valid, just return false, we want to enable infinite unbound sources? and a source can be checked for being unbound by just checking if it is invalid
		return hasValidData() && other.hasValidData() ? key == other.key : false;
	}
	bool KeyboardKeySource::matches(const sf::Event& event) const
	{
		if (isCompatibleWith(event))
		{
			return hasValidData() && key == event.key.code;
		}
		else
		{
			return false;
		}
	}
	//note: these two below implimentations are likely to need to be ensured to return the same values manually for a number of classes anyway
	bool KeyboardKeySource::isCompatibleWith(const sf::Event& event)
	{
		return event.type == sf::Event::KeyPressed || event.type == sf::Event::KeyReleased;
	}

	bool KeyboardKeySource::bindToEventSource(const sf::Event& event)
	{
		if (isCompatibleWith(event))
		{
			setKey(event.key.code);
			return true;
		}
		else
		{
			return false;
		}
	}

	std::string KeyboardKeySource::getKeyCodeName(sf::Keyboard::Key key)
	{
		switch (sf::Keyboard::Key keyCode = key)
		{
		case sf::Keyboard::Key::Unknown:
			return "Unknown";
			break;
		case sf::Keyboard::Key::A:
			return "A";
			break;
		case sf::Keyboard::Key::B:
			return "B";
			break;
		case sf::Keyboard::Key::C:
			return "C";
			break;
		case sf::Keyboard::Key::D:
			return "D";
			break;
		case sf::Keyboard::Key::E:
			return "E";
			break;
		case sf::Keyboard::Key::F:
			return "F";
			break;
		case sf::Keyboard::Key::G:
			return "G";
			break;
		case sf::Keyboard::Key::H:
			return "H";
			break;
		case sf::Keyboard::Key::I:
			return "I";
			break;
		case sf::Keyboard::Key::J:
			return "J";
			break;
		case sf::Keyboard::Key::K:
			return "K";
			break;
		case sf::Keyboard::Key::L:
			return "L";
			break;
		case sf::Keyboard::Key::M:
			return "M";
			break;
		case sf::Keyboard::Key::N:
			return "N";
			break;
		case sf::Keyboard::Key::O:
			return "O";
			break;
		case sf::Keyboard::Key::P:
			return "P";
			break;
		case sf::Keyboard::Key::Q:
			return "Q";
			break;
		case sf::Keyboard::Key::R:
			return "R";
			break;
		case sf::Keyboard::Key::S:
			return "S";
			break;
		case sf::Keyboard::Key::T:
			return "T";
			break;
		case sf::Keyboard::Key::U:
			return "U";
			break;
		case sf::Keyboard::Key::V:
			return "V";
			break;
		case sf::Keyboard::Key::W:
			return "W";
			break;
		case sf::Keyboard::Key::X:
			return "X";
			break;
		case sf::Keyboard::Key::Y:
			return "Y";
			break;
		case sf::Keyboard::Key::Z:
			return "Z";
			break;
		case sf::Keyboard::Key::Num0:
			return "Num0";
			break;
		case sf::Keyboard::Key::Num1:
			return "Num1";
			break;
		case sf::Keyboard::Key::Num2:
			return "Num2";
			break;
		case sf::Keyboard::Key::Num3:
			return "Num3";
			break;
		case sf::Keyboard::Key::Num4:
			return "Num4";
			break;
		case sf::Keyboard::Key::Num5:
			return "Num5";
			break;
		case sf::Keyboard::Key::Num6:
			return "Num6";
			break;
		case sf::Keyboard::Key::Num7:
			return "Num7";
			break;
		case sf::Keyboard::Key::Num8:
			return "Num8";
			break;
		case sf::Keyboard::Key::Num9:
			return "Num9";
			break;
		case sf::Keyboard::Key::Escape:
			return "Escape";
			break;
		case sf::Keyboard::Key::LControl:
			return "LControl";
			break;
		case sf::Keyboard::Key::LShift:
			return "LShift";
			break;
		case sf::Keyboard::Key::LAlt:
			return "LAlt";
			break;
		case sf::Keyboard::Key::LSystem:
			return "LSystem";
			break;
		case sf::Keyboard::Key::RControl:
			return "RControl";
			break;
		case sf::Keyboard::Key::RShift:
			return "RShift";
			break;
		case sf::Keyboard::Key::RAlt:
			return "RAlt";
			break;
		case sf::Keyboard::Key::RSystem:
			return "RSystem";
			break;
		case sf::Keyboard::Key::Menu:
			return "Menu";
			break;
		case sf::Keyboard::Key::LBracket:
			return "LBracket";
			break;
		case sf::Keyboard::Key::RBracket:
			return "RBracket";
			break;
		case sf::Keyboard::Key::SemiColon:
			return "SemiColon";
			break;
		case sf::Keyboard::Key::Comma:
			return "Comma";
			break;
		case sf::Keyboard::Key::Period:
			return "Period";
			break;
		case sf::Keyboard::Key::Quote:
			return "Quote";
			break;
		case sf::Keyboard::Key::Slash:
			return "Slash";
			break;
		case sf::Keyboard::Key::BackSlash:
			return "BackSlash";
			break;
		case sf::Keyboard::Key::Tilde:
			return "Tilde";
			break;
		case sf::Keyboard::Key::Equal:
			return "Equal";
			break;
		case sf::Keyboard::Key::Dash:
			return "Dash";
			break;
		case sf::Keyboard::Key::Space:
			return "Space";
			break;
		case sf::Keyboard::Key::Return:
			return "Return";
			break;
		case sf::Keyboard::Key::BackSpace:
			return "BackSpace";
			break;
		case sf::Keyboard::Key::Tab:
			return "Tab";
			break;
		case sf::Keyboard::Key::PageUp:
			return "PageUp";
			break;
		case sf::Keyboard::Key::PageDown:
			return "PageDown";
			break;
		case sf::Keyboard::Key::End:
			return "End";
			break;
		case sf::Keyboard::Key::Home:
			return "Home";
			break;
		case sf::Keyboard::Key::Insert:
			return "Insert";
			break;
		case sf::Keyboard::Key::Delete:
			return "Delete";
			break;
		case sf::Keyboard::Key::Add:
			return "Add";
			break;
		case sf::Keyboard::Key::Subtract:
			return "Subtract";
			break;
		case sf::Keyboard::Key::Multiply:
			return "Multiply";
			break;
		case sf::Keyboard::Key::Divide:
			return "Divide";
			break;
		case sf::Keyboard::Key::Left:
			return "Left";
			break;
		case sf::Keyboard::Key::Right:
			return "Right";
			break;
		case sf::Keyboard::Key::Up:
			return "Up";
			break;
		case sf::Keyboard::Key::Down:
			return "Down";
			break;
		case sf::Keyboard::Key::Numpad0:
			return "Numpad0";
			break;
		case sf::Keyboard::Key::Numpad1:
			return "Numpad1";
			break;
		case sf::Keyboard::Key::Numpad2:
			return "Numpad2";
			break;
		case sf::Keyboard::Key::Numpad3:
			return "Numpad3";
			break;
		case sf::Keyboard::Key::Numpad4:
			return "Numpad4";
			break;
		case sf::Keyboard::Key::Numpad5:
			return "Numpad5";
			break;
		case sf::Keyboard::Key::Numpad6:
			return "Numpad6";
			break;
		case sf::Keyboard::Key::Numpad7:
			return "Numpad7";
			break;
		case sf::Keyboard::Key::Numpad8:
			return "Numpad8";
			break;
		case sf::Keyboard::Key::Numpad9:
			return "Numpad9";
			break;
		case sf::Keyboard::Key::F1:
			return "F1";
			break;
		case sf::Keyboard::Key::F2:
			return "F2";
			break;
		case sf::Keyboard::Key::F3:
			return "F3";
			break;
		case sf::Keyboard::Key::F4:
			return "F4";
			break;
		case sf::Keyboard::Key::F5:
			return "F5";
			break;
		case sf::Keyboard::Key::F6:
			return "F6";
			break;
		case sf::Keyboard::Key::F7:
			return "F7";
			break;
		case sf::Keyboard::Key::F8:
			return "F8";
			break;
		case sf::Keyboard::Key::F9:
			return "F9";
			break;
		case sf::Keyboard::Key::F10:
			return "F10";
			break;
		case sf::Keyboard::Key::F11:
			return "F11";
			break;
		case sf::Keyboard::Key::F12:
			return "F12";
			break;
		case sf::Keyboard::Key::F13:
			return "F13";
			break;
		case sf::Keyboard::Key::F14:
			return "F14";
			break;
		case sf::Keyboard::Key::F15:
			return "F15";
			break;
		case sf::Keyboard::Key::Pause:
			return "Pause";
			break;
		case sf::Keyboard::Key::KeyCount:
			return "KeyCount";
			break;
		default:
			return "N/A";
			break;
		}
	}
}