#pragma once
#include "definitions.hpp"
#include "GUIObject.hpp"
#include "TextBox.hpp"
#include "DemoBindingSet.hpp"
#include <SFML/Window/Event.hpp>
namespace __GAME_NAMESPACE__
{
	class InputWatchDemo :
		public GUIObject
	{
	public:
		DemoBindingSet bindings; //probably not good to leave this publicity long-term but that's not the plan anyway, this class is just a demo.
	private:
		InputBinding<>* pendingBinding;
		
		sf::Vector2<InputTypeTraits<InputType::Axis>::ValueType> movement;
		sf::Vector2<InputTypeTraits<InputType::Axis>::ValueType> aim;
		
		bool isPauseTriggerPulled;
		TextBox bindingAInfo;
		TextBox movementDisplay;
		TextBox aimDisplay;
		TextBox isPauseTriggerPulledDisplay;
		TextBox latestInputDisplay;
		TextBox hasPendingBindDisplay;
		
		std::string getAxisName(sf::Joystick::Axis);
		
	protected:
		virtual void draw(sf::RenderTarget&, sf::RenderStates) const;
	public:
		InputWatchDemo(const sf::Font&);
		virtual ~InputWatchDemo();
		void onEvent(const sf::Event&);
		void update();

		virtual sf::FloatRect getBounds() const;
		virtual sf::Vector2f getPosition() const;

		bool hasPendingBinding() const;

		void setPendingBinding(InputBinding<>* ptr);
	};
}