#pragma once
#include "InputSourceID.hpp"
#include "definitions.hpp"
#include <SFML\Window\Joystick.hpp>
namespace __GAME_NAMESPACE__
{
	typedef InputSourceID<unsigned int, sf::Joystick::Axis, true> JoystickAxisID;

	template<>
	template<>
	bool JoystickAxisID::isCompatibleWith(const sf::Event::JoystickMoveEvent& event)
	{
		return deviceId.get() == event.joystickId;
	}

	template<>
	template<>
	bool JoystickAxisID::setToMatch(const sf::Event::JoystickMoveEvent& event)
	{
		if (isCompatibleWith(event))
		{
			componentId = event.axis;
			return true;
		}
		else
		{
			return false;
		}
	}

	template<>
	template<>
	bool JoystickAxisID::matches(const sf::Event::JoystickMoveEvent& event)
	{
		return deviceId.get() == event.joystickId && componentId == event.axis;
	}

	template<>
	float getSourceValue(const JoystickAxisID& srcId)
	{
		return sf::Joystick::getAxisPosition(srcId.deviceId.get(), srcId.componentId);
	}
}