#include "KBMButtonBinding.hpp"
namespace __GAME_NAMESPACE__
{
	void KBMButtonBinding::setSourceType(SourceType newType)
	{
		if (activeType != newType)
		{
			switch (SourceType curType = activeType) //consistency, all switches should be on active type by default
			{
			case KeyboardKey:
				delete source.keyboard;
				break;
			case MouseButton:
				delete source.mouse;
				break;
			default:
				break;
			}

			switch (SourceType nextType = newType)
			{
			case MouseButton:
				source.mouse = new MouseButtonSource();
				activeType = MouseButton;
				break;
			case KeyboardKey:
				source.keyboard = new KeyboardKeySource();
				activeType = KeyboardKey;
				break;
			default:
				activeType = Unknown;
				break;
			}
		}
	}

	InputSource& KBMButtonBinding::getSource()
	{
		switch (SourceType curType = activeType)
		{
		case KeyboardKey:
			return *(source.keyboard);
			break;
		case MouseButton:
			return *(source.mouse);
			break;
		default:
			throw std::string("NOPE NO SOURCE TO ACCESS (KBMButtonBinding::getSource())");
			break;
		}
	}

	KBMButtonBinding::KBMButtonBinding(const sf::String& name)
		: InputBinding<Button>(name), activeType(Unknown)
	{}

	bool KBMButtonBinding::hasSource() const
	{
		return activeType != Unknown;
	}

	InputTypeTraits<KBMButtonBinding::type>::ValueType KBMButtonBinding::getValue() const
	{
		switch (SourceType curType = activeType)
		{
			//can't really use the or (continue) features of a switch in this case, but we can take advantage of the return
		case KeyboardKey:
			if (hasValidData())
			{
				return sf::Keyboard::isKeyPressed(source.keyboard->getKey());
			}
			break;
		case MouseButton:
			if (hasValidData())
			{
				return sf::Mouse::isButtonPressed(source.mouse->getButton());
			}
			break;
		default:
			break; //redundant break on default but meh.
		}
		return InputTypeTraits<type>::defaultValue;
	}

	const InputSource& KBMButtonBinding::getSource() const
	{
		switch (SourceType curType = activeType)
		{
		case KeyboardKey:
			return *(source.keyboard);
			break;
		case MouseButton:
			return *(source.mouse);
			break;
		default:
			throw std::string("NOPE NO SOURCE TO ACCESS (KBMButtonBinding::getSource() const)");
			break;
		}
	}

	bool KBMButtonBinding::isCompatibleWith(const sf::Event& event)
	{
		return KeyboardKeySource::isCompatibleWith(event) || MouseButtonSource::isCompatibleWith(event);
	}

	bool KBMButtonBinding::bindToEventSource(const sf::Event& event)
	{
		//if we can, utilize the current source-info object.
		if (hasSource() && getSource().bindToEventSource(event))
		{
			return true;
		}
		else if (KeyboardKeySource::isCompatibleWith(event))
		{
			setSourceType(KeyboardKey);
			return getSource().bindToEventSource(event);
		}
		else if (MouseButtonSource::isCompatibleWith(event))
		{
			setSourceType(MouseButton);
			return getSource().bindToEventSource(event);
		}
		else
		{
			return false;
		}

	}

	void KBMButtonBinding::clear()
	{
		setSourceType(Unknown);
	}

	KBMButtonBinding::~KBMButtonBinding()
	{
		switch (SourceType curType = activeType)
		{
		case KeyboardKey:
			delete source.keyboard;
			break;
		case MouseButton:
			delete source.mouse;
			break;
		default:
			break;
		}
	}
}