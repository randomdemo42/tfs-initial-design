#pragma once
#include "definitions.hpp"
#include "InputBinding.hpp"
#include <vector>
namespace __GAME_NAMESPACE__
{
	struct UserControlsEvent;

	class BindingSet
	{
	public:
		enum ButtonBindingCode
		{
			Pause,
			Confirm,
			Back,
			NavLeft,
			NavUp,
			NavDown,
			NavRight,
			ButtonBindingCodeCount
		};
	protected:
		BindingSet() = default;
		virtual ~BindingSet() = default;
	public:
		//useful for iterating over only the basics without needing to know about the contents.
		virtual std::vector<InputBinding<>*> getCustomizableBindings() = 0; //non-const intentionally

		//NOTE THAT THE SECOND PARAM IS NOT CONST. whether or not it is overriden (i.e. whether or not the supplied event matched something), is the return value.
		virtual bool retreiveEvent(const sf::Event&, UserControlsEvent&) = 0; //non-const intentionally

		virtual bool hasPendingBind() const = 0;
		virtual InputBinding<>*& getPendingBindingSlot();

		hasPendingBind ETC
	};
}
//have to include after the full declaration of this class for circular dependancy to work out.
#include "UserControlsEvent.hpp"