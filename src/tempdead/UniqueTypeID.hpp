
//function-pointer magic, thanks to http://codereview.stackexchange.com/questions/48594/unique-type-id-no-rtti

class TypeID
{
	using sig = TypeID(); //the sig-type is a function that returns TypeID

	sig* id; //function-pointer
	TypeID(sig* id) : id{ id } {}

public:
	template<typename T>
	friend TypeID getTypeId();

	bool operator==(TypeID o) const { return id == o.id; }
	bool operator!=(TypeID o) const { return id != o.id; }
};

template<typename T>
TypeID getTypeId() { return &getTypeId<T>; }