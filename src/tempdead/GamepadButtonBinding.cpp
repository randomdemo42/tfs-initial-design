#include "GamepadButtonBinding.hpp"

namespace __GAME_NAMESPACE__
{
	GamepadButtonBinding::GamepadButtonBinding(const sf::String& name, const unsigned int& joystickId) : InputBinding<Button>(name), activeType(Unknown), joystickId(joystickId)
	{
	}

	void GamepadButtonBinding::setSourceType(SourceType newType)
	{
		if (activeType != newType)
		{
			switch (SourceType curType = activeType) //consistency, all switches should be on active type by default
			{
			case JoystickButton:
				delete source.button;
				break;
			case JoystickAxis:
				delete source.axis;
				break;
			default:
				break;
			}

			switch (SourceType nextType = newType)
			{
			case JoystickButton:
				source.button = new JoystickButtonSource();
				activeType = JoystickButton;
				break;
			case JoystickAxis:
				source.axis = new JoystickAxisSource();
				activeType = JoystickAxis;
				break;
			default:
				activeType = Unknown;
				break;
			}
		}
	}

	//note the duplicated code with the two getSourceInfo() here, and to be careful!

	InputSource& GamepadButtonBinding::getSource()
	{
		switch (SourceType curType = activeType)
		{
		case JoystickButton:
			return (*source.button);
			break;
		case JoystickAxis:
			return (*source.axis);
			break;
		default:
			throw std::string("NOPE NO SOURCE TO ACCESS (JoystickButtonBinding::getSource())");
			break;
		}
	}

	const InputSource& GamepadButtonBinding::getSource() const
	{
		switch (SourceType curType = activeType)
		{
		case JoystickButton:
			return (*source.button);
			break;
		case JoystickAxis:
			return (*source.axis);
			break;
		default:
			throw std::string("NOPE NO SOURCE TO ACCESS (JoystickButtonSource::getSourceInfo()) const");
			break;
		}
	}

	bool GamepadButtonBinding::hasSource() const
	{
		return activeType != Unknown;
	}

	//defaults to constantly giving the default-value of the type if the source info isn't existing and valid.
	InputTypeTraits<GamepadButtonBinding::type>::ValueType GamepadButtonBinding::getValue() const
	{
		switch (SourceType curType = activeType)
		{
		case JoystickButton:
			if (hasValidData())
			{
				return sf::Joystick::isButtonPressed(joystickId, source.button->getButtonId());
			}
			break;
		case JoystickAxis:
			if (hasValidData())
			{
				return std::abs(sf::Joystick::getAxisPosition(joystickId, source.axis->getAxis())) >= (InputTypeTraits<Axis>::maxValue - InputTypeTraits<Axis>::defaultValue) / 2;
			}
			break;
		default:
			break;
		}
		return InputTypeTraits<type>::defaultValue;
	}

	bool GamepadButtonBinding::isCompatibleWith(const sf::Event& event)
	{
		return JoystickButtonSource::isCompatibleWith(event) || JoystickAxisSource::isCompatibleWith(event);
	}

	bool GamepadButtonBinding::bindToEventSource(const sf::Event& event)
	{
		if ((event.type == sf::Event::EventType::JoystickButtonPressed && event.joystickButton.joystickId != joystickId) || (event.type == sf::Event::EventType::JoystickMoved && event.joystickMove.joystickId != joystickId))
		{
			return false;
		}
		else if (hasSource() && getSource().bindToEventSource(event)) //if we can, utilize the current source-info object.
		{
			return true;
		}
		else if (JoystickButtonSource::isCompatibleWith(event))
		{
			setSourceType(JoystickButton);
			return getSource().bindToEventSource(event);
		}
		else if (JoystickAxisSource::isCompatibleWith(event))
		{
			setSourceType(JoystickAxis);
			return getSource().bindToEventSource(event);
		}
		else
		{
			return false;
		}
	}

	void GamepadButtonBinding::clear()
	{
		setSourceType(Unknown);
	}

	GamepadButtonBinding::~GamepadButtonBinding()
	{
		switch (SourceType curType = activeType)
		{
		case JoystickButton:
			delete source.button;
			break;
		case JoystickAxis:
			delete source.axis;
		default:
			break;
		}
	}

	GamepadButtonBinding::GamepadButtonBinding(GamepadButtonBinding&& src)
		: InputBinding<Button>(std::forward< InputBinding<Button> >(src)), source(std::move(src.source)), activeType(std::move(src.activeType)), joystickId(std::move(joystickId))
	{

	}
	GamepadButtonBinding& GamepadButtonBinding::operator=(GamepadButtonBinding&& src)
	{
		InputBinding<Button>::operator=(std::move(src));
		source = std::move(src.source);
		activeType = std::move(src.activeType);
		joystickId = std::move(src.joystickId);
	}

TODO: MAKE SURE OTHER BINDINGS CLASSES AND BINDINGSETS ETC CAN BE MOVE CONSTRUCTED, THEN WORK OUT HOW BINDINGS BEING "APPLIED" SHOULD BE IMPLIMENTED, ETC?
}