#include "InputBinding.hpp"

namespace __GAME_NAMESPACE__
{
	InputBinding<>::InputBinding(const sf::String& name)
		: name(name)
	{}
	sf::String InputBinding<>::getName() const
	{
		return name;
	}

	void InputBinding<>::setName(const sf::String& name)
	{
		this->name = name;
	}
	bool InputBinding<>::hasValidData() const
	{
		return hasSource() && getSource().hasValidData();
	}

	bool InputBinding<>::matches(const InputBinding<>& other) const
	{
		return hasSource() && other.hasSource() ? getSource().matches(other.getSource()) : false;
	}

	/*
	returns whether or not the source of the event was the exact input source represented by this object
	*/
	bool InputBinding<>::matches(const sf::Event& event) const
	{
		return InputBinding<>::hasValidData() ? getSource().matches(event) : false;
	}

	sf::String InputBinding<>::getSourceName() const
	{
		return getSource().getName();
	}
};