#pragma once
#include "InputSource.hpp"
#include <string>
namespace __GAME_NAMESPACE__
{
	class KeyboardKeySource : public InputSource
	{
	public:
		static std::string getKeyCodeName(sf::Keyboard::Key key);
	private:
		sf::Keyboard::Key key;
		bool matches(const KeyboardKeySource& other) const;
		
	protected:
		/*
		KeyboardKeySource(const TypeID& comparableTypeId, sf::Keyboard::Key key = sf::Keyboard::Key::Unknown);
		KeyboardKeySource(const TypeID& comparableTypeId,  sf::Keyboard::Key key, const sf::String& name);
		*/
		
	public:
		KeyboardKeySource(sf::Keyboard::Key key = sf::Keyboard::Key::Unknown);
		KeyboardKeySource(sf::Keyboard::Key key, const sf::String& name);

		void setKey(sf::Keyboard::Key key);
		void setData(sf::Keyboard::Key key, const sf::String& name); //incase the name should be non-standard for some reason
		
		//returns whether or not the object is in a valid state.
		virtual bool hasValidData() const;
		
		sf::Keyboard::Key getKey() const;
		//Important: semi-cheating double-dispatch, see InputSource.hpp for implimentation details.
		virtual bool matches(const InputSource& other) const;

		/*
		returns whether or not the source of the event was the exact input source represented by this object
		*/
		virtual bool matches(const sf::Event&) const;
		
		//NOTE: CURRENT IMPLIMENTATION ONLY ATTEMPTS TO BIND/BE COMPATIBLE WITH PRESS EVENTS, NOT RELEASE, CONSIDER WHETHER OR NOT THAT MAY NEED CHANGING

		/*
		returns whether or not the source of the event was compatible with this sourceinfo type.
		*/
		static bool isCompatibleWith(const sf::Event& event);
		/*
		if the supplied event is compatible, this takes source information from an event, such that the button/stick/key associated with that event becomes the source.
		if the event was not compatable, the data remains unchanged.

		returns whether or not the event was compatible.
		*/
		virtual bool bindToEventSource(const sf::Event& event);

		//post: the source will no longer have valid data
		virtual void clear();
		
		virtual ~KeyboardKeySource() = default;
	};
}