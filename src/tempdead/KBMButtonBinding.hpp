#pragma once
#include "InputBinding.hpp"
#include "KeyboardKeySource.hpp"
#include "MouseButtonSource.hpp"
namespace __GAME_NAMESPACE__
{
	//this basically provides the ability to use either  KeyboardKeySource or MouseButtonSource in the one InputSource.
	class KBMButtonBinding
		: public InputBinding < Button >
	{
	private:
		enum SourceType
		{
			KeyboardKey,
			MouseButton,
			Unknown
		};
		union Source
		{
			KeyboardKeySource* keyboard;
			MouseButtonSource* mouse;
		};
		Source source;
		SourceType activeType;
		void setSourceType(SourceType newType);

		//note the duplicated code with the two getSourceInfo() here, and to be careful!
		//pre: hasSource()
		virtual InputSource& getSource();
		//pre: hasSource()
		virtual const InputSource& getSource() const;
	public:
		KBMButtonBinding(const sf::String& name);
		//not neccessarily impossible, but disabling now due to non-triviality, will impliment if it becomes appropriate or neccessary. Not sure if the default move is appropriate, but it probably is.
		KBMButtonBinding(const KBMButtonBinding&) = delete;
		KBMButtonBinding(KBMButtonBinding&&) = delete;
		KBMButtonBinding& operator=(const KBMButtonBinding&) = delete;
		KBMButtonBinding& operator==(KBMButtonBinding&&) = delete;
		
		virtual bool hasSource() const;

		//defaults to constantly giving the default-value of the type if the source info isn't existing and valid.
		//virtual std::function< InputTypeTraits<type>::ValueType()> getValueRetriever() const;
		virtual InputTypeTraits<type>::ValueType getValue() const;

		static bool isCompatibleWith(const sf::Event& event);

		virtual bool bindToEventSource(const sf::Event& event);

		virtual void clear();
		
		virtual ~KBMButtonBinding();
	};
}