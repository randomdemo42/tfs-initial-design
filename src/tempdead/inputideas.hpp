#pragma once
#include "definitions.hpp"

namespace __GAME_NAMESPACE__
{
	namespace inputSource
	{
		template<typename T>
		struct TypeWrapper
		{
			using type = T;//has a single member type "type"
		};

		template < typename Tp, typename... List >
		struct contains : std::true_type {};

		template < typename Tp, typename Head, typename... Rest >
		struct contains<Tp, Head, Rest...>
			: std::conditional < std::is_same<Tp, Head>::value,
			std::true_type,
			contains<Tp, Rest...>
			> ::type{};

		template < typename Tp >
		struct contains<Tp> : std::false_type{};

		struct Device {};

		struct Component {};

		//not cv or reference
		template<typename T>
		using IsRawType = std::conditional_t < !std::is_const<T>::value && !std::is_volatile<T>::value && !std::is_reference<T>::value, std::true_type, std::false_type > ;
		
		//declare the type by specialising these wrappers, but to reduce verbosity, type aliasing are provided for them.

		template<typename DeviceType>
		struct DeviceIDTypeWrapper : public TypeWrapper < void > {};

		//has a single member type "type"
		template<typename DeviceType, typename ComponentType>
		struct ComponentIDTypeWrapper : public TypeWrapper < void > {};

		//has a single member type "type"
		template<typename ComponentType>
		struct ValueTypeWrapper : public TypeWrapper < void > {};

		
		template<typename DeviceType>
		using DeviceIDType = typename DeviceIDTypeWrapper < DeviceType >::type;

		template<typename DeviceType, typename ComponentType>
		using ComponentIDType = typename ComponentIDTypeWrapper<DeviceType, ComponentType>::type;

		template<typename ComponentType>
		using ValueType = typename ValueTypeWrapper<ComponentType>::type;

		//Describes whether or not there is support to detect multiple devices of this type at once.
		//It is a boolean which is determined based on whether or not DeviceIDType<DeviceType> is void.
		template<typename DeviceType>
		using MultiDeviceDetection = std::conditional_t< std::is_same < DeviceIDType<DeviceType>, void >::value, std::false_type, std::true_type > ;

		//static compatibilities, first DeviceIDType, then ComponentIDType where they are components of DeviceIDType devices?
		//each extend or alias an std::integral_constant<bool, ?> 

		//This describes whether or not an "EventType" is an event which is produced by a device of DeviceType, or a component of a device of DeviceType.
		//Specifically, it is an alias for std::true_type iff either: the id of the device that triggered the event could be determined from any and all instances of the EventType, or there is only support for detecting one device of DeviceType (see MultiDeviceDection<DeviceType>)
		//Otherwise is an alias for std::false_type.
		template<typename EventType, typename DeviceType>
		struct IsCompatibleWithDevice;

		//This describes whether or not an "EventType" is an event which is produces by a component of type ComponentType of a device of type DeviceType.
		//Specifically, it is an alias for std::true_type iff AreCompatible<DeviceType, EventType> and the of the device-component that triggered the event could be determined from any and all instances of the EventType.
		//Otherwise is an alias for std::false_type.
		template<typename EventType, typename DeviceType, typename ComponentType>
		struct IsCompatibleWithComponent;

		//these manual overloads for const& seem to be neccessary to allow the EventType to be removed from the parameters needed in brackets, allowing them to be provided purely via arguement/param type. Reducing verbosity of specific implimentations and of the usage of these constructs

		//Retrieves the device id for the DeviceType corresponding to the supplied event. Must be a valid operation for all instances of EventType
		//Read this as: DeviceIDType getDeviceID<DeviceType>( EventType (const&)? ); const& if EventType is non-fundemental
		template<typename DeviceType, typename EventType>
		std::enable_if_t< IsRawType<EventType>::value && MultiDeviceDetection<DeviceType>::value && IsCompatibleWithDevice<EventType, DeviceType>::value && std::is_fundamental<EventType>::value, DeviceIDType<DeviceType> >
		getDeviceID(EventType);

		template<typename DeviceType, typename EventType>
		std::enable_if_t< IsRawType<EventType>::value && MultiDeviceDetection<DeviceType>::value && IsCompatibleWithDevice<EventType, DeviceType>::value && !std::is_fundamental<EventType>::value, DeviceIDType<DeviceType> >
		getDeviceID(const EventType&);
		

		//Retrieves the component id for the ComponentType of the DeviceType corresponding to the supplied event. Must be a valid operation for all instances of EventType.
		//Read this is: ComponentIDType<DeviceType, ComponentType> getComponentID(EventType (const&)? ); const& if EventType is non-fundemental
		template<typename DeviceType, typename ComponentType, typename EventType>
		std::enable_if_t< IsRawType < EventType >::value && IsCompatibleWithComponent<EventType, DeviceType, ComponentType>::value && std::is_fundamental<EventType>::value, ComponentIDType<DeviceType, ComponentType> >
		getComponentID(EventType);

		template<typename DeviceType, typename ComponentType, typename EventType>
		std::enable_if_t< IsRawType<EventType>::value && IsCompatibleWithComponent<EventType, DeviceType, ComponentType>::value && !std::is_fundamental<EventType>::value, ComponentIDType<DeviceType, ComponentType> >
		getComponentID(const EventType&);

		//getState will get the value that corresponds to the current state of the input source if the device and component are connected, otherwise some non-meaningful value.
		//Read this as: 
		template<typename DeviceType, typename ComponentType>
		std::enable_if_t< IsRawType<DeviceType>::value && IsRawType<ComponentType>::value && MultiDeviceDetection<DeviceType>::value && std::is_fundamental< DeviceIDType<DeviceType> >::value && std::is_fundamental< ComponentIDType<DeviceType, ComponentType> >::value, ValueType<ComponentType> >
		getState(DeviceIDType<DeviceType>, ComponentIDType<DeviceType, ComponentType>);

		template<typename DeviceType, typename ComponentType>
		std::enable_if_t< IsRawType<DeviceType>::value && IsRawType<ComponentType> && MultiDeviceDetection<DeviceType>::value && std::is_fundamental< DeviceIDType<DeviceType> >::value && !std::is_fundamental< ComponentIDType<DeviceType, ComponentType> >::value, ValueType<ComponentType> >
		getState(DeviceIDType<DeviceType>, const ComponentIDType<DeviceType, ComponentType>&);

		template<typename DeviceType, typename ComponentType>
		std::enable_if_t< IsRawType<DeviceType>::value && IsRawType<ComponentType>::value && MultiDeviceDetection<DeviceType>::value && !std::is_fundamental< DeviceIDType<DeviceType> >::value && std::is_fundamental< ComponentIDType<DeviceType, ComponentType> >::value, ValueType<ComponentType> >
		getState(const DeviceIDType<DeviceType>&, ComponentIDType<DeviceType, ComponentType>);

		template<typename DeviceType, typename ComponentType>
		std::enable_if_t< IsRawType<DeviceType>::value && IsRawType<ComponentType>::value && MultiDeviceDetection<DeviceType>::value && !std::is_fundamental< DeviceIDType<DeviceType> >::value && !std::is_fundamental< ComponentIDType<DeviceType, ComponentType> >::value, ValueType<ComponentType> >
		getState(const DeviceIDType<DeviceType>&, const ComponentIDType<DeviceType, ComponentType>&);

		template<typename DeviceType, typename ComponentType>
		std::enable_if_t< IsRawType<DeviceType>::value && IsRawType<ComponentType>::value && !MultiDeviceDetection<DeviceType>::value && std::is_fundamental< ComponentIDType<DeviceType, ComponentType> >::value, ValueType<ComponentType> >
		getState(ComponentIDType<DeviceType, ComponentType>);

		template<typename DeviceType, typename ComponentType>
		std::enable_if_t< IsRawType<DeviceType>::value && IsRawType<ComponentType>::value && !MultiDeviceDetection<DeviceType>::value && !std::is_fundamental< ComponentIDType<DeviceType, ComponentType> >::value, ValueType<ComponentType> >
		getState(const ComponentIDType<DeviceType, ComponentType>&);

		/*examples?*/
		
		struct Mouse{}; //Empty struct for type mappings?

		struct Button{};

		struct Coordinate{};

		template<>
		struct ValueTypeWrapper<Coordinate> : public TypeWrapper < sf::Vector2f > {};
		
		template<>
		struct ValueTypeWrapper<Button> : public TypeWrapper < bool > {};

		//mouse button
		template<>
		struct ComponentIDTypeWrapper<Mouse, Button> : public TypeWrapper < sf::Mouse::Button > {};

		//mouse move
		template<>
		struct ComponentIDTypeWrapper<Mouse, Coordinate> : public TypeWrapper < bool >{};

		template<>
		struct IsCompatibleWithDevice< sf::Event::MouseButtonEvent, Mouse> : public std::true_type{};

		template<>
		struct IsCompatibleWithDevice< sf::Event::MouseMoveEvent, Mouse > : public std::true_type{};
		
		template<>
		struct IsCompatibleWithDevice< sf::Event::MouseWheelEvent, Mouse > : public std::true_type{};

		template<>
		struct IsCompatibleWithComponent<sf::Event::MouseButtonEvent, Mouse, Button> : public std::true_type{};

		template<>
		struct IsCompatibleWithComponent<sf::Event::MouseButtonEvent, Mouse, Coordinate> : public std::true_type{};


		template<>
		inline sf::Mouse::Button getComponentID<Mouse, Button>(const sf::Event::MouseButtonEvent& event)
		{
			return event.button;
			
		}

		template<>
		inline bool getComponentID<Mouse, Coordinate>(const sf::Event::MouseButtonEvent& event)
		{
			return true;
		}
	}
	



}