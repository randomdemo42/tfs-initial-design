/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "definitions.hpp"
#include <SFML/System/Vector2.hpp>
#include <utility>
namespace __GAME_NAMESPACE__
{
	//no need for getters on this with const/non-const right?
	/*template<typename NumType>
	class Dimensions
	{
	public:
		NumType width;
		NumType height;
		Dimensions(NumType width, NumType height) : width(width), height(height){};
		virtual ~Dimensions(){};
	};
	*/
	/*
	typedef sf::Vector2f Vector2Df;
	//standard types for semi-global data. E.g. window and window object dimensions etc
	//this technically has an x and y stored publically too but thats not garaunteed, DON'T use it xD, too lazy to impliment all the operators manually, later I will probs make it private though
	class Dimensions : public sf::Vector2u
	{
	public:
		unsigned int& width;
		unsigned int& height;
		Dimensions(unsigned int width, unsigned int height) : sf::Vector2u(width, height), width(x), height(y){}
		Dimensions(const sf::Vector2u& vector) : sf::Vector2u(vector), width(x), height(y){}
		Dimensions(const Dimensions& src) : sf::Vector2u(src.width, src.height), width(x), height(y){};
		Dimensions& operator= (const Dimensions& src)
		{
			width = src.width;
			height = src.height;
			return *this;
		}

		//these move things are probably innefficient, not sure?
		Dimensions(Dimensions&& src) : sf::Vector2u(std::move(src)), width(x), height(y){}
		Dimensions& operator = (Dimensions&& src)
		{
			sf::Vector2u::operator=(src);
		}

	};
	typedef sf::Vector2i Position;
	*/

}
