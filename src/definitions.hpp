/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#define __GAME_NAMESPACE__ twinStickShooter
#define RENDER_DEBUGGING 0
#define __DEFAULT_BINDING_FILE_PATH__ "/home/marcos/git-projects/tfs-initial-design/output/input_bindings/bindings.default.tfs_bindings"
#define __ACTIVE_BINDING_FILE_PATH__ "/home/marcos/git-projects/tfs-initial-design/output/input_bindings/bindings.active.tfs_bindings"
#include <iostream>

namespace __GAME_NAMESPACE__
{
    static const double MATH_PI = 3.14159265358979323846;
}
