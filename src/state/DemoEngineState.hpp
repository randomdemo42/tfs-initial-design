/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "State.hpp"
#include "../input/InputHandler.hpp"
#include "../engine/client/OfflineClientECSCore.hpp"
#include "../menu/Menu.hpp"
#include "../menu/SimpleTextButton.hpp"

//the number of samples taken together to calculate the frame rate
#define SAMPLE_COUNT 5

namespace __GAME_NAMESPACE__
{
    //this isn't intended to be a normal menu-state, but (might be later, it's a test-state anyway) a back button for now might be useful at least - we'll only send the menu MouseEvents for now?
    class DemoEngineState: public State
    {
    private:
        const sf::Font& defaultFont;
        std::function<void()> backFunc;
        Menu menu;
        InputHandler& inputHandler;
        sf::Clock clock;
        long unsigned int lastUpdateTime; //track the time of the last update, so we can determine when we are on a new second and use that to colculate our framerate
        long unsigned int updateCounts[SAMPLE_COUNT]; //average the render count over the last minute, , we'll update the index corresponding to secondCount mod 10 when we do the update. - storing 61 so we discount the latest second?
        long unsigned int sumUpdateCount; //sum update count over the last 60 seconds

        OfflineClientECSCore engineCore;

    protected:
        virtual void draw(sf::RenderTarget&, sf::RenderStates) const;
    public:
        DemoEngineState(const sf::Font& defaultFont, const std::function<void()>& backFunc, InputHandler& inputHandler);
        virtual void update();

		//default implementations, do nothing
		virtual void onEvent(const CharacterInputEvent&);
		virtual void onEvent(const MouseEvent&);
		virtual void onEvent(const MenuInputEvent&);
    };
}
