/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "DemoEngineState.hpp"
#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/ConvexShape.hpp>
namespace __GAME_NAMESPACE__
{
    DemoEngineState::DemoEngineState(const sf::Font& defaultFont, const std::function<void()>& backFunc, InputHandler& inputHandler)
    : defaultFont(defaultFont), backFunc(backFunc), menu(sf::Vector2f(550, 550)), inputHandler(inputHandler), lastUpdateTime(0), sumUpdateCount(0)
    {
        for(int i = 0; i < SAMPLE_COUNT; ++i)
        {
            updateCounts[i] = 0;
        }
        engineCore.renderingSystem.setCanvasArea(sf::FloatRect(50, 50, 500, 500));
        menu.addItem<SimpleTextButton>(true, defaultFont, "Back to Main Menu", [&](SimpleTextButton*){this->backFunc();});
        EntityID targetId = engineCore.createNewEntity();

        engineCore.transforms[targetId].transformable.setPosition(100, 100);
        std::vector< std::unique_ptr<sf::Drawable> >& shapes = engineCore.visuals[targetId].shapes;
        auto targetCircle = new sf::CircleShape(20, 50);
        targetCircle->setOrigin(20, 20);
        // targetCircle->setPosition(0, 0);
        targetCircle->setOutlineColor(sf::Color(255, 0, 0));
        targetCircle->setFillColor(sf::Color(0, 255, 255));
        targetCircle->setOutlineThickness(1);
        shapes.emplace_back(targetCircle);
        auto targetLine = new sf::ConvexShape(4);
        targetLine->setPoint(0, sf::Vector2f(0,0));
        targetLine->setPoint(1, sf::Vector2f(20, 0));
        targetLine->setPoint(2, targetLine->getPoint(1));
        targetLine->setPoint(3, targetLine->getPoint(0));
        targetLine->setOutlineColor(sf::Color(255, 0, 0));
        targetLine->setFillColor(sf::Color(255, 0, 0));
        targetLine->setOutlineThickness(1);
        shapes.emplace_back(targetLine);
        engineCore.motions[targetId].velocity = sf::Vector2f();
    }
    void DemoEngineState::draw(sf::RenderTarget& target, sf::RenderStates states) const
    {
        TextBox eachTextBox(defaultFont);
        sf::String eachString;
        sf::FloatRect eachBounds;
        std::stringstream stringstream;
        target.draw(engineCore.renderingSystem, states);
        sf::FloatRect bounds = engineCore.renderingSystem.getBounds();
        target.draw(menu, states);
        int averagedUpdateCount = std::round( sumUpdateCount/( (float)( std::max(std::min( lastUpdateTime, SAMPLE_COUNT-1ul), 1ul) ) ) ); //if we haven't had 60 seconds yet, only devide by the number we do have, otherwise cap it at 60, also add the fractional time since the last second - which means that we can start giving non-zero readings on the first frame
        // long unsigned int averagedUpdateCount = std::round(float(sumUpdateCount)/float(clock.getElapsedTime().asSeconds()));
        stringstream << averagedUpdateCount;
        eachTextBox.setString(stringstream.str());
        eachTextBox.setPosition(TextBox::PADDING);
        target.draw(eachTextBox, states);

        #if RENDER_DEBUGGING
        		GUIObject::drawDebug(target, states);
        #endif
    }

    void DemoEngineState::update()
    {
        engineCore.update();
        long unsigned int curTime = (long unsigned int)(std::floor(clock.getElapsedTime().asSeconds()));

        if(curTime != lastUpdateTime)
        {
            sumUpdateCount += updateCounts[lastUpdateTime % SAMPLE_COUNT];
            sumUpdateCount -= updateCounts[curTime % SAMPLE_COUNT];
            updateCounts[curTime % SAMPLE_COUNT] = 0;
            lastUpdateTime = curTime;
        }
        ++(updateCounts[curTime % SAMPLE_COUNT]);
    }

    void DemoEngineState::onEvent(const CharacterInputEvent& event)
    {
         engineCore.inputHandlingSystem.onEvent(event);
    }
    void DemoEngineState::onEvent(const MouseEvent& event)
    {
        MouseEvent eventToForward = event;
        sf::Vector2f mousePosition;
		switch(MouseEvent::SubType subType = event.subType)
		{
			case MouseEvent::Move:
                mousePosition = sf::Vector2f(event.move.x, event.move.y);
				break;
			case MouseEvent::ButtonPressed: //continue to next case without breaking.
			case MouseEvent::ButtonReleased:
				mousePosition = sf::Vector2f(event.button.x, event.button.y);
				break;
		}
        if(engineCore.renderingSystem.getBounds().contains(mousePosition))
        {
            mousePosition -= engineCore.renderingSystem.getPosition(); //translate into the local space of the rendering box, since the engine should be oblivious to the way it's contents are rendered.
            switch(MouseEvent::SubType subType = event.subType)
    		{
    			case MouseEvent::Move:
                    eventToForward.move.x = mousePosition.x;
                    eventToForward.move.y = mousePosition.y;
    				break;
    			case MouseEvent::ButtonPressed: //continue to next case without breaking.
    			case MouseEvent::ButtonReleased:
    				eventToForward.button.x = mousePosition.y;
                    eventToForward.button.y = mousePosition.y;
    				break;
    		}
            engineCore.inputHandlingSystem.onEvent(eventToForward);
        }
        else if(menu.getBounds().contains(mousePosition))
        {
            menu.onEvent(event);
        }
    }
    void DemoEngineState::onEvent(const MenuInputEvent& event)
    {
        menu.onEvent(event);
    }


}
