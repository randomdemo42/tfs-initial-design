/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "../definitions.hpp"
#include "State.hpp"
#include "StateMachine.hpp"
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Window/Event.hpp>
namespace __GAME_NAMESPACE__
{
	//note that StateMachine::changeState must have been called at least once for the machine to be in a valid state.
	template<class StateType>
	class StateManager: public StateMachine<StateType>, public State
	{
	protected:
		StateManager();
		StateManager(const StateManager&) = delete;
		StateManager(StateManager&&) = delete;
		StateManager& operator=(const StateManager&) = delete;
		StateManager& operator=(StateManager&&) = delete;

		virtual void draw(sf::RenderTarget&, sf::RenderStates) const;
	public:

		// virtual void onEvent(const sf::Event& event);d

		//default implementations, send to current state
		virtual void onEvent(const CharacterInputEvent&);
		virtual void onEvent(const MouseEvent&);
		virtual void onEvent(const MenuInputEvent&);
		virtual void onEvent(const MiscInputEvent&);
		virtual void onEvent(const TextInputEvent&);

		virtual void update();
		virtual ~StateManager() = 0;
	};

	template<class StateType>
	StateManager<StateType>::StateManager() : StateMachine<StateType>()
	{
	}

	template<class StateType>
	StateManager<StateType>::~StateManager()
	{}

	template<class StateType>
	void StateManager<StateType>::onEvent(const CharacterInputEvent& event)
	{
        ((State*)(StateMachine<StateType>::currentState.get()))->onEvent(event);
	}

	template<class StateType>
	void StateManager<StateType>::onEvent(const MouseEvent& event)
	{
		((State*)(StateMachine<StateType>::currentState.get()))->onEvent(event);
	}

	template<class StateType>
	void StateManager<StateType>::onEvent(const MenuInputEvent& event)
	{
		((State*)(StateMachine<StateType>::currentState.get()))->onEvent(event);
	}

    template<class StateType>
    void StateManager<StateType>::onEvent(const MiscInputEvent& event)
    {
        ((State*)(StateMachine<StateType>::currentState.get()))->onEvent(event);
    }

    template<class StateType>
    void StateManager<StateType>::onEvent(const TextInputEvent& event)
    {
        ((State*)(StateMachine<StateType>::currentState.get()))->onEvent(event);
    }

	template<class StateType>
	void StateManager<StateType>::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		target.draw((*StateMachine<StateType>::currentState), states);
	}

	template<class StateType>
	void StateManager<StateType>::update()
	{
		StateMachine<StateType>::currentState->update();
	}
}
