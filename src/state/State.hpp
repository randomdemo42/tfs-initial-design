/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "../definitions.hpp"
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include "../event/EventListener.hpp"
#include "../input/MouseEvent.hpp"
#include "../input/CharacterInputEvent.hpp"
#include "../input/MenuInputEvent.hpp"
#include "../input/MiscInputEvent.hpp"
#include "../input/TextInputEvent.hpp"
namespace __GAME_NAMESPACE__
{
	//note that all States should actually be given a clientStopHelper probably? might want to make the declaration reflect this somehow
	class State: public sf::Drawable, public EventListener<MouseEvent>, public EventListener<CharacterInputEvent>, public EventListener<MenuInputEvent>, public EventListener<MiscInputEvent>, public EventListener<TextInputEvent>
	{
	public:
		virtual void update() = 0;
		virtual ~State() = 0;

		//default implementations, do nothing
		virtual void onEvent(const CharacterInputEvent&);
		virtual void onEvent(const MouseEvent&);
		virtual void onEvent(const MenuInputEvent&);
        virtual void onEvent(const MiscInputEvent&);
        virtual void onEvent(const TextInputEvent&);
	};

}
