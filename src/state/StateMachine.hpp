/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "../definitions.hpp"
#include <functional>
#include <memory>
namespace __GAME_NAMESPACE__
{
	//NOTE: changeState() must have been called at least once for the machine to be in a valid state, this is mostly important for usage of derived classes
	//NOTE: This class (unless changes are made, in which case also change comments below as neccessary) utilitises unique_ptr and therefore can only be supplied with heap-allocated pointers, which should be managed no where else.)
	template<typename StateType>
	class StateManager;

	//state machine is the part of statemanager that only deals with changing state.
	template<class StateType> //if needed in future, we could add a boolean param for whether or not the StateMachine should maintain a unique pointer to the relevant state-type that defaults to true?
	class StateMachine
	{
		friend class StateManager<StateType>;
		std::unique_ptr<StateType> currentState;
	protected:
		StateMachine();
		StateMachine(const StateMachine&) = delete;
		StateMachine(StateMachine&&) = delete;
		StateMachine& operator=(const StateMachine&) = delete;
		StateMachine& operator=(StateMachine&&) = delete;

	public:
		virtual ~StateMachine();

		//keeping this wrapper around the constructors for now? may remove though tbh.

		//shift the current State to the selected State type, constructing it with the supplied args.
		//must have been called at least once for the machine to be in a valid state.
		template<class TargetStateType, typename... Args>
		typename std::enable_if<std::is_base_of<StateType, TargetStateType>::value, void>::type
		changeState(Args&&... args)
		{
			currentState.reset(new TargetStateType(std::forward<Args>(args)...));
		}
	};

	template<class StateType>
	StateMachine<StateType>::StateMachine() : currentState()
	{
	}

	template<class StateType>
	StateMachine<StateType>::~StateMachine()
	{
	}
}
