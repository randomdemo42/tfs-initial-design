/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "InputHandlingSystem.hpp"
#include <cmath>
namespace __GAME_NAMESPACE__
{
    InputHandlingSystem::InputHandlingSystem(std::map<EntityID, TransformComponent>& transforms, std::map<EntityID, MotionComponent>& motions):
        transforms(transforms), motions(motions), onPosRotX(false), onPosRotY(false), onPosMovX(false), onPosMovY(false), curPosRotX(0), curNegRotX(0), curPosRotY(0), curNegRotY(0), curPosMovX(0), curNegMovX(0), curPosMovY(0), curNegMovY(0)
    {}

    void InputHandlingSystem::update()
    {
        //do nothing for now, this is more of an event-handler
    }

    void InputHandlingSystem::onEvent(const CharacterInputEvent& event)
    {
        float resultX, resultY;
        float movementMagnitude;
        sf::Vector2f updatedMovement;

        switch(event.subType)
        {
            case CharacterInputEvent::SubType::Rotation:
                if(event.activation)
                {
                    if(event.axis == CharacterInputEvent::Axis::X)
                    {
                        if(event.position > 0)
                        {
                            onPosRotX = true;
                            curPosRotX = event.position;
                        }
                        else if(event.position < 0)
                        {
                            onPosRotX = false;
                            curNegRotX = event.position;
                        }
                        else
                        {
                            if(onPosRotX)
                            {
                                curPosRotX = event.position;
                            }
                            else
                            {
                                curNegRotX = event.position;
                            }
                        }
                    }
                    else if(event.axis == CharacterInputEvent::Axis::Y)
                    {
                        if(event.position > 0)
                        {
                            onPosRotY = true;
                            curPosRotY = event.position;
                        }
                        else if(event.position < 0)
                        {
                            onPosRotY = false;
                            curNegRotY = event.position;
                        }
                        else
                        {
                            if(onPosRotY)
                            {
                                curPosRotY = event.position;
                            }
                            else
                            {
                                curNegRotY = event.position;
                            }
                        }
                    }
                    resultX = onPosRotX ? curPosRotX : curNegRotX;
                    resultY = onPosRotY ? curPosRotY : curNegRotY;
                    (*(transforms.begin())).second.transformable.setRotation(std::atan2(resultY, resultX)*(180.0f/MATH_PI));
                }
                else
                {
                    if(event.axis == CharacterInputEvent::Axis::X)
                    {
                        if(event.position > 0)
                        {
                            onPosRotX = false;
                            curPosRotX = 0;
                        }
                        else
                        {
                            onPosRotX = true;
                            curNegRotX = 0;
                        }
                    }
                    else if(event.axis == CharacterInputEvent::Axis::Y)
                    {
                        if(event.position > 0)
                        {
                            onPosRotY = false;
                            curPosRotY = 0;
                        }
                        else
                        {
                            onPosRotY = true;
                            curNegRotY = 0;
                        }
                    }
                }
                // something is wrong seemingly with activation, somehow there is no activation event being thrown for the exact n,s,e,w directions!
                // a similar issue is occuring with movement, it's almost as if activations aren't correctly overriding after a deactivation?
                break;
            case CharacterInputEvent::SubType::Movement:
                if(event.activation)
                {
                    if(event.axis == CharacterInputEvent::Axis::X)
                    {
                        if(event.position > 0)
                        {
                            onPosMovX = true;
                            curPosMovX = event.position;
                        }
                        else if(event.position < 0)
                        {
                            onPosMovX = false;
                            curNegMovX = event.position;
                        }
                        else
                        {
                            if(onPosMovX)
                            {
                                curPosMovX = event.position;
                            }
                            else
                            {
                                curNegMovX = event.position;
                            }
                        }
                    }
                    else if(event.axis == CharacterInputEvent::Axis::Y)
                    {
                        if(event.position > 0)
                        {
                            onPosMovY = true;
                            curPosMovY = event.position;
                        }
                        else if(event.position < 0)
                        {
                            onPosMovY = false;
                            curNegMovY = event.position;
                        }
                        else
                        {
                            if(onPosMovY)
                            {
                                curPosMovY = event.position;
                            }
                            else
                            {
                                curNegMovY = event.position;
                            }
                        }
                    }
                }
                else
                {
                    if(event.axis == CharacterInputEvent::Axis::X)
                    {
                        if(event.position > 0)
                        {
                            onPosMovX = false;
                            curPosMovX = 0;
                        }
                        else
                        {
                            onPosMovX = true;
                            curNegMovX = 0;
                        }
                    }
                    else if(event.axis == CharacterInputEvent::Axis::Y)
                    {
                        if(event.position > 0)
                        {
                            onPosMovY = false;
                            curPosMovY = 0;
                        }
                        else
                        {
                            onPosMovY = true;
                            curNegMovY = 0;
                        }
                    }
                }
                updatedMovement.x = onPosMovX ? curPosMovX : curNegMovX;
                updatedMovement.y = onPosMovY ? curPosMovY :  curNegMovY;
                movementMagnitude = updatedMovement.x*updatedMovement.x + updatedMovement.y*updatedMovement.y; //sqrt(1) == 1 only perform the costly sqrt if neccessary

                //normalise if the magnitude is bigger than 1
                if(movementMagnitude > 1)
                {
                    movementMagnitude = std::sqrt(movementMagnitude);
                    updatedMovement /= movementMagnitude;
                }
                (*(motions.begin())).second.velocity = updatedMovement*150.0f;
                break;
        }
    }

    void InputHandlingSystem::onEvent(const MouseEvent& event)
    {
        sf::Vector2f mousePosition, resultingAngle;
		switch(MouseEvent::SubType subType = event.subType)
		{
			case MouseEvent::Move:
                mousePosition = sf::Vector2f(event.move.x, event.move.y);
				break;
			case MouseEvent::ButtonPressed: //continue to next case without breaking.
			case MouseEvent::ButtonReleased:
				mousePosition = sf::Vector2f(event.button.x, event.button.y);
				break;
		}
        resultingAngle = mousePosition-(*(transforms.begin())).second.transformable.getPosition();
        (*(transforms.begin())).second.transformable.setRotation(std::atan2(resultingAngle.y, resultingAngle.x)*(180.0f/MATH_PI));
    }
}
