/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "../../../definitions.hpp"
#include <SFML/Graphics/Drawable.hpp>
#include <memory>
namespace __GAME_NAMESPACE__
{
    class VisualComponent
    {
    public:
        std::vector< std::unique_ptr<sf::Drawable> > shapes;

        //happily discovered that this should be done by passing the transform into renderstates for visual objects, and it works well IMO for this case. not sure how I'm going to apply it to collision objects though, may not use sf::Shape for the collision aspect, as it doesn't even really appear to have added functionality for it, in which case.
    };
}
