/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "../common/System.hpp"
#include "../../input/CharacterInputEvent.hpp"
#include "../../input/MouseEvent.hpp"
#include "../common/components/MotionComponent.hpp"
#include "../common/components/TransformComponent.hpp"
#include "../common/Entity.hpp"
namespace __GAME_NAMESPACE__
{
    class InputHandlingSystem
    {
    private:
        std::map<EntityID, TransformComponent>& transforms;
        std::map<EntityID, MotionComponent>& motions;
        //these two are going to be used for testing the viability of using deactivation to indicate that the player wants to stop moving
        float curPosRotX, curNegRotX, curPosRotY, curNegRotY;
        float curPosMovX, curNegMovX, curPosMovY, curNegMovY;
        bool onPosRotX, onPosRotY, onPosMovX, onPosMovY; //lets try updating the rotation only on events where whatever is changing is increasing.
        //it's after this point that we'll need to introduce players and playerids into the system.
    public:
        InputHandlingSystem(std::map<EntityID, TransformComponent>& transforms, std::map<EntityID, MotionComponent>& motions);
        virtual void update();
        void onEvent(const MouseEvent&);
        void onEvent(const CharacterInputEvent&);
    };
}
