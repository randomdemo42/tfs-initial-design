/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
//the core of the Entity Compent System, it's where the memory for the entities and each of the component lists is stored, and each of the systems that run on the client are listed/instantiated here too.
#include "RenderingSystem.hpp"
#include "BasicDemoSystem.hpp"
#include "InputHandlingSystem.hpp"
#include "../common/components/TransformComponent.hpp"
#include "components/VisualComponent.hpp"
#include "../common/components/MotionComponent.hpp"
#include "../common/ECSCore.hpp"
#include <tuple>
namespace __GAME_NAMESPACE__
{

    //a client-engine for local-only play.
    class OfflineClientECSCore: public ECSCore
    {
    //I was thinking that all the component maps and systems should be public so that systems can access whatever they want and be created dynamically, long term some other layer/wrapper should probably be used to provide any desired access protection
    //but then I realised it would be easier for systems (where relevant, if it ever will be)  to be uniform by only giving them the component sets they want so that their implimentations aren't core-type dependant..
    //perhaps still pubilcise this and only give systems access to the components.
    //it would be possible to externally modify components in a way that breaks the logic of the systems, but the systems should be implemented safely enough that this doesn't cause the program to crash or hang, so that's acceptable?
    //this engine design is semi data-orientated.. I think?
    //TODO: make position component into PlacementComponent, including a position and a rotation.

//public for now? TODO: MAKE PRIVATE LATER MAYBE? MAINLY PUBLIC FOR SPAWNING PLAYER ENTITIES AND STUFF, THOUGH WE'LL ALSO PROBABLY GIVE FULL ACCESS TO THE CONSOLE SO. IDK.
//public is easier for now at least.
public:
        friend class System; //declaring systems as friend classes may be redundant... making
        std::map<EntityID, TransformComponent> transforms;
        std::map<EntityID, VisualComponent> visuals;
        std::map<EntityID, MotionComponent> motions; //note that if an entity has a motion ATM it will be assumed to also have a transform (actually it's difficult to think of an entity that won't have a transform)


        //the basic demo spawn system will simply turn all entities which are in the position system into visible rectangles for testing purposes;
        BasicDemoSystem basicDemoSystem;
        RenderingSystem renderingSystem;
        PhysicsSystem physicsSystem;
        InputHandlingSystem inputHandlingSystem;
    public:
        OfflineClientECSCore();
        virtual void destroyEntity(EntityID);

        //each of the stored systems gets singalled to update as well.
        void update();
        //
        // RenderingSystem& getRenderingSystem();
        // const RenderingSystem& getRenderingSystem() const;

    };
}
