/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "BasicDemoSystem.hpp"
namespace __GAME_NAMESPACE__
{
    BasicDemoSystem::BasicDemoSystem(ECSCore& core, std::map<EntityID, TransformComponent>& transforms, std::map<EntityID, MotionComponent>& motions): core(core), transforms(transforms), motions(motions)
    {}

    void BasicDemoSystem::update()
    {
        // std::map<EntityID, TransformComponent>::iterator transformsIt; //lazy use of auto instead of defining the
        // for(auto motionsIt = motions.begin(); motionsIt != motions.end(); ++motionsIt)
        // {
        //     sf::Vector2f eachPosition;
        //     transformsIt = transforms.find((*motionsIt).first);
        //     if(transformsIt != transforms.end())
        //     {
        //         eachPosition = (*transformsIt).second.transformable.getPosition();
        //         if(eachPosition.x <= 200 && eachPosition.y <= 200)
        //         {
        //             (*motionsIt).second.velocity = sf::Vector2f(100, 0);
        //             (*transformsIt).second.transformable.setRotation(0);
        //         }
        //         else if(eachPosition.x <= 200 && eachPosition.y >= 400)
        //         {
        //             (*motionsIt).second.velocity = sf::Vector2f(0, -100);
        //             (*transformsIt).second.transformable.setRotation(270);
        //         }
        //         else if(eachPosition.x >= 400 && eachPosition.y <= 200)
        //         {
        //             (*motionsIt).second.velocity = sf::Vector2f(0, 100);
        //             (*transformsIt).second.transformable.setRotation(90);
        //         }
        //         else if(eachPosition.x >= 400 && eachPosition.y >= 400)
        //         {
        //             (*motionsIt).second.velocity = sf::Vector2f(-100, 0);
        //             (*transformsIt).second.transformable.setRotation(180);
        //         }
        //     }
        //     else
        //     {
        //         throw std::string("basic-demo-system: ENTITY HAD A MOTION BUT NO TRANSFORM, THIS COULD BE HANDLED WITH A DEFAULT 0,0 POSITIONING BUT IS UNEXPECTED BEHAVIOUR");
        //     }
        // }

        sf::Vector2f eachPosition;
        //special shortcut demo, in actuality a dedicated system (possibly) or special collision logic will be used for this?
        //and it use something like a collision boundary or something (not sure what to do for partical effects, though their lifetime should be limited anyway I guess)
        for(auto it = transforms.begin(); it != transforms.end(); ++it)
        {
            eachPosition = (*it).second.transformable.getPosition();
            if(eachPosition.x < 100)
            {
                eachPosition.x = 100;
            }
            else if(eachPosition.x > 400)
            {
                eachPosition.x = 400;
            }

            if(eachPosition.y < 100)
            {
                eachPosition.y = 100;
            }
            else if(eachPosition.y > 400)
            {
                eachPosition.y = 400;
            }

            (*it).second.transformable.setPosition(eachPosition);
        }
    }
}
