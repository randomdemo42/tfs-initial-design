/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "RenderingSystem.hpp"
#include <SFML/Graphics/RenderTarget.hpp>
namespace __GAME_NAMESPACE__
{
    //note that current implementation doesn't prevent drawing beyond the canvas, leaving that as a TODO
    void RenderingSystem::draw(sf::RenderTarget& target, sf::RenderStates states) const
    {
        sf::Transform initialStatesTransform = states.transform.translate(getPosition());
        std::map<EntityID, TransformComponent>::iterator transformsIt; //lazy use of auto instead of defining the
        for(auto visualsIt = visuals.begin(); visualsIt != visuals.end(); ++visualsIt)
        {
            transformsIt = transforms.find((*visualsIt).first);
            if(transformsIt != transforms.end())
            {
                states.transform = initialStatesTransform;
                states.transform.combine((*transformsIt).second.transformable.getTransform());
                //transform the shapes by the optimisised version of reversing the last transform and applying the new one for the entity?
                for(auto shapesIt = (*visualsIt).second.shapes.begin(); shapesIt != (*visualsIt).second.shapes.end(); ++shapesIt)
                {
                    target.draw(*((*shapesIt).get()), states);
                }
            }
            else
            {
                throw std::string("basic-demo-system: ENTITY HAD A VISUAL BUT NO TRANSFORM, THIS COULD BE HANDLED WITH A DEFAULT 0,0 POSITIONING BUT IS UNEXPECTED BEHAVIOUR");
            }
        }
    }

    RenderingSystem::RenderingSystem(std::map<EntityID, TransformComponent>& transforms, std::map<EntityID, VisualComponent>& visuals, const sf::FloatRect& canvasArea): canvasArea(canvasArea), transforms(transforms), visuals(visuals)
    {}

    sf::FloatRect RenderingSystem::getBounds() const
    {
        return canvasArea;
    }

    sf::Vector2f RenderingSystem::getPosition() const
    {
        return sf::Vector2f(canvasArea.left, canvasArea.top);
    }

    void RenderingSystem::update()
    {
        //do nothing for now? may remove
    }

    void RenderingSystem::setCanvasArea(const sf::FloatRect& newArea)
    {
        canvasArea = newArea;
    }

}
