/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "OfflineClientECSCore.hpp"

namespace __GAME_NAMESPACE__
{

    OfflineClientECSCore::OfflineClientECSCore(): basicDemoSystem(*this, transforms, motions), renderingSystem(transforms, visuals), physicsSystem(transforms, motions), inputHandlingSystem(transforms, motions)
    {}

    void OfflineClientECSCore::destroyEntity(EntityID targetId)
    {
        //go through all the component lists and remove the target entity from them if it is in there
        auto transformsIt = transforms.find(targetId);
        if(transformsIt != transforms.end())
        {
            transforms.erase(transformsIt);
        }

        auto visualsIt = visuals.find(targetId);
        if(visualsIt != visuals.end())
        {
            visuals.erase(visualsIt);
        }

        auto motionsIt = motions.find(targetId);
        if(motionsIt != motions.end())
        {
            motions.erase(motionsIt);
        }
    }

    void OfflineClientECSCore::update()
    {

        physicsSystem.update();
        basicDemoSystem.update();
        renderingSystem.update();
    }

    // RenderingSystem& OfflineClientECSCore::getRenderingSystem()
    // {
    //     return renderingSystem;
    // }
    //
    // const RenderingSystem& OfflineClientECSCore::getRenderingSystem() const
    // {
    //     return renderingSystem;
    // }
}
