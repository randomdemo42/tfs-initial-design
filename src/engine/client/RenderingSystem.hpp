/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "../common/Entity.hpp"
#include "../common/PhysicsSystem.hpp"
#include "../common/System.hpp"
#include "components/VisualComponent.hpp"
#include "../common/components/TransformComponent.hpp"
#include "../../gui/GUIObject.hpp"
#include <map>
namespace __GAME_NAMESPACE__
{
    //this is a draft rendering engine, the canvasArea probably won't remain in the later versions, not sure
    class RenderingSystem: public GUIObject, public System
    {
    private:
        sf::FloatRect canvasArea;

        std::map<EntityID, TransformComponent>& transforms;
        std::map<EntityID, VisualComponent>& visuals;
        //may need access to more components later.
    protected:
        virtual void draw(sf::RenderTarget&, sf::RenderStates) const;
    public:
        RenderingSystem(std::map<EntityID, TransformComponent>& transforms, std::map<EntityID, VisualComponent>& visuals, const sf::FloatRect& canvasArea = sf::FloatRect()); //canvasArea indicates where to draw on the screen and the size of the physics world to draw, currently, by default, it draws below, right of the origin? or may try to draw everything but that's what everything is relative to, that may change, or everything may become static even

		virtual sf::FloatRect getBounds() const;
		virtual sf::Vector2f getPosition() const;
        void setCanvasArea(const sf::FloatRect&);

        virtual void update();
    };
}
