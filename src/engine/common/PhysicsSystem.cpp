/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "PhysicsSystem.hpp"
#include <string>
namespace __GAME_NAMESPACE__
{

    PhysicsSystem::PhysicsSystem(std::map<EntityID, TransformComponent>& transforms, std::map<EntityID, MotionComponent>& motions): transforms(transforms), motions(motions)
    {}
    void PhysicsSystem::update()
    {
        sf::Time curTime = theClock.restart();
        std::map<EntityID, TransformComponent>::iterator transformsIt; //lazy use of auto instead of defining the
        for(auto motionsIt = motions.begin(); motionsIt != motions.end(); ++motionsIt)
        {
            transformsIt = transforms.find((*motionsIt).first);
            if(transformsIt != transforms.end())
            {
                (*transformsIt).second.transformable.move((*motionsIt).second.velocity * curTime.asSeconds());
            }
            else
            {
                throw std::string("physics: ENTITY HAD A MOTION BUT NO TRANSFORM, THIS COULD BE HANDLED WITH A DEFAULT 0,0 POSITIONING BUT IS UNEXPECTED BEHAVIOUR");
            }
        }
    }
}
