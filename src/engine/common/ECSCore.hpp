/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "Entity.hpp"
#include <deque>
namespace __GAME_NAMESPACE__
{
    //the core of the ECS system, this class is responsible mainly for ensuring that IDs are properly kept distinct, that when an entity is destroyed, all it's components are destroyed, etc.
    //the list of components, and systems added to the engine are decided by the client and server core implementations.
    class ECSCore {
        //currently planning to recycle EntityIDs because locality is probably relative at best and creating entities nearby each other when they are created together is unlike to have that much locality benifit
        //may change later, we'll have to see?
    protected:


        //used IDs will always have one id from 0 to size-1 ifunused Ids is empty, so size() can be used for the new one.
        // std::deque<EntityID> usedIds;//usedIds may actually be redundant, since we can just check !unusedIds or something, and only keep a count of how many ids are used.
        EntityID nextAvailableID;
        std::deque<EntityID> unusedIds;

        //NOTE:TODO: probably going to need a UUID system for use across a network, noting that clients may in fact have more entities than their servers due to aesthetics or something, which might make things even more difficult.
        //as such, this id system will need to be revamped at that point, so I'm glad I did a using/typedef
        //perhaps adding to it a boolean that determines whether or not it is a locally-defined or server-defined id might help to allow clients and servers to freely generate entities suited to their needs without clashing?
        //worth considering.
    public:
        //creates two empty deques
        ECSCore();
        //may reuse an id if an entity was removed;
        EntityID createNewEntity();

        //extending classes must ensure that all components associated with the entity are destroyed as well.
        //for now, recycles entity ids. Note that if a distinction between locally generated and remote-sourced ids is drawn, only local-ids can be recycled, remote ids must be recycled remotely
        void destroyEntity(EntityID);
    };
}
