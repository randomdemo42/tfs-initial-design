/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "../../../definitions.hpp"
#include <SFML/System/Vector2.hpp>
#include "SFML/Graphics/Transformable.hpp"
namespace __GAME_NAMESPACE__
{
    //tracks the location, orientation (e.g. facing direction), etc of an entity as well as (potentially) other temporary effects such as scaling, however I'm less certain aboutusing it for scaling.
    class TransformComponent
    {
    public:
        // sf::Vector2f position;
        // float rotation; //the number of degrees by which the entity has been rotated (compared to some initial rotation?)
        // //transformation logic to pair up different components is not yet implimented?
        sf::Transformable transformable;
        //may replace this with a transform component and an sf::Transform object, but this is mainly used as a central point for the physics/logic to update, and then other system-specific components store their own transform, and compare it against this to determine what updates need to be applied.
        //as mentioned in the VisualComponent class, it may be better for those component's shapes to forced have a zero transform and not seperately store transforms for the components but we'll see, particularly it is more versatile to have the seperately stored transforms, such that parts like a mounted gun or something (including a swarm) can self-orientate within the one entity, although it might be easier even in cases like that to have a concept of child-entities?
        //we'll have to see,
    };
}
