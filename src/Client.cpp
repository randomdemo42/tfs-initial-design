/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Client.hpp"
#include <SFML/System/Sleep.hpp>
#include <functional>
#include <SFML/System/String.hpp>
#include "state/HomeState.hpp"
#include <fstream>

#ifdef WINDOWS
    #include <direct.h>
    #define GetCurrentDir _getcwd
#else
    #include <unistd.h>
    #define GetCurrentDir getcwd
 #endif

namespace __GAME_NAMESPACE__
{
	//remember that send a pointer to PrimaryStateManager to dynamically allocated member is dependant on the current
	Client::Client() : running(false), mainWindow(sf::VideoMode(1600, 900), "I don't have a real name for this yet"), console(defaultFont, mainWindow.getSize()), globalCommands(), centralInputHandler(this), sfmlEventHandler(centralInputHandler)
	{
        std::cout << "Twin-Stick-Fighter (Needs a better name)  Copyright (C) 2016 Marcos Erich Cousens-Schulz under GPL v3. \n"
        "This program comes with ABSOLUTELY NO WARRANTY;\n"
        "This is free software, and you are welcome to redistribute it .\n"
        "You should have received a copy of the GNU General Public License along with this program.\n"
        "If not, see <http://www.gnu.org/licenses/>."
        << std::endl;
        bool bindingLoadSuccess = false;
		if (!defaultFont.loadFromFile(__DEFAULT_FONT_FILEPATH__))
		{
			std::cout << "Failed to load the default font :c" << std::endl; //TODO: create an error log which writes to file probably at some point.
			exitApplication();
			return;
		}

        //gotta move this somewhere
        std::ifstream bindingsFileStream(__ACTIVE_BINDING_FILE_PATH__);
        Json::Reader reader;
        Json::Value bindingsData;
        try
        {
            reader.parse(bindingsFileStream, bindingsData, false);

            bindingLoadSuccess = centralInputHandler.setBindings(bindingsData);
        }
        catch(Json::RuntimeError err)
        {
            std::cout << err.what() << std::endl;
        }

        if(!bindingLoadSuccess)
        {
            std::cout << "An error occured when loading the current bindings, reverting to defaults." << std::endl;
            bindingsFileStream.close();
            bindingsFileStream.open(__DEFAULT_BINDING_FILE_PATH__);


            reader.parse(bindingsFileStream, bindingsData, false);

            bindingLoadSuccess = centralInputHandler.setBindings(bindingsData);
        }
        StateManager<State>::changeState<HomeState>(defaultFont, *this, centralInputHandler);
        //lazily setup the command list here in the constructor for now, move/improve later.
        globalCommands.insert(GUIConsole::CommandSet::value_type("repeat", [&](const sf::String& args) {console.logMessage(args); }));
        globalCommands.insert(GUIConsole::CommandSet::value_type("quitApplication", [&](const sf::String& args) { quit(); }));
        console.setCommands(globalCommands);
	}

	void Client::update()
	{
        StateManager::update();
		console.checkForUpdates();
	}
	void Client::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		StateManager::draw(target, states);
		if (console.isOpen())
		{
			target.draw(console, states);
		}

	}

	void Client::render()
	{
		mainWindow.clear();
		mainWindow.draw(*this);
		mainWindow.display();
	}

	Client::~Client()
	{
	}

    void Client::onEvent(const MiscInputEvent& event)
    {
        if(event.subType == MiscInputEvent::SubType::ConsoleToggle)
        {
            console.isOpen() ? console.close() : console.open();
        }
        else
        {
            PrimaryStateManager::onEvent(event);
        }
    }

    void Client::onEvent(const TextInputEvent& event)
    {
        if(event.unicode != 0x0000223C && event.unicode != 0x00000060) //grave-accent and tilde
        {
            if(console.isOpen())
            {
                console.onEvent(event);
            }
            else
            {
                PrimaryStateManager::onEvent(event);
            }
        }
    }

    void Client::onEvent(const MenuInputEvent& event)
    {
        if(console.isOpen())
        {
            console.onEvent(event);
        }
        else
        {
            PrimaryStateManager::onEvent(event);
        }
    }

    void Client::onEvent(const CharacterInputEvent& event)
    {
        if(!console.isOpen())
        {
            PrimaryStateManager::onEvent(event);
        }
    }

    void Client::onEvent(const MouseEvent& event)
    {
        if(!console.isOpen())
        {
            PrimaryStateManager::onEvent(event);
        }
    }

    //NOTE: CONSOLE NEEDS TO BE UPDATED TO HANDLE TextEntered events differently now, since it needs to manually ignore tilde and grave, and will have a different signature
    //also SFMLEventHandler's onEvent needs to be slightly different to accomodate these changes, etc

	// void Client::onEvent(const sf::Event& event)
	// {
	// 	//special events such as closing the window, opening or using the console do not go to the raw input handler, but others do.
	// 	if (event.type == sf::Event::EventType::KeyPressed && event.key.code == sf::Keyboard::Tilde)
	// 	{
	// 		console.isOpen() ? console.close() : console.open();
	// 	}
	// 	else if (event.type == sf::Event::Closed)
	// 	{
	// 		quit();
	// 		return;
	// 	}
	// 	else if (console.isOpen())
	// 	{
	// 		if (event.type == sf::Event::EventType::KeyPressed ||
	// 			(event.type == sf::Event::EventType::TextEntered &&
	// 				(event.text.unicode != 0x0000223C && event.text.unicode != 0x00000060) //tilde and grave accent respectively
	// 			)
	// 		)
	// 		{
	// 			console.onEvent(event);
	// 		}
	// 	}
	// 	else
	// 	{
	// 		sfmlEventHandler.onEvent(event);
	// 	}
	// }

	void Client::processEvents(sf::Window& targWindow)
	{
		if (targWindow.isOpen())
		{
			sf::Event event;
			while (targWindow.pollEvent(event))
			{
                if (event.type == sf::Event::Closed)
            	{
            		quit();
            		return;
            	}
                else if (event.type == sf::Event::EventType::KeyPressed && event.key.code == sf::Keyboard::Tilde)
                {
                    onEvent(MiscInputEvent(MiscInputEvent::SubType::ConsoleToggle));
                }
                else
                {
                    sfmlEventHandler.onEvent(event);
                }
			}
		}
		else
		{
			quit();
			return;
		}
	}

	void Client::run()
	{
		//using currentFrameDuration clock and timeToWait, this method should, in theory,
		//offset or eliminate differences in exeution times for each frame by adjusting the wait duration proportionally
		sf::Clock currentFrameDuration;
		sf::Time timeToWait;
		running = true;
		while (running)
		{
            currentFrameDuration.restart();
			processEvents(mainWindow);
			if (running)
			{
                update();
			}
			if (running)
			{
				render();
			}
		}
		exitApplication();
	}

	void Client::quit()
	{
		running = false;
	}

	void Client::exitApplication()
	{
		//nothing to do right now.
	}
}
