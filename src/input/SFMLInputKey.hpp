/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "InputKey.hpp"
namespace __GAME_NAMESPACE__
{
    class SFMLInputKey: public InputKey
    {
    public:
        enum Category
        {
            Unknown,
            KeyboardKey,
            JoystickAxis,
            JoystickButton,
            MouseButton,
            MouseWheel
        };

        Category category;
        //may not include mousemove since the event treats the axes as a pair and this may be difficult to meld with the other systems, but we can deal with that later?
        //the sign here (for axis and wheels), indicates whether positive (sign>=1), negative (sign<=-1), or both (sign==0) values for this axis trigger the event (note that by default either +ve or -ve will be selected when getting the input due to the deadzone and 0 must be set intuitively where the binding set is managed)
        union
        {
            sf::Keyboard::Key key;
            struct
            {
                sf::Joystick::Axis axis;
                int sign;
            } joystickAxis;
            unsigned int joystickButton;
            sf::Mouse::Button mouseButton;
            struct
            {
                sf::Mouse::Wheel wheel;
                int sign;
            } mouseWheel;
        };


        //this default constructor isn't meant to be considered a valid state, the Category will be unknown, and the union will be set to Unknown keyboard keys initially, since this is likely to be a value that has collisions on multiple keys from the source and thus not particularly be usable anyway.
        //despite actually being set, the union value should be considered defined for reading (more about stopping gleaming random data? or for map-ordering purposes, but it's semi-redundant)
        SFMLInputKey();
        SFMLInputKey(sf::Keyboard::Key); //keyboard key
        SFMLInputKey(sf::Mouse::Button); //mouse button
        SFMLInputKey(sf::Joystick::Axis, int sign); //joystick axis
        SFMLInputKey(unsigned int); //joystick button
        SFMLInputKey(sf::Mouse::Wheel, int sign); //joystick axis

        //if the supplied event doesn't correspond to a bindable input source, the result is equivilant to the default constructor.
        //possibly slightly innefficient compared to other constructors but since you basically need a switch or if-statement to determine where to get the data from, it's still an efficient enough way to create one /from/ an sf::Event&
        SFMLInputKey(const sf::Event&);

        //creates a Json::Value corresponding to this key.
        Json::Value jsonify() const;
        sf::String toString() const;

        bool operator<(const SFMLInputKey&) const;

        bool operator==(const SFMLInputKey&) const;

        //the second part of the pair indicates whether or not the supplied json creates a valid, recognisable SFMLInputKey, i.e. the first of the pair is considered to be valid/informative/non-garbage IFF the second is true;
        static std::pair<SFMLInputKey, bool> fromJson(const Json::Value&);
    };
}
