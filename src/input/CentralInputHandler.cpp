/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "CentralInputHandler.hpp"
#include <fstream>
namespace __GAME_NAMESPACE__
{
	CentralInputHandler::CentralInputHandler(PrimaryStateManager* primaryStateManager): primaryStateManager(primaryStateManager), currentRequest(nullptr)
	{
	}

	//not sure whether or not to templatise this but not for now.
	void CentralInputHandler::onEvent(const CharacterInputEvent& event)
	{
		primaryStateManager->onEvent(event);
	}
	void CentralInputHandler::onEvent(const MouseEvent& event)
	{
		primaryStateManager->onEvent(event);
	}
	void CentralInputHandler::onEvent(const MenuInputEvent& event)
	{
		primaryStateManager->onEvent(event);
	}
    void CentralInputHandler::onEvent(const MiscInputEvent& event)
	{
		primaryStateManager->onEvent(event);
	}
    void CentralInputHandler::onEvent(const TextInputEvent& event)
	{
		primaryStateManager->onEvent(event);
	}

    void CentralInputHandler::addSubhandler(InputHandler* newHandler)
    {
        subhandlers.push_back(newHandler);
    }

    //the bool return indicates success
    //remember to move the auto-windback behaviour out of the specific handler and into the central handler
    bool CentralInputHandler::setBindings(const Json::Value& bindingData)
    {
        bool success = true;
        for(auto it = subhandlers.begin(); it < subhandlers.end(); ++it)
        {
            success = success && (*it)->setBindings(bindingData);
        }
        return success;
    }

    //receive a request and fulfill it when the next input event occurs. Note that only one event should be used to fulfill any request even if multiple handlers are competing to fulfill the request.
    //only one request can be pending at a time.
    void CentralInputHandler::receiveInputKeyRequest(const std::shared_ptr<InputKeyRequest>& request)
    {
        currentRequest = request;
        for(auto it = subhandlers.begin(); it < subhandlers.end(); ++it)
        {
            (*it)->receiveInputKeyRequest(request);
        }
    }

    //cancel the request so that when one is completed, all the other handlers stop trying to fulfill it.
    void CentralInputHandler::cancelInputKeyRequest()
    {
        currentRequest = nullptr;
        for(auto it = subhandlers.begin(); it < subhandlers.end(); ++it)
        {
            (*it)->cancelInputKeyRequest();
        }
    }

    bool CentralInputHandler::hasPendingInputKeyRequest()
    {
        return currentRequest != nullptr;
    }
}
