/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include <SFML/Window/Event.hpp>
#include "InputEvent.hpp"
namespace __GAME_NAMESPACE__
{
	/**
	 * These events do not contain additional data as the event type indicates all that needs to be known?
	 */
	class MenuInputEvent: public InputEvent
	{
	public:
		enum SubType
		{
			// Focus, focus and blur are not input events, I'll need to reintroduce them as individual onBlur and onFocus methods
			// Blur,
			Up,
			Down,
			Left,
			Right,
			Activate,
			Cancel
		};

		SubType subType;

		MenuInputEvent(SubType type);

        virtual Json::Value jsonify() const;
        virtual sf::String toString() const;
        //the second part of the pair indicates whether or not the supplied json creates a valid, recognisable SFMLInputKey, i.e. the first of the pair is considered to be valid/informative/non-garbage IFF the second is true;
        static std::pair<MenuInputEvent, bool> fromJson(const Json::Value&);
	};


}
