/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "InputEvent.hpp"
#include <SFML/System/Vector2.hpp>
namespace __GAME_NAMESPACE__
{
	//this may need to be renamed but it relates to controlling the in-game character
	class CharacterInputEvent: public InputEvent
    {
    public:
        enum SubType
        {
            /*
            Rotation & Movement traits:
            x & y coords.
            each value:
              type: float
              min: -100f
              max: 100f
            */
            Rotation,
            Movement
        };

        enum Axis
        {
            X,
            Y
        };
        SubType subType;
        Axis axis;
        float position; //value between 1 and -1

        //if !activation, then deactivation, IE when a button is released, or an axis goes into the deadzone, relevant to start/stop firing, not yet sure how to relate it to movement where it's more of a cessation of input but breaks might be possible, though that's more of a stand alone control event.
        //may or may not end up keeping this later on, not sure yet.
        bool activation;
        CharacterInputEvent(SubType type, Axis axis, float value, bool activation = true);

        //TODO: DEAL WITH  HOW TO BIND INDIVIDUAL AXIS TO THE ACTUAL CHARACTER INPUTS AND WHATNOT, PERHAPS THEY SHULD BE AXIES INSTEAD FOR EXAMPLE, ETC, ETC

        //creates a Json::Value corresponding the current data
        virtual Json::Value jsonify() const;

        //gets a human-readable string representing the current data
        virtual sf::String toString() const;

        static std::pair<CharacterInputEvent, bool> fromJson(const Json::Value&);
    };
}
