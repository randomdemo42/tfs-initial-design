/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "CharacterInputEvent.hpp"
#include "MenuInputEvent.hpp"
#include "MouseEvent.hpp"
#include "../event/EventSource.hpp"
#include "../state/PrimaryStateManager.hpp"
#include "SFMLEventHandler.hpp"
#include "InputHandler.hpp"
#include <vector>

namespace __GAME_NAMESPACE__
{

	//central class for managing the collection of all other input handlers, although there is only one for now.
	class CentralInputHandler: public InputHandler
	{
	private:
		PrimaryStateManager* primaryStateManager;
        std::vector<InputHandler*> subhandlers;

        std::shared_ptr<InputKeyRequest> currentRequest; //not sure for now why we'd bother storing it in the central handler and each of the others considering that cancel doesn't take anything as parameters but for now it can't really hurt
	public:

		CentralInputHandler(PrimaryStateManager*);

        virtual void onEvent(const CharacterInputEvent&);
        virtual void onEvent(const MouseEvent&);
        virtual void onEvent(const MenuInputEvent&);
        virtual void onEvent(const MiscInputEvent&);
		virtual void onEvent(const TextInputEvent&);

        //possibly add more methods for working with the list of handlers? probably actually only need the add subhandler though? may remove the others

        void addSubhandler(InputHandler*);

        //the bool return indicates success
        //remember to move the auto-windback behaviour out of the specific handler and into the central handler
        virtual bool setBindings(const Json::Value&);

        //receive a request and fulfill it when the next input event occurs. Note that only one event should be used to fulfill any request even if multiple handlers are competing to fulfill the request.
        //only one request can be pending at a time.
        virtual void receiveInputKeyRequest(const std::shared_ptr<InputKeyRequest>&);

        //cancel the request so that when one is completed, all the other handlers stop trying to fulfill it.
        virtual void cancelInputKeyRequest();

        virtual bool hasPendingInputKeyRequest();

  };
}
