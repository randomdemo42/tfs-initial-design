/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "SFMLEventHandler.hpp"
#include "CentralInputHandler.hpp"
#include <iostream>
#include <cmath>
#include "MouseEvent.hpp"
#include <SFML/System/Sleep.hpp>
namespace __GAME_NAMESPACE__
{
    const float SFMLEventHandler::DeadzoneManager::MAX_DEADZONE_SIZE = 100;
    void SFMLEventHandler::DeadzoneManager::addDeadzone(sf::Joystick::Axis axis1, sf::Joystick::Axis axis2, float radius)
    {
        pairingMap.insert(std::make_pair(axis1, axis2));
        pairingMap.insert(std::make_pair(axis2, axis1));
        radii.push_back(radius*MAX_DEADZONE_SIZE);
        radiiMap.insert(std::pair< sf::Joystick::Axis, std::vector<float>::size_type>(axis1, radii.size()-1));
        radiiMap.insert(std::pair< sf::Joystick::Axis, std::vector<float>::size_type>(axis2, radii.size()-1));
    }

    bool SFMLEventHandler::DeadzoneManager::addFromJsonList(const Json::Value& src)
    {
        sf::Joystick::Axis eachAxis1, eachAxis2;
        float eachRadius;
        bool success = true;
        for(auto it = src.begin(); it != src.end(); ++it)
        {
            if((*it).isObject())
            {
                if((*it).isMember("axis_codes"))
                {
                    if((*it)["axis_codes"].isArray() && (*it)["axis_codes"].size() >= 1)
                    {
                        if((*it)["axis_codes"][0].isString())
                        {
                            //note that this set of ifs exists also exists inn SFMLInputKey and would need to be updated alongside
                            if((*it)["axis_codes"][0] == "X")
                            {
                                eachAxis1 = sf::Joystick::Axis::X;
                            }
                            else if((*it)["axis_codes"][0] == "Y")
                            {
                                eachAxis1 = sf::Joystick::Axis::Y;
                            }
                            else if((*it)["axis_codes"][0] == "Z")
                            {
                                eachAxis1 = sf::Joystick::Axis::Z;
                            }
                            else if((*it)["axis_codes"][0] == "R")
                            {
                                eachAxis1 = sf::Joystick::Axis::R;
                            }
                            else if((*it)["axis_codes"][0] == "U")
                            {
                                eachAxis1 = sf::Joystick::Axis::U;
                            }
                            else if((*it)["axis_codes"][0] == "V")
                            {
                                eachAxis1 = sf::Joystick::Axis::V;
                            }
                            else if((*it)["axis_codes"][0] == "PovX")
                            {
                                eachAxis1 = sf::Joystick::Axis::PovX;
                            }
                            else if((*it)["axis_codes"][0] == "PovY")
                            {
                                eachAxis1 = sf::Joystick::Axis::PovY;
                            }
                            else
                            {
                                success = false;
                                continue;
                            }
                        }
                        else
                        {
                            success = false;
                            continue;
                        }
                        if((*it)["axis_codes"].size() > 1)
                        {
                            if((*it)["axis_codes"][1].isString())
                            {
                                if((*it)["axis_codes"][1] == "X")
                                {
                                    eachAxis2 = sf::Joystick::Axis::X;
                                }
                                else if((*it)["axis_codes"][1] == "Y")
                                {
                                    eachAxis2 = sf::Joystick::Axis::Y;
                                }
                                else if((*it)["axis_codes"][1] == "Z")
                                {
                                    eachAxis2 = sf::Joystick::Axis::Z;
                                }
                                else if((*it)["axis_codes"][1] == "R")
                                {
                                    eachAxis2 = sf::Joystick::Axis::R;
                                }
                                else if((*it)["axis_codes"][1] == "U")
                                {
                                    eachAxis2 = sf::Joystick::Axis::U;
                                }
                                else if((*it)["axis_codes"][1] == "V")
                                {
                                    eachAxis2 = sf::Joystick::Axis::V;
                                }
                                else if((*it)["axis_codes"][1] == "PovX")
                                {
                                    eachAxis2 = sf::Joystick::Axis::PovX;
                                }
                                else if((*it)["axis_codes"][1] == "PovY")
                                {
                                    eachAxis2 = sf::Joystick::Axis::PovY;
                                }
                                else
                                {
                                    success = false;
                                    continue;
                                }
                            }
                            else
                            {
                                success = false;
                                continue;
                            }
                        }
                        else
                        {
                            eachAxis2 = eachAxis1;
                        }

                        if((*it).isMember("radius") && (*it)["radius"].isNumeric())
                        {
                            eachRadius = (*it)["radius"].asFloat();
                            addDeadzone(eachAxis1, eachAxis2, eachRadius);
                        }
                        else
                        {
                            success = false;
                            continue;
                        }
                    }
                    else if((*it)["axis_codes"].isString())
                    {
                        //note that this set of ifs exists also exists inn SFMLInputKey and would need to be updated alongside
                        if((*it)["axis_codes"] == "X")
                        {
                            eachAxis1 = sf::Joystick::Axis::X;
                        }
                        else if((*it)["axis_codes"] == "Y")
                        {
                            eachAxis1 = sf::Joystick::Axis::Y;
                        }
                        else if((*it)["axis_codes"] == "Z")
                        {
                            eachAxis1 = sf::Joystick::Axis::Z;
                        }
                        else if((*it)["axis_codes"] == "R")
                        {
                            eachAxis1 = sf::Joystick::Axis::R;
                        }
                        else if((*it)["axis_codes"] == "U")
                        {
                            eachAxis1 = sf::Joystick::Axis::U;
                        }
                        else if((*it)["axis_codes"] == "V")
                        {
                            eachAxis1 = sf::Joystick::Axis::V;
                        }
                        else if((*it)["axis_codes"] == "PovX")
                        {
                            eachAxis1 = sf::Joystick::Axis::PovX;
                        }
                        else if((*it)["axis_codes"] == "PovY")
                        {
                            eachAxis1 = sf::Joystick::Axis::PovY;
                        }
                        else
                        {
                            success = false;
                            continue;
                        }
                        if(success)
                        {
                            eachAxis2 = eachAxis1;
                        }
                    }
                    else
                    {
                        success = false;
                        continue;
                    }
                }
                else
                {
                    success = false;
                    continue;
                }
            }
            else
            {
                success = false;
                continue;
            }
        }
        return success;
    }

    bool SFMLEventHandler::DeadzoneManager::isActive(const sf::Event::JoystickMoveEvent& event, bool ignoreCrossAxis) const
    {
        sf::Joystick::Axis axis1 = event.axis, axis2;
        auto it = pairingMap.find(axis1);
        float deadzoneRadius;
        if(it == pairingMap.end())
        {
            std::cout << "not mapped" << std::endl;
            //if not mapped, then assume the no deadzone, which means we don't need to worry about another axis anyway really, since we aren't clipping the values at all
            return event.position != 0;
        }
        else
        {
            axis2 = (*it).second;
            //if it's in the pairing map it's assumed to be in the radiiMap;
            deadzoneRadius = radii[(*radiiMap.find(axis1)).second];
            //the !axis1 == axis2 may or may not be an efficiency gain, I'm figuring less calls into the sfml system are probably good?
            return std::abs(event.position) > deadzoneRadius ||  (!ignoreCrossAxis && !axis1 == axis2 && std::abs(sf::Joystick::getAxisPosition(event.joystickId, axis2)) > deadzoneRadius);
        }

    }

    float SFMLEventHandler::DeadzoneManager::getScaledValue(const sf::Event::JoystickMoveEvent& event, bool ignoreCrossAxis) const
    {
        sf::Joystick::Axis axis1 = event.axis, axis2;
        auto it = pairingMap.find(axis1);
        float deadzoneRadius;
        float targetValue, pairedValue;
        float rawMagnitude;
        if(it == pairingMap.end())
        {
            //if not mapped, then assume the no deadzone, which means we don't need to worry about another axis anyway really, since we aren't clipping the values at all
            return event.position/MAX_DEADZONE_SIZE;
        }
        else
        {
            axis2 = (*it).second;
            //if it's in the pairing map it's assumed to be in the radiiMap;
            deadzoneRadius = radii[(*radiiMap.find(axis1)).second];
            if(axis1 == axis2 || ignoreCrossAxis)
            {
                return (event.position-deadzoneRadius)/(MAX_DEADZONE_SIZE-deadzoneRadius);
            }
            else
            {
                targetValue = event.position; //we'll only
                pairedValue = sf::Joystick::getAxisPosition(event.joystickId, axis2);
                rawMagnitude = std::sqrt(targetValue*targetValue + pairedValue*pairedValue);
                // std::cout << "original: " << targetValue << " result: " << (targetValue/rawMagnitude) * ( (rawMagnitude-deadzoneRadius)/(MAX_DEADZONE_SIZE-deadzoneRadius) ) << std::endl;
                return (targetValue/rawMagnitude) * ( (rawMagnitude-deadzoneRadius)/(MAX_DEADZONE_SIZE-deadzoneRadius) );
            }
        }
    }

    SFMLEventHandler::SFMLEventHandler(CentralInputHandler& centralInputHandler): centralInputHandler(centralInputHandler), currentRequest(nullptr)
    {
        centralInputHandler.addSubhandler(this);
    }

    bool SFMLEventHandler::setBindings(const Json::Value& newBindings)
    {

        std::map< SFMLInputKey, std::vector<MenuInputEvent> > newMenuInputBindings;
        std::map< SFMLInputKey, std::vector<CharacterInputEvent> > newCharacterInputBindings;
        DeadzoneManager newDeadzoneManager;

        //decode the bindings.
        //if no errors, store the bindings as the current ones, if errors, restore the previous bindings.
        bool success = true;

        //using reference_wrapper so that a full memory copy of all the data isn't performed, (jsoncpp performs deep copy)

        std::reference_wrapper<const Json::Value> currentObject(newBindings);
        std::reference_wrapper<const Json::Value> eachBinding(currentObject);
        std::reference_wrapper<const Json::Value> eachBindingSourceList(currentObject);
        std::reference_wrapper<const Json::Value> eachBindingSource(currentObject);
        std::reference_wrapper<const Json::Value> eachBindingTarget(currentObject);
        std::pair<SFMLInputKey, bool> eachInputKey; //the bool indicates whether or not the key is in a valid/readable state.
        std::pair<MenuInputEvent, bool> eachMenuTarget = MenuInputEvent::fromJson(Json::Value());
        std::pair<CharacterInputEvent, bool> eachCharacterTarget = CharacterInputEvent::fromJson(Json::Value());
        if(newBindings.isMember("sfml_specific_settings"))
        {
            if(!newBindings["sfml_specific_settings"].isObject())
            {
                success = false;
            }
            else
            {
                if(newBindings["sfml_specific_settings"].isMember("axis_deadzones"))
                {
                    if(!newBindings["sfml_specific_settings"]["axis_deadzones"].isArray())
                    {
                        success = false;
                    }
                    else
                    {
                        success = newDeadzoneManager.addFromJsonList(newBindings["sfml_specific_settings"]["axis_deadzones"]);
                    }
                }
            }
        }
        if(!success || !newBindings.isMember("bindings") || !newBindings["bindings"].isArray())
        {
            success = false;
        }
        else
        {
            currentObject = newBindings["bindings"];

            for(int i = 0; i < currentObject.get().size(); ++i)
            {
                eachBinding = currentObject.get()[i];

                //TODO: these below could probably, to an extent be moved to their own file/class (probably into CharacterInputEvent etc themselves)
                if(!eachBinding.get().isMember("target") || !eachBinding.get()["target"].isObject())
                {
                    success = false;
                    continue;
                    //success is set to false but we'll let it_range.first continue processing what it_range.first can for now, make further decisions later?
                }
                else
                {
                    eachBindingTarget = eachBinding.get()["target"];

                    if(!eachBindingTarget.get().isMember("event_type") || !eachBindingTarget.get()["event_type"].isString())
                    {
                        success = false;
                        continue;
                    }
                    else
                    {
                        if(eachBindingTarget.get()["event_type"] == "MenuInputEvent")
                        {
                            eachMenuTarget = MenuInputEvent::fromJson(eachBindingTarget);
                            if(!eachMenuTarget.second)
                            {
                                success = false;
                                continue;
                            }
                        }
                        else if(eachBindingTarget.get()["event_type"] == "CharacterInputEvent")
                        {
                            eachCharacterTarget = CharacterInputEvent::fromJson(eachBindingTarget);
                            if(!eachCharacterTarget.second)
                            {
                                success = false;
                                continue;
                            }
                        }

                        if(!eachBinding.get().isMember("sources") || !eachBinding.get()["sources"].isArray())
                        {
                            success = false;
                            continue;
                        }
                        else
                        {
                            eachBindingSourceList = eachBinding.get()["sources"];
                            for(int j = 0; success && j < eachBindingSourceList.get().size(); ++j)
                            {
                                if(eachBindingSourceList.get()[j].isObject())
                                {
                                    eachInputKey = SFMLInputKey::fromJson(eachBindingSourceList.get()[j]);
                                    //only try to store the binding if it_range.first's considered valid.
                                    if(eachInputKey.second)
                                    {
                                        //if no fails occured, set the binding onto the temp/new list
                                        if(eachBindingTarget.get()["event_type"] == "MenuInputEvent")
                                        {
                                            newMenuInputBindings[eachInputKey.first].emplace_back(eachMenuTarget.first);
                                        }
                                        else
                                        {
                                            newCharacterInputBindings[eachInputKey.first].emplace_back(eachCharacterTarget.first);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if(success)
        {
            menuInputBindings = newMenuInputBindings;
            characterInputBindings = newCharacterInputBindings;
            deadzoneManager = newDeadzoneManager;
        }
        else
        {
            std::cout << "Failed to load new bindings, reverting to previos bindings" << std::endl;
            //LOG ERROR? probably put it_range.first in the places where success is set to false though
        }
        return success;
    }

    //TODO: PROBABLY CONDENSE THIS METHOD DOWN A LOT
    //pre-condition: matches is the vector of MenuInputEvents bound to the key, and the key corresponds to the srcEvent
    void SFMLEventHandler::processAsPotentialEvent(const sf::Event& srcEvent, const SFMLInputKey& key, const std::vector<MenuInputEvent>& matches)
    {
        for(auto it = matches.begin(); it != matches.end();++it)
        {
            //this means that having decoding of target
            switch(sf::Event::EventType eventType = srcEvent.type)
            {
                case sf::Event::EventType::KeyPressed:
                    centralInputHandler.onEvent((*it));
                    break;

                case sf::Event::EventType::JoystickMoved:
                    centralInputHandler.onEvent((*it));
                    break;
                case sf::Event::EventType::JoystickButtonPressed:
                    centralInputHandler.onEvent((*it));
                    break;
                case sf::Event::EventType::MouseButtonPressed:
                    centralInputHandler.onEvent((*it));
                    break;
                case sf::Event::EventType::MouseWheelScrolled:
                    //discrete event, non-discrete (multiplicative?)
                    //fire one event for every whole tick (may be none?)
                    int tickCount = std::floor(std::abs(srcEvent.mouseWheelScroll.delta));
                    while(tickCount > 0)
                    {
                        centralInputHandler.onEvent((*it));
                        --tickCount;
                    }
                    break;
            }
        }
    }

    void SFMLEventHandler::processAsPotentialEvent(const sf::Event& srcEvent, const SFMLInputKey& key, const std::vector<CharacterInputEvent>& matches)
    {
        CharacterInputEvent eachResultEvent(CharacterInputEvent::SubType::Rotation, CharacterInputEvent::Axis::X, 0);
        for(auto it = matches.begin(); it != matches.end();++it)
        {
            eachResultEvent = (*it);
            //NOTE: a non-activation movement event should be considered to correspond to a vector(0,0) effectively and for any axis of the event's vector that is set to a non-zero value, the engine should consider the player to applying no thrust in those directions? (characterinputevents with 0 for a given axis don't modify that axis, so supplying non-zero value indicates which axies to 'hault')
            eachResultEvent.activation = determineInputState(srcEvent);
            switch(sf::Event::EventType eventType = srcEvent.type)
            {
                case sf::Event::EventType::KeyPressed:
                case sf::Event::EventType::KeyReleased:
                    centralInputHandler.onEvent(eachResultEvent);
                    break;
                case sf::Event::EventType::JoystickMoved:
                    if(eachResultEvent.activation)
                    {
                        //this is so that the axis-direction doesn't dictate the movement direction - the binding-target does.
                        eachResultEvent.position = (*it).position * std::abs(deadzoneManager.getScaledValue(srcEvent.joystickMove)); //devide by 100 which is the max value of sfml axies
                    }
                    centralInputHandler.onEvent(eachResultEvent);
                    break;
                case sf::Event::EventType::JoystickButtonPressed:
                case sf::Event::EventType::JoystickButtonReleased:
                    centralInputHandler.onEvent(eachResultEvent);
                    break;
                case sf::Event::EventType::MouseButtonPressed:
                case sf::Event::EventType::MouseButtonReleased:
                    centralInputHandler.onEvent(eachResultEvent);
                    break;
                case sf::Event::EventType::MouseWheelScrolled:
                    //discrete event, non-discrete (multiplicative?)
                    //fire one event for every whole tick (may be none?)
                    int tickCount = std::floor(std::abs(srcEvent.mouseWheelScroll.delta));
                    while(tickCount > 0)
                    {
                        centralInputHandler.onEvent(eachResultEvent);
                        --tickCount;
                    }
                    break;
            }
        }
    }

    void SFMLEventHandler::onEvent(const sf::Event& event)
    {
        bool alreadyHadPending = hasPendingInputKeyRequest();
        bool oldInputStatus = false, newInputStatus = false; //assume that the input wasn't already active unless we learn that it is?
        SFMLInputKey inputKey, altKey;

        //reference_wrapper to avoid complex deep copy

        //these are the vectors of context-events that a key matches to
        std::map< SFMLInputKey, std::vector<MenuInputEvent> >::iterator menuBindingIt;
        std::map< SFMLInputKey, std::vector<CharacterInputEvent> >::iterator characterBindingIt;

    	if (event.type == sf::Event::EventType::KeyPressed && event.key.code == sf::Keyboard::Tilde)
    	{
    		centralInputHandler.onEvent(MiscInputEvent(MiscInputEvent::ConsoleToggle));
    	}
    	else if(event.type == sf::Event::EventType::TextEntered)
        {
            centralInputHandler.onEvent(TextInputEvent(event.text));
        }
        else
        {
            if (event.type == sf::Event::MouseMoved)
    		{
    			centralInputHandler.onEvent(MouseEvent(event.mouseMove));
    		}
    		else
            {
                //issue the standard MouseButton event AND check for bindings
                if (event.type == sf::Event::MouseButtonPressed || event.type == sf::Event::MouseButtonReleased)
                {
                    centralInputHandler.onEvent(MouseEvent(event.mouseButton, event.type == sf::Event::MouseButtonPressed));
                }

                inputKey = SFMLInputKey(event);
                if(inputKey.category != SFMLInputKey::Category::Unknown)
                {
                    menuBindingIt = menuInputBindings.find(inputKey);
                    if(menuBindingIt != menuInputBindings.end()) //for some reason the return value of upper_bound/equal_range().second is corrupting after each compare so the lookup needs to occur every time?
                    {
                        if(inputKey.category == SFMLInputKey::Category::MouseWheel)
                        {
                            processAsPotentialEvent(event, inputKey, (*menuBindingIt).second); //always fire MouseWheel events, don't event bother updating the activation map
                        }
                        else
                        {
                            oldInputStatus = inputActivationStatusMap[inputKey];
                            inputActivationStatusMap[inputKey] = newInputStatus = determineInputState(event, true); //update the stored input status for next time

                            //for menu events at least we want to block repeats, only send the event if it_range.first wasn't already active.
                            if(!oldInputStatus && newInputStatus)
                            {
                                processAsPotentialEvent(event, inputKey, (*menuBindingIt).second);
                            }
                        }
                    }

                    //character events aren't really concerned with the old activation state in the same way, they deal with repetition differently depending on the event target and the context so just let the method for handling it make such decisions
                    characterBindingIt = characterInputBindings.find(inputKey);
                    if(characterBindingIt != characterInputBindings.end())
                    {
                        processAsPotentialEvent(event, inputKey, (*characterBindingIt).second);
                    }

                    if(inputKey.category == SFMLInputKey::Category::JoystickAxis && inputKey.joystickAxis.sign == 0) //if we get a proper deadzone/zero-value on the joystick we won't match any signed inputs so we'll need to check against both sides and at the least update the input status for both of them
                    {
                        altKey = inputKey;
                        altKey.joystickAxis.sign = -1;
                        //do once for the negative, then for the position, bit of a nasty hack syntactically though I guess;
                        do
                        {
                            menuBindingIt = menuInputBindings.find(altKey);
                            if(menuBindingIt != menuInputBindings.end()) //for some reason the return value of upper_bound/equal_range().second is corrupting after each compare so the lookup needs to occur every time?
                            {
                                if(altKey.category == SFMLInputKey::Category::MouseWheel)
                                {
                                    processAsPotentialEvent(event, altKey, (*menuBindingIt).second);
                                }
                                else
                                {
                                    oldInputStatus = inputActivationStatusMap[altKey];
                                    inputActivationStatusMap[altKey] = newInputStatus = determineInputState(event, true); //update the stored input status for next time

                                    //for menu events at least we want to block repeats, only send the event if it_range.first wasn't already active.
                                    if(!oldInputStatus && newInputStatus)
                                    {
                                        processAsPotentialEvent(event, altKey, (*menuBindingIt).second);
                                    }
                                }
                            }

                            //character events aren't really concerned with the old activation state in the same way, they deal with repetition differently depending on the event target and the context so just let the method for handling it make such decisions
                            characterBindingIt = characterInputBindings.find(altKey);
                            if(characterBindingIt != characterInputBindings.end())
                            {
                                processAsPotentialEvent(event, altKey, (*characterBindingIt).second);
                            }

                            //flip the sign
                            altKey.joystickAxis.sign = -altKey.joystickAxis.sign;
                        }
                        while (altKey.joystickAxis.sign != -1);
                    }
                    //the request may have been canceled by this event
                    if(alreadyHadPending && hasPendingInputKeyRequest() && determineInputState(event, true)) //not really concerned with the error margin of the floating point type here, as the joystick shouldn't be hovering too close to the deadzoner (moreover ignoring this hovering/error margin is the point of the deadzone)
                    {
                        currentRequest->fulfill(inputKey);
                        centralInputHandler.cancelInputKeyRequest();
                    }
                }
            }
        }
    }

    void SFMLEventHandler::receiveInputKeyRequest(const std::shared_ptr<InputKeyRequest>& request)
    {
        currentRequest = request;
    }

    void SFMLEventHandler::cancelInputKeyRequest()
    {
        currentRequest = nullptr;
    }

    bool SFMLEventHandler::hasPendingInputKeyRequest()
    {
        return currentRequest != nullptr;
    }

    bool SFMLEventHandler::determineInputState(const sf::Event& event, bool ignoreCrossAxis) const
    {
        bool result;
        switch(sf::Event::EventType type = event.type)
        {
            case sf::Event::EventType::KeyPressed:
            case sf::Event::EventType::MouseButtonPressed:
            case sf::Event::EventType::JoystickButtonPressed:
            case sf::Event::EventType::MouseWheelScrolled:
                result = true;
                break;
            case sf::Event::EventType::JoystickMoved:
                result = deadzoneManager.isActive(event.joystickMove, ignoreCrossAxis);
                break;
            case sf::Event::EventType::KeyReleased:
            case sf::Event::EventType::MouseButtonReleased:
            case sf::Event::EventType::JoystickButtonReleased:
            default:
                result = false;
        }
        return result;
    }

    //ADD THINGS FOR THE OTHER SFML EVENT INPUTS TO HAVE THEM MAPPED TOO?
}
