/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "MenuInputEvent.hpp"
#include "CharacterInputEvent.hpp"
#include "../jsoncpp/json/json.h"
#include "InputKeyRequest.hpp"
namespace __GAME_NAMESPACE__
{
    //base class for input handlers that handle actual inputs and produce the custom input events from them.
    class InputHandler
    {

    public:
        // virtual void update() = 0; //let the updating process be managed elsewhere for now I guess?

        // virtual void restoreDefaultBindings() = 0;

        //the bool return indicates success
        //remember to move the auto-windback behaviour out of the specific handler and into the central handler
        virtual bool setBindings(const Json::Value&) = 0;

        // //notice: does not filter out failed bindings for now?
        // virtual Json::Value getCurrentBindings() const = 0; //possibly not have each and every InputHandler have this method? that or each live-binding-thing need to be coupled with their Json::value?

        //receive a request and fulfill it when the next input event occurs. Note that only one event should be used to fulfill any request even if multiple handlers are competing to fulfill the request.
        //only one request can be pending at a time.
        virtual void receiveInputKeyRequest(const std::shared_ptr<InputKeyRequest>&) = 0;

        //cancel the request so that when one is completed, all the other handlers stop trying to fulfill it.
        virtual void cancelInputKeyRequest() = 0;

        virtual bool hasPendingInputKeyRequest() = 0;

    };
}
