/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "MenuInputEvent.hpp"
#include "CharacterInputEvent.hpp"
#include "../jsoncpp/json/json.h"
#include "../event/EventListener.hpp"
#include "InputHandler.hpp"
#include "InputKeyRequest.hpp"
#include <unordered_map>
#include "SFMLInputKey.hpp"

namespace __GAME_NAMESPACE__
{
    class CentralInputHandler;

    //TODO: add another class as a base for this which is a special type of event source that takes a raw input handlr, a Json::Value for updating bindings and dispatches events to the raw input handler
    //TODO: Add a way to report "incompatible source-input pair" or something to the console, gives a good opportunity to figure out how to incorporate the console into the system.
    class SFMLEventHandler: public InputHandler, public EventListener<sf::Event>
    {
    private:
        //implementation concept for scaled radial deadzones, uses obne map to find the paired axis (axis: axis map) + a map from a pair of axies to the deadzone value.
        //slightly less universally flexible implementation - only works for sfml joystick axies, but another more universal implementation might be implementable later? though I'm not really sure that you'd ever want to pair axies under different handlers to form radial deadzones? And writing a universal compator doesn't seem easy to manage - for types that aren't yet known, they would need to have some kind of internal identifier somehow garaunteed not to overlap, altough we could to make it so that they are ccompared as arrays on integers, and mod-load-order stuff handled the different types but this gets into modding, and seems like an entirely different ball-game for now, so I'll  just make this implementation easy to remove if need be later
        //also do we want radial deadzones but still per-axis deadzone values? Nah, radial deadzones, but maybe per-axis sensitivity modifiers

        //single-axis deadzones have a slightly different logic, since they still need scaling but not as a vector. and duplicating their axis value would creating vetcors of double the possible magnitude?
        class DeadzoneManager
        {
        public:
            static const float MAX_DEADZONE_SIZE; //the maximum possible value for an sfml joystick axis to take.
        private:
            //note that this data structuring doesn't prevent the pairings being spliced incorrectly via duplicate entries, (e.g. <X,Y> then <Y,Z>) since the map will ignore all but the first attempts at insert for a given key so only the first entry will be used, but the Z in this case would still check against Y so it's still broken. TODO: Possibly create a test and warning for this later?
            std::map<sf::Joystick::Axis, sf::Joystick::Axis> pairingMap; //two entries for each pair, one a-to-b, then b-to-a, (note that axial deadzones if really desired can be created by pairing an axis with itself, which results in the same amount of processsing as radial, but doesn't produce two entries)

            std::vector<float> radii;

            //map from joystick-axis to the deadzone value (which is stored in a separate vector, not for efficiency but for data-consistancy I guess?d), two entries per-pair but both entries should in theory point to the same value, just because of how the data is to be entered into it
            std::map<sf::Joystick::Axis, std::vector<float>::size_type> radiiMap;

            void addDeadzone(sf::Joystick::Axis, sf::Joystick::Axis, float radius); //the radius for this method should be given as a value between 0 and 1, then that will be used to scale against MAX_DEADZONE_SIZE to determine the value to store
        public:
            //clears existing deadzones then adds all the deadzones found in a list of deadzones in JSON format, returns true if all deadzones were successfully added, false if any of them fail.
            bool addFromJsonList(const Json::Value&);

            //determines whether or not the raw value from the event and it's paired axis are far enough to be outside of the deadzone
            //ignoreCrossAxis can be used to strictly only look at the current axis to determine results (e.g. for menu events so that the cross-axis, if one exists, doesn't accidentally trigger it)
            bool isActive(const sf::Event::JoystickMoveEvent& event, bool ignoreCrossAxis = false) const;

            //returns the scaled value from the event, such that 'ramping' starts from the deadzone rather than the center.
            //pre-condition: isActive() would return true for the event, as it will not be checked, and if false, may give weird values back.
            //return type will be a value between 0 and 1. At the moment
            //ignoreCrossAxis can be used to strictly only look at the current axis to determine results (e.g. for menu events so that the cross-axis, if one exists, doesn't accidentally trigger it)
            float getScaledValue(const sf::Event::JoystickMoveEvent& event, bool ignoreCrossAxis = false) const;

            //removes all stored deadzones;
            void clear();
        };

        CentralInputHandler& centralInputHandler;

		// //one for every unique key, there are KeyCount keys, enum is convertable to int it seems.
		// bool keyStates[sf::Keyboard::Key::KeyCount];

        //create a static method for decoding bound-target's (both menu and character controls in the one decoder? not sure?) json thingies though?, possibly with a similar unionised return type as above?
        //this would be outside of the individual event handlers and thus suitably modular?

        //don't think there's anything more to add to comments/ideas today?

        //oh yeah, find a way to modularise the way that centralInputHandler checks for new events by adding subhandlers (the SFML event handler could be submitted post-construction(and the client class can give the window to it for it to request events from before handing it on to the centralInputHandler))?
        //even though it's kind of an innefficiency this SFMLEventHandler could replace the other centralInputHandler later with relative ease which would resolve efficiencies I think?
        //does this shed some light on how to deal with console??

        //though I guess it may never see practical use and is partially an efficiency loss, in a way it's partially just an exercise though anyway.

        //do NOT merge these two maps, they are allowed to have overlap.
        //do not merge because both events can occur simultaneously.
        //IMPORTANT


        //using unordered_multimap instead of multimap for now because of what appears to be a g++ bug


        std::map<SFMLInputKey, std::vector<MenuInputEvent> > menuInputBindings; //hold instances of the menu event themselves since all they are is their subtype effectively, maybe optimise the surrounding state out later?

        std::map<SFMLInputKey, std::vector<CharacterInputEvent> > characterInputBindings; //hold instances of the character event to indicate the direction of directional movement etc (note that this gives an option for keyboard keys to strafe in any direction they want, note however that  TODO: the sum of the two axies should be capped to ensure a radius of 1 though when reading the input (this should be done at main-engine level maybe? not sure, maybe it should just be taken as a direction and always bounded by one by the input)
        // std::map<SFMLInputKey, MouseEvent::EventType> mouseEventBindings; //mice need not be a binding target at all silly

        std::shared_ptr<InputKeyRequest> currentRequest;

        ////the unsigned int is for the playerid which isn't implemented for the rest of the system yet so we'll leave it for now. possbibly use a pointer to a player object which ids which map to search and stuff as well, or use a vector of maps, whatever.
        //std::map<std::pair<unsigned int, SFMLInputKey, bool> repeatBlock; //tracks whether or not an input is currently 'active' (a press event without a release event, a joystick axis being outside of it's deadzone, etc)
        std::map<SFMLInputKey, bool> inputActivationStatusMap;

        DeadzoneManager deadzoneManager;
        //note that this doesn't handle non-bindable inputs like the mouse movement (decided that mouse buttons should be bindable but also always fires the normal mouse event?)

        void processAsPotentialEvent(const sf::Event& srcEvent, const SFMLInputKey& key, const std::vector<MenuInputEvent>& matches);
        void processAsPotentialEvent(const sf::Event& srcEvent, const SFMLInputKey& key, const std::vector<CharacterInputEvent>& matches);

        //updates the current status of an input and whether or not it is currently 'active' (e.g. key is pressed down)
        //ignoreCrossAxis only applies to sf::JoystickMoved events (otherwise it is ignored), and can be used to strictly only look at the current axis to determine results (e.g. for menu events so that the cross-axis, if one exists, doesn't accidentally trigger a menu event)
        bool determineInputState(const sf::Event&, bool ignoreCrossAxis = false) const;
    public:
        //it's a normal constructor
        // template<class PrimaryInputListener, typename std::enable_if< std::is_base_of< EventListener<CharacterInputEvent>, T >::value && std::is_base_of< EventListener<MenuInputEvent>, T >::value, void>::type >
        SFMLEventHandler(CentralInputHandler&);

        //updates on the bindings found in the supplied json value, dumping any old ones for the appropriate produced event type
        //remember to check for viability of the binding.
        //mainly for loading from file and stuff.
        //the bool return indicates success
        virtual bool setBindings(const Json::Value&);

        // //IMPORTANT NOTE THAT THIS  CURRENTLY RETURNS SOMETHING INVAILID AND IS ONLY A PLACEHOLDER?
        // //update this so that each of the subhandlers contribute!
        // virtual Json::Value getCurrentBindings() const;

        // virtual void restoreDefaultBindings();

        virtual void receiveInputKeyRequest(const std::shared_ptr<InputKeyRequest>&);
        virtual void cancelInputKeyRequest();
        virtual bool hasPendingInputKeyRequest();


        //WREMEMBER TO ADD A PLAYERID TO THE BOUND-EVENTS?

        //process inputs and possibly generate our custom in-context events
        virtual void onEvent(const sf::Event&);

    };
}
