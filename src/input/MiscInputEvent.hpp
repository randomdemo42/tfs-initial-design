/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "InputEvent.hpp"
namespace __GAME_NAMESPACE__
{
    //note that although additional shortcuts can be given for console toggling, tilde will always be bound to console toggling, incase bindings fail?
    class MiscInputEvent: public InputEvent
    {
    public:
        enum SubType
        {
            PauseToggle,
            ConsoleToggle
        };
        SubType subType;
        MiscInputEvent(SubType);

        virtual Json::Value jsonify() const;
        virtual sf::String toString() const;
        //the second part of the pair indicates whether or not the supplied json creates a valid, recognisable SFMLInputKey, i.e. the first of the pair is considered to be valid/informative/non-garbage IFF the second is true;
        static std::pair<MiscInputEvent, bool> fromJson(const Json::Value&);
    };
}
