/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "MenuInputEvent.hpp"
#include <utility>
namespace __GAME_NAMESPACE__
{
	MenuInputEvent::MenuInputEvent(SubType subType) : subType(subType)
	{}

    Json::Value MenuInputEvent::jsonify() const
    {
        Json::Value result;
        result["event_type"] = "MenuInputEvent";
        switch(subType)
        {
        case MenuInputEvent::SubType::Up:
            result["sub_type"] = "Up";
            break;
        case MenuInputEvent::SubType::Down:
            result["sub_type"] = "Down";
            break;
        case MenuInputEvent::SubType::Left:
            result["sub_type"] = "Left";
            break;
        case MenuInputEvent::SubType::Right:
            result["sub_type"] = "Right";
            break;
        case MenuInputEvent::SubType::Activate:
            result["sub_type"] = "Activate";
            break;
        case MenuInputEvent::SubType::Cancel:
            result["sub_type"] = "Cancel";
            break;
        }
        result["display_name"] = toString().toAnsiString();
        return result;
    }

    sf::String MenuInputEvent::toString() const
    {
        switch(subType)
        {
        case MenuInputEvent::SubType::Up:
            return "Menu: Navigate Up";
            break;
        case MenuInputEvent::SubType::Down:
            return "Menu: Navigate Down";
            break;
        case MenuInputEvent::SubType::Left:
            return "Menu: Navigate Left";
            break;
        case MenuInputEvent::SubType::Right:
            return "Menu: Navigate Right";
            break;
        case MenuInputEvent::SubType::Activate:
            return "Menu: Activate";
            break;
        case MenuInputEvent::SubType::Cancel:
            return "Menu: Cancel/Back";
            break;
        default:
            return "Menu: Unknown";
        }
    }

    std::pair<MenuInputEvent, bool> MenuInputEvent::fromJson(const Json::Value& src)
    {
        MenuInputEvent::SubType resultType;
        bool success = true; //assume valid unless/until found invalid.
        if(src["event_type"] == "MenuInputEvent")
        {
            if(!src.isMember("sub_type") || !src["sub_type"].isString())
            {
                success = false;
            }
            else if(src["sub_type"] == "Up")
            {
                resultType = MenuInputEvent::SubType::Up;
            }
            else if(src["sub_type"] == "Down")
            {
                resultType = MenuInputEvent::SubType::Down;
            }
            else if(src["sub_type"] == "Left")
            {
                resultType = MenuInputEvent::SubType::Left;
            }
            else if(src["sub_type"] == "Right")
            {
                resultType = MenuInputEvent::SubType::Right;
            }
            else if(src["sub_type"] == "Activate")
            {
                resultType = MenuInputEvent::SubType::Activate;
            }
            else if(src["sub_type"] == "Cancel")
            {
                resultType = MenuInputEvent::SubType::Cancel;
            }
            else
            {
                success = false;
            }
        }
        else
        {
            success = false;
        }

        return std::make_pair(MenuInputEvent(resultType), success);
    }
}
