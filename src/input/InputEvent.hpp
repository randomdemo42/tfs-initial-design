/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "../definitions.hpp"
#include "../jsoncpp/json/json.h"
#include <SFML/System/String.hpp>
namespace __GAME_NAMESPACE__
{
  class InputEvent
  {
      //creates a Json::Value corresponding the current data
    //   virtual Json::Value jsonify() const = 0;
      //
    //   //gets a human-readable string representing the current data
    //   virtual sf::String toString() const = 0;

      //Note: each derived class should also provide a static std::pair<T, bool> fromJSON(const Json::Value&) method for attempting to parse json objects into their type (used for bindings)
  };
}
