/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "CharacterInputEvent.hpp"
#include <utility>
#include <sstream>
namespace __GAME_NAMESPACE__
{
    CharacterInputEvent::CharacterInputEvent(SubType subType, Axis axis, float position, bool activation): subType(subType), axis(axis), position(position), activation(activation)
    {}

    Json::Value CharacterInputEvent::jsonify() const
    {
        Json::Value result;
        result["event_type"] = "CharacterInputEvent";
        switch(SubType _subType = subType)
        {
            case SubType::Rotation:
                result["sub_type"] = "Rotation";
                break;
            case SubType::Movement:
                result["sub_type"] = "Movement";
                break;
        }

        switch(Axis _axis = axis)
        {
            case Axis::X:
                result["axis"] = "X";
                break;
            case Axis::Y:
                result["axis"] = "Y";
                break;
        }
        result["position"] = position;
        result["display_name"] = toString().toAnsiString();
        return result;
    }

    sf::String CharacterInputEvent::toString() const
    {
        std::stringstream resultBuilder;
        resultBuilder << "Character: ";
        switch(CharacterInputEvent::SubType _subType = subType)
        {
            case CharacterInputEvent::SubType::Rotation:
                resultBuilder << "Face ";
                break;
            case CharacterInputEvent::SubType::Movement:
                resultBuilder << "Move ";
                break;
        }
        resultBuilder << "towards ";
        if(axis == Axis::X)
        {
            resultBuilder << "the ";
            resultBuilder << (position < 0 ? "Left" : "Right");
        }
        else if(axis == Axis::Y)
        {
            resultBuilder << "the ";
            resultBuilder << (position < 0 ? "Top" : "Bottom");
        }

        return resultBuilder.str();
    }

    //note that additionally this method normalises the stored vector to a magnitude of one (although this may be considered a waste efficiency wise and later removed)
    //the position will be normalised, but tyhis shouldn't make much difference in the long run;
    std::pair<CharacterInputEvent, bool> CharacterInputEvent::fromJson(const Json::Value& src)
    {
        SubType subType;
        Axis axis;
        float position;
        bool success = true;
        if(src.isMember("event_type") && src["event_type"].isString() && src["event_type"] == "CharacterInputEvent")
        {
            if(src.isMember("sub_type") && src["sub_type"].isString())
            {
                if(src["sub_type"] == "Rotation")
                {
                    subType = SubType::Rotation;
                }
                else if(src["sub_type"] == "Movement")
                {
                    subType = SubType::Movement;
                }
                else
                {
                    success = false;
                }
                //stop upon first failure
                if(success)
                {
                    if(src.isMember("axis") && src["axis"].isString())
                    {
                        if(src["axis"] == "X")
                        {
                            axis = Axis::X;
                        }
                        else if(src["axis"] == "Y")
                        {
                            axis = Axis::Y;
                        }
                        else
                        {
                            success = false;
                        }
                    }
                }

                if(success)
                {
                    if(src.isMember("position") && src["position"].isNumeric())
                    {
                        position = 1.0f/src["position"].asFloat(); //normalised
                    }
                    else
                    {
                        success = false;
                    }
                }
            }
            else
            {
                success = false;
            }
        }
        else
        {
            success = false;
        }
        return std::make_pair(CharacterInputEvent(subType, axis, position, true), success);
    }
}
