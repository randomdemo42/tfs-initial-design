/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "MiscInputEvent.hpp"
namespace __GAME_NAMESPACE__
{
    MiscInputEvent::MiscInputEvent(SubType subType): subType(subType)
    {}

    Json::Value MiscInputEvent::jsonify() const
    {
        Json::Value result;
        result["input_type"] = "MiscInputEvent";
        switch(SubType _subType = subType)
        {
            case PauseToggle:
                result["sub_type"] = "PauseToggle";
                break;
            case ConsoleToggle:
                result["sub_type"] = "ConsoleToggle";
                break;
        }
        result["display_name"] = toString().toAnsiString();
        return result;
    }

    sf::String MiscInputEvent::toString() const
    {
        sf::String result("Misc: ");
        switch(SubType _subType = subType)
        {
            case PauseToggle:
                result += "Pause Game (Toggle)";
                break;
            case ConsoleToggle:
                result += "Open Console (Toggle)";
                break;
        }
    }

    std::pair<MiscInputEvent, bool> MiscInputEvent::fromJson(const Json::Value& src)
    {
        SubType resultType;
        bool success = true;
        if(!src.isMember("input_type") || !src["input_type"].isString() || src["input_type"] != "MiscInputEvent" || !src.isMember("sub_type") || !src["sub_type"].isString())
        {
            success = false;
        }
        else if(src["sub_type"] == "PauseToggle")
        {
            resultType = SubType::PauseToggle;
        }
        else if(src["sub_type"] == "ConsoleToggle")
        {
            resultType = SubType::ConsoleToggle;
        }
        else
        {
            success = false;
        }
        return std::make_pair(MiscInputEvent(resultType), success);
    }
}
