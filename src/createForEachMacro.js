function createForEachMacro(maxArgsCount)
{
	var resultPartA = "#define FE_CALLITn1(a, b) a b\n";
	var resultPartB = "#define FE_n1(what, x, ...) what(x)\n";
	var resultPartC = "#define FOR_EACH_GET_MACRO(_1,";
	var resultPartD = "#define FOR_EACH(what, ...) FE_CALLITn1(FOR_EACH_GET_MACRO(_0, __VA_ARGS__, ";
	for(var i = 2; i <= (maxArgsCount); i++)
	{
		resultPartA = resultPartA + "#define FE_CALLITn"+i+"(a, b) a b\n";
		resultPartB = resultPartB + "#define FE_n"+i+"(what, x, ...) what(x) FE_CALLITn"+i+"(FE_n"+(i-1)+", (what, __VA_ARGS__))\n";
		resultPartC = resultPartC + "_" + i + ",";
	}
	resultPartC = resultPartC + " Name, ...) Name\n";

	for(var i = maxArgsCount; i >= 0; i--)
	{
		resultPartD = resultPartD + "FE_n"+ i + ", ";
	}

	resultPartD = resultPartD + "), (what, __VA_ARGS__)) \n";

	console.log(resultPartA, "\n\n", resultPartB, "\n\n", resultPartC, "\n\n", resultPartD);
}

#define STRINGIZE(arg)  STRINGIZE1(arg)
#define STRINGIZE1(arg) STRINGIZE2(arg)
#define STRINGIZE2(arg) #arg

#define CONCATENATE(arg1, arg2)   CONCATENATE1(arg1, arg2)
#define CONCATENATE1(arg1, arg2)  CONCATENATE2(arg1, arg2)
#define CONCATENATE2(arg1, arg2)  arg1##arg2






#define EXPAND(x) x



#define FOR_EACH_NARG(...) FOR_EACH_NARG_(__VA_ARGS__, FOR_EACH_RSEQ_N())
#define FOR_EACH_NARG_(...) EXPAND(FOR_EACH_ARG_N(__VA_ARGS__))


#define FOR_EACH_ARG_N(_1, _2, _3, _4, _5, _6, _7, _8, N, ...) N 
#define FOR_EACH_RSEQ_N() 8, 7, 6, 5, 4, 3, 2, 1, 0


#define CONCATENATE(x,y) x##y
#define FOR_EACH_(N, what, x, ...) EXPAND(CONCATENATE(FOR_EACH_, N)(what, x, __VA_ARGS__))
#define FOR_EACH(what, x, ...) FOR_EACH_(FOR_EACH_NARG(x, __VA_ARGS__), what, x, __VA_ARGS__)