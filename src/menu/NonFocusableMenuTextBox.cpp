/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "NonFocusableMenuTextBox.hpp"
namespace __GAME_NAMESPACE__
{
	NonFocusableMenuTextBox::NonFocusableMenuTextBox(const sf::Font& font, const sf::String& string, const sf::Vector2f& position, const sf::Color& color, FontSize fontSize): TextBox(font, string, position, color, fontSize)
    {}

    void NonFocusableMenuTextBox::draw(sf::RenderTarget& renderTarget, sf::RenderStates renderStates) const
    {
        TextBox::draw(renderTarget, renderStates);
    }

    sf::FloatRect NonFocusableMenuTextBox::getBounds() const
    {
        return TextBox::getBounds();
    }

    sf::Vector2f NonFocusableMenuTextBox::getPosition() const
    {
        return TextBox::getPosition();
    }

    void NonFocusableMenuTextBox::update()
    {}

    void NonFocusableMenuTextBox::setPosition(const sf::Vector2f& position)
    {
        TextBox::setPosition(position);
    }
}
