/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
///deprecated for now, 
// #pragma once
// #include "MenuState.hpp"
// //a menu state that comes with a "Back" button, and responds to the "Back" menu-event, (by calling a function supplied to it's constructor.
// //deprecated
// namespace __GAME_NAMESPACE__
// {
//
// 	class BackSteppingMenuState :
// 		public MenuState
// 	{
//     protected:
// 		std::function<void()> backFunc;
// 	public:
// 		BackSteppingMenuState(const sf::Font& defaultFont, const std::function<void()>& backFunc, const sf::Vector2f& menuPosition = sf::Vector2f());
// 		virtual void onEvent(const MenuInputEvent&);
// 		virtual ~BackSteppingMenuState() = 0;
// 	};
//
// }
