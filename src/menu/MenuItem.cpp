/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "MenuItem.hpp"
namespace __GAME_NAMESPACE__
{
    const sf::Color MenuItem::DEFAULT_TEXT_COLOR = sf::Color(255, 255, 255, 255);
    const sf::Color MenuItem::DEFAULT_BG_COLOR = sf::Color(20, 40, 80, 255);
    const sf::Color MenuItem::DEFAULT_FOCUS_COLOR = sf::Color(255, 255, 0, 255);
    const sf::Color MenuItem::DEFAULT_BORDER_COLOR = sf::Color(255, 255, 255, 255);


    const float MenuItem::DEFAULT_HIGHLIGHT_WIDTH = -2.f;
    //negative so that its inside the bounds instead of outside
    const float MenuItem::DEFAULT_BORDER_WIDTH = MenuItem::DEFAULT_HIGHLIGHT_WIDTH;
    const sf::Vector2f MenuItem::DEFAULT_HIGHLIGHT_VECTOR = sf::Vector2f(DEFAULT_HIGHLIGHT_WIDTH, DEFAULT_HIGHLIGHT_WIDTH);
    const sf::Vector2f MenuItem::PADDING = sf::Vector2f(5.f, 5.f);
    const sf::Vector2f MenuItem::INNER_PADDING = sf::Vector2f(8.f, 8.f) - MenuItem::DEFAULT_HIGHLIGHT_VECTOR; //offset against border

	bool MenuItem::focusIfFocusable()
    {
        return false;
    }

    bool MenuItem::blurIfFocusable()
    {
        return false;
	}

    bool MenuItem::isFocusable() const
    {

    }

    void MenuItem::onEvent(const MenuInputEvent&)
    {
        //default, do nothing.
    }

    void MenuItem::onEvent(const MouseEvent&)
    {
        //default, do nothing.
    }

}
