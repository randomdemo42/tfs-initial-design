/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "MenuState.hpp"
#include "../input/InputHandler.hpp"
#include "../jsoncpp/json/json.h"

namespace __GAME_NAMESPACE__
{
    //this possibly should be called the bindings state instead of the settings state but it's our only settings so far?
    //: public BackSteppingMenuState
    class SettingsMenuState: public MenuState
    {
        //temp bindings store here??
        //figure out how to do the setting of them?
    private:
        class BindingInputKeyRequest : public InputKeyRequest
        {
        private:
            std::function<void(const InputKey&)> callback;
            // bool hasInputKeyRequestActive();
        public:
            //construct it with a callback that is triggered when it is fulfilled
            BindingInputKeyRequest(const std::function<void(const InputKey&)>&);

            void fulfill(const InputKey&);
        };
        std::function<void()> backFunc;
        InputHandler& inputHandler;
        Json::Value previousBindings; //the bindings from before the last save?


        std::map<sf::String, std::vector<Json::Value>> tentativeBindingGroups; //only need to split the tentatives, for viewing purposes, the old one can just get reloaded and all
        // Json::Value tentativeSFMLSpecificSettings;
        Json::Value tentativeBindings;
        std::function<void()> currentRequestCancelFunc; //update it when issueing a new request, void when there is no request

        //if not at main-settings menu, just skip back to it. (we could/should use a stack of backfuncs instead though in future?)
        bool isAtMainMenu;

        //revert the the tentative bindings to the previousBindings;
        void revertTentativeBindings();

        // //call this to split the list of bindings into groups to use in submenus
        // void groupBindings();
        //the central menu for the settings,.
        void loadMainSettingsMenu();
        void loadBindingSubMenu(std::string EventType);

        //updates tentative bindings to match the current grouped bindings, and the current SFML specifics
        void updateTentativeBindings();


        //brings them into effect but doesn't save them (once the menu is left they will revert)
        void applyTentativeBindings();

        void saveTentativeBindings();
    public:
        SettingsMenuState(const sf::Font& defaultFont, const std::function<void()>& backFunc, InputHandler&);
        virtual void onEvent(const MenuInputEvent&);
    };
}
