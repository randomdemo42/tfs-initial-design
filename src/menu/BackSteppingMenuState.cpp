/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
//deprecated for now?
// #include "BackSteppingMenuState.hpp"
// #include "SimpleTextButton.hpp"
// namespace __GAME_NAMESPACE__
// {
// 	BackSteppingMenuState::BackSteppingMenuState(const sf::Font& defaultFont, const std::function<void()>& backFunc, const sf::Vector2f& menuPosition)
// 		: MenuState(defaultFont, menuPosition), backFunc(backFunc)
// 	{
// 		menu.addItem<SimpleTextButton>(true, defaultFont, "Back", [this](SimpleTextButton*){this->backFunc();});
// 	}
//
// 	void BackSteppingMenuState::onEvent(const MenuInputEvent& event)
// 	{
// 		if (event.subType == MenuInputEvent::Cancel)
// 		{
// 			backFunc();
// 		}
// 		else
// 		{
// 			MenuState::onEvent(event);
// 		}
// 	}
//
// 	BackSteppingMenuState::~BackSteppingMenuState()
// 	{
// 	}
// }
