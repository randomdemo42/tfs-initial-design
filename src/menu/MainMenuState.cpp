/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "MainMenuState.hpp"
#include "SimpleTextButton.hpp"
#include "ActivationCountingButton.hpp"
#include "SettingsMenuState.hpp"
#include "NonFocusableMenuTextBox.hpp"
#include "../state/DemoEngineState.hpp"
#include "../state/HomeState.hpp"
//#include "ControlsMenuState.hpp"
namespace __GAME_NAMESPACE__
{
	MainMenuState::MainMenuState(const sf::Font& defaultFont, PrimaryStateManager& primaryManager, StateMachine<MenuState>& menuMachine, InputHandler& inputHandler) : MenuState(defaultFont, sf::Vector2f(20, 20)), primaryManager(primaryManager), menuMachine(menuMachine), quitFunc([&primaryManager]{ primaryManager.quit(); }), inputHandler(inputHandler)
	{
        menu.addItem<NonFocusableMenuTextBox>(true, defaultFont,  "Main Menu").setFontSize(25);
		menu.addItem<ActivationCountingButton>(true, defaultFont);
        menu.addItem<SimpleTextButton>(true, defaultFont, "Controls Settings",
            [&](SimpleTextButton*)
            {
                menuMachine.changeState<SettingsMenuState>(defaultFont,
        			[&]
                    {
        				menuMachine.changeState<MainMenuState>(defaultFont, primaryManager, menuMachine, inputHandler);
        			},
                    inputHandler
                );
            }
        );

        menu.addItem<SimpleTextButton>(true, defaultFont, "Engine Testing",
            [&](SimpleTextButton*)
            {
                primaryManager.changeState<DemoEngineState>(defaultFont,
        			[&]
                    {
        				primaryManager.changeState<HomeState>(defaultFont, primaryManager, inputHandler);
        			},
                    inputHandler
                );
            }
        );

		menu.addItem<SimpleTextButton>(true, defaultFont, "Quit to Desktop", [&](SimpleTextButton*){quitFunc();});
	}


	void MainMenuState::update()
	{
	}

	void MainMenuState::onEvent(const MenuInputEvent& event)
	{
		MenuState::onEvent(event);
	}


	void MainMenuState::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		MenuState::draw(target, states);
	}
}
