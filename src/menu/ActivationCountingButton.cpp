/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ActivationCountingButton.hpp"
#include <sstream>
namespace __GAME_NAMESPACE__
{
	//should probably hide methods such as set string in this class but it's only a test class


	const sf::String ActivationCountingButton::baseText = "Times Activated: ";
	ActivationCountingButton::ActivationCountingButton(const sf::Font& font, const sf::Vector2f& position) : SimpleTextButton(font, baseText + "0", position), timesActivated(0)
	{
		SimpleTextButton::setActivateFunc([&](SimpleTextButton*){onActivation(); });
	}

	void ActivationCountingButton::onActivation()
	{
		std::stringstream stream;
		timesActivated++;
		stream << timesActivated;
		setString(baseText + stream.str());
	}
}
