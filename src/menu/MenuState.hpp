/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "../state/State.hpp"
#include "Menu.hpp"
#include <SFML/Graphics/RenderWindow.hpp>
#include <functional>
namespace __GAME_NAMESPACE__
{

	//NOTE: upon destruction this class does NOT automatically destruct & delete memory used by objects in it's menu, but it provides a method which can be used to automate this process for all items in the menu.
	class MenuState : public State
	{

	protected:

		const sf::Font& defaultFont;
		Menu menu;

		virtual void draw(sf::RenderTarget&, sf::RenderStates) const;
	public:
		// virtual void onEvent(const sf::Event&);

		/*
			events already processed and specialised as menu events.
			it's also possible to capture other events if desired, I suppose this would mostly be for sub-menus?
			TODO: assess at some stage whether or not this is actually useful.
		*/
		virtual void onEvent(const MenuInputEvent&);
		virtual void onEvent(const MouseEvent&);

		// //THIS SHOULDN'T BE NECCESSARY AS THE DO-NOTHING implimentation SHOULD COME FROM STATE BUT IT WON'T SO WHATEVER
		// virtual void onEvent(const CharacterInputEvent&);

		virtual void update();
		MenuState(const sf::Font& defaultFont, const sf::Vector2f& menuPosition = sf::Vector2f());

		virtual ~MenuState() = 0;
	};
}
