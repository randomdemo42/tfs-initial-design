/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "SimpleTextButton.hpp"
namespace __GAME_NAMESPACE__
{

	//test class only, doesn't hide methods it should (e.g. SimpleTextButton::SetString()) to make it usefull in a real State, also what it actually does is kinda silly/useless anyway.
	class ActivationCountingButton: public SimpleTextButton
	{
		static const sf::String baseText;
		int timesActivated;
	protected:
		void onActivation();
	public:
		ActivationCountingButton(const sf::Font&, const sf::Vector2f& position = sf::Vector2f(0,0));
		virtual ~ActivationCountingButton() = default;

	};
}
