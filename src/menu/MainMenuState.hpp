/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "MenuState.hpp"
#include "Menu.hpp"
#include "../state/PrimaryStateManager.hpp"
#include <SFML/Graphics/RenderWindow.hpp>
#include "../input/InputHandler.hpp"
namespace __GAME_NAMESPACE__
{
	class MainMenuState :
		public MenuState
	{
		PrimaryStateManager& primaryManager;
		StateMachine<MenuState>& menuMachine;
        InputHandler& inputHandler;
		std::function<void()> quitFunc;
	protected:
		virtual void draw(sf::RenderTarget&, sf::RenderStates) const;
	public:
		MainMenuState(const sf::Font& defaultFont, PrimaryStateManager& primaryManager, StateMachine<MenuState>& menuMachine, InputHandler& inputHandler);
		virtual void update();
		virtual void onEvent(const MenuInputEvent&);
		virtual ~MainMenuState() = default;
	};

}
