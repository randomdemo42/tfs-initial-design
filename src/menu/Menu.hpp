/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "../definitions.hpp"
#include "../gui/GUIObject.hpp"
#include <SFML/System/String.hpp>
#include <list>
#include "../input/MenuInputEvent.hpp"
#include "MenuItem.hpp"
#include <SFML/Graphics/Text.hpp>
#include "../gui/TextBox.hpp"
#include <utility>
#include <SFML/Graphics/Rect.hpp>
#include <memory>
namespace __GAME_NAMESPACE__
{

    //TODO: consider moving the menu title out of the menu class and make it a seperate unfocusable-text-box that needs to be manually added to menus as a normal item?

	//note that this class uses std::unique_ptr, and as such should only be given pointers to heap-allocated objects, which should not be managed elsewhere
	//class that and organises menu items given to it both in terms of behaviour and ensuring visibility/that items don't overlap, etc
	class Menu :
		public GUIObject, public EventListener<MenuInputEvent>, public  EventListener<MouseEvent>
	{

		//NOTE: one  restriction of this class is that we need to continously track which item in the container is being "focused" on, and have the items ordered in terms of that focu.
		//The current solution for this uses iterators and as such requires that updating the container (insert/remove) never invalidates the iterators, as such the ItemsContainer type must, currently be a list.
		//a potentionally more efficient solution may be to use vectors and size-type indexes instead of iterators, as we can ensure that accuracy is maintained whenever updates occur, but this is an optimisation that can occur later.


		typedef std::vector< std::vector< std::unique_ptr<MenuItem> > > ItemsContainer; //must be a list such that modifcations don't invalidate iterators other than the removed item if one is removed
	public:
		static const sf::Vector2f PADDING;
		typedef ItemsContainer::iterator iterator;
		typedef ItemsContainer::reverse_iterator reverse_iterator;
		typedef ItemsContainer::const_iterator const_iterator;
		typedef ItemsContainer::const_reverse_iterator const_reverse_iterator;
	private:
		sf::Vector2f position;
		ItemsContainer items;
		int focusedRow, focusedColumn;
        int itemCount; //more efficient to track it then recalculate, same probably goes for buonds but that can wait?
	protected:
		virtual void draw(sf::RenderTarget&, sf::RenderStates) const;
		//note that menu doesn't (currently) require an update geometry and if it did it would probably have to be public since it's contents can be changed externally.
	public:
		Menu(const sf::Vector2f& position = sf::Vector2f(0,0));

		//preventing this from being copiable unless/until there seems to be a good reason to do it, accidental copying could cause a dangling pointer issue after all
		Menu(const Menu&) = delete;
		Menu& operator=(Menu) = delete;
		Menu& operator=(Menu&&) = delete;

        bool hasFocusedItem();

        //pre-condition something is focused (hasFocusedItem())
		// //post-condition: a reference to the currently focused item is returned
		// //note that once added, the menu will trigger/control positioning, events, etc of the item, until it is removed.
        MenuItem& getFocusedItem();
		const MenuItem& getFocusedItem() const;

        //sets the focusedItem to the item in the row, column specified, if something exists there uinless nothing changes
		bool setFocusPosition(int, int);

		// //bool incrementFocus();
		// //bool decrementFocus();
		// iterator begin();
		// const_iterator begin() const;
		// iterator end();
		// const_iterator end() const;
        //
		// reverse_iterator rbegin();
		// const_reverse_iterator rbegin() const;
		// reverse_iterator rend();
		// const_reverse_iterator rend() const;

		virtual void onEvent(const MenuInputEvent&);
		virtual void onEvent(const MouseEvent&);

		//pre-condition: the menu is not empty
		//post-condition: the item at the end of the menu will be removed.
		//note that it does not deallocate the memory associated with the item and that must be done elsewhere
		void deleteLastItem();

		//removes all items from the menu.
		//note that it does not deallocate the memory associated with the item and that must be done elsewhere
		void clearItems();
		virtual sf::FloatRect getBounds() const;

		virtual sf::Vector2f getPosition() const;
		virtual ~Menu();

		virtual void update();

        //determine the number of items in the menu currently
        int size() const;

		//TODO: add scrollbar type deal maybe?

		//adds a new text button to the bottom of the menu, using the supplied type and args
		template<class TargetItemType, typename... Args>
		typename std::enable_if< std::is_base_of<MenuItem, TargetItemType>::value, TargetItemType&>::type
		addItem(bool shouldCreateNewRow, Args&&...); //by default create a new row for each  item, only add to the row on explicit request? - partially this provides legacy support for the original format


        //updates geometry as neccesary to adjust to changes that may have occured internally. (checks for said changes)
		void updateGeometry();
	};


	//definition for addItem.
	template<class TargetItemType, typename... Args>
	typename std::enable_if< std::is_base_of<MenuItem, TargetItemType>::value, TargetItemType& >::type
	Menu::addItem(bool shouldCreateNewRow, Args&&... args)
	{
        TargetItemType* tempPointer = new TargetItemType(std::forward<Args>(args)...); //stored temporarily so that a reference to the actual produced object can be returned
		std::unique_ptr<MenuItem> itemPtr(tempPointer);
		// itemPtr.reset();
		sf::Vector2f nextPosition = position, tempPosition;
        sf::FloatRect tempBounds;

		//titleText will always exist, use that if we have no actual menu items, else use the last menu item
		if(!items.empty())
        {
            for(auto it = items.back().begin(); it != items.back().end(); ++it)
            {
                tempBounds = (*it)->getBounds();
                if(shouldCreateNewRow)
                {
                    tempPosition.y = tempBounds.top + tempBounds.height;
                    if (tempPosition.y > nextPosition.y)
        			{
        				nextPosition.y = tempPosition.y;
        			}
                }
                else
                {
                    tempPosition.x = tempBounds.left + tempBounds.width;
                    if (tempPosition.x > nextPosition.x)
                    {
                        nextPosition.x = tempPosition.x;
                    }
                }
            }
        }

        if(shouldCreateNewRow)
        {
            nextPosition += PADDING;
        }
        else
        {
            nextPosition.y = tempBounds.top;
            nextPosition.x += PADDING.x;
        }

		(itemPtr)->setPosition(nextPosition);

        if(items.empty() || shouldCreateNewRow)
        {
            items.emplace_back();
        }
        items.back().push_back(std::move(itemPtr));

		// if (items.size() == 1 || (focusedRow == -1 && focusedColumn == -1))
		// {
		// 	setFocusPosition(items.size()-1, items.back().size()-1);
		// }
        ++itemCount;
        return *tempPointer;
	}
}
