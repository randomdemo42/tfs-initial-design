/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "MenuState.hpp"
namespace __GAME_NAMESPACE__
{
	MenuState::MenuState(const sf::Font& defaultFont, const sf::Vector2f& menuPosition) : defaultFont(defaultFont), menu(menuPosition)
	{
	}

	void MenuState::onEvent(const MenuInputEvent& event)
	{
		menu.onEvent(event);
	}

	void MenuState::onEvent(const MouseEvent& event)
	{
		menu.onEvent(event);
	}

	// void MenuState::onEvent(const CharacterInputEvent& event)
	// {}

	void MenuState::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		target.draw(menu, states);
	}

	void MenuState::update()
	{
		menu.update();
	}

	MenuState::~MenuState()
	{
	}
}
