/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Menu.hpp"
#include "SimpleTextButton.hpp"
#include "../definitions.hpp"
#include <iostream>
#include <algorithm>

namespace __GAME_NAMESPACE__
{
	const sf::Vector2f Menu::PADDING = sf::Vector2f(10, 10);
	Menu::Menu(const sf::Vector2f& position) : position(position), items(), focusedRow(-1), focusedColumn(-1), itemCount(0)
	{}

    bool Menu::hasFocusedItem()
    {
        return focusedRow  != -1 && focusedColumn != -1;
    }

	MenuItem& Menu::getFocusedItem()
	{
		return (*items[focusedRow][focusedColumn]);
	}

	const MenuItem& Menu::getFocusedItem() const
	{
		return (*items[focusedRow][focusedColumn]);
	}

	//only triggers an event if the focused was not already at the given position and the supplied iterator was focusable (otherwise)
    //returns whether or not the focusing occured
	bool Menu::setFocusPosition(int row, int column)
	{
		if (row < items.size() && column < items[row].size() && !(focusedRow == row && focusedColumn == column))
		{
            if(items[row][column]->focusIfFocusable())
			{
                if (hasFocusedItem())
    			{
    				items[focusedRow][focusedColumn]->blurIfFocusable();
    			}
    			focusedRow = row;
                focusedColumn = column;
                return true;
            }
		}
        return false;
	}

	// Menu::iterator Menu::begin()
	// {
	// 	return items.begin();
	// }
    //
	// Menu::const_iterator Menu::begin() const
	// {
	// 	return items.begin();
	// }
    //
	// Menu::iterator Menu::end()
	// {
	// 	return items.end();
	// }
    //
	// Menu::const_iterator Menu::end() const
	// {
	// 	return items.end();
	// }
    //
	// Menu::reverse_iterator Menu::rbegin()
	// {
	// 	return items.rbegin();
	// }
    //
	// Menu::const_reverse_iterator Menu::rbegin() const
	// {
	// 	return items.rbegin();
	// }
    //
	// Menu::reverse_iterator Menu::rend()
	// {
	// 	return items.rend();
	// }
    //
	// Menu::const_reverse_iterator Menu::rend() const
	// {
	// 	return items.rend();
	// }

	//NOTE: as it a template, addItem is defined in the header not here.

	void Menu::deleteLastItem()
	{
		//several steps, first if this the focusediterator, change it the to end, i.e. there is no focus
		if (focusedRow == items.size()-1 && focusedColumn == items.back().size()-1)
		{
			items.back().back()->blurIfFocusable();
			focusedRow = -1;
            focusedColumn = -1;
		}
        items.back().pop_back();
        if(items.back().empty())
	    {
            items.pop_back();
        }
        --itemCount;
	}
	void Menu::clearItems()
	{
        items.clear();
        focusedRow = -1;
        focusedColumn = -1;
        --itemCount;
	}

	void Menu::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
        for (auto rowIt = items.begin(); rowIt != items.end(); ++rowIt)
        {
            for(auto columnIt = (*rowIt).begin(); columnIt != (*rowIt).end(); ++columnIt)
            {
                target.draw((**columnIt), states);
            }
        }

#if RENDER_DEBUGGING
		GUIObject::drawDebug(target, states);
#endif
	}

	sf::FloatRect Menu::getBounds() const
	{
		//determine the precise top left corner and bottom right corner of the menu contents
		sf::Vector2f tempBottomRight;
		sf::FloatRect tempBounds = sf::FloatRect(position.x, position.y, 0,0);
		sf::Vector2f bottomRight;

		if (items.size() > 0)
		{
			//check the right-edge of all elements, but only
			for (auto rowIt = items.begin(); rowIt != items.end(); ++rowIt)
			{
                for(auto columnIt = (*rowIt).begin(); columnIt != (*rowIt).end(); ++columnIt)
				{
                    tempBounds = (*columnIt)->getBounds();
    				tempBottomRight.x = tempBounds.left + tempBounds.width;
    				if (tempBottomRight.x > bottomRight.x)
    				{
    					bottomRight.x = tempBottomRight.x;
    				}
                }
			}

			tempBottomRight.y = tempBounds.top + tempBounds.height;
			if (tempBottomRight.y > bottomRight.y)
			{
				bottomRight.y = tempBottomRight.y;
			}
		}
		//factor the padding into the return value;
		return sf::FloatRect(position, (bottomRight-position)+PADDING*2.0f);
	}

	sf::Vector2f Menu::getPosition() const
	{
		//using the internal innards.getPosition doesn't seem to match up with the bounds
		return position;
	}

	void Menu::onEvent(const MouseEvent& event)
	{
        sf::Vector2f mousePosition;
        sf::FloatRect eachBounds;
		switch(MouseEvent::SubType subType = event.subType)
		{
			case MouseEvent::Move:
                mousePosition = sf::Vector2f(event.move.x, event.move.y);
				break;
			case MouseEvent::ButtonPressed: //continue to next case without breaking.
			case MouseEvent::ButtonReleased:
				mousePosition = sf::Vector2f(event.button.x, event.button.y);
				break;
		}
        for (int i = 0;  i < items.size(); ++i)
        {
            eachBounds = items[i].front()->getBounds();
            //stop looking once we find the first row that is definitely past the location
            if(eachBounds.top > mousePosition.y)
            {
                break;
            }
            else
            {
                for(int j = 0; j < items[i].size(); ++j)
                {
                    eachBounds = items[i][j]->getBounds();
                    // std::cout << "test1" << std::endl;
                    if (eachBounds.contains(mousePosition.x, mousePosition.y))
                    {
                        //note: 2 events are sent to the target menu-item seperately: Focus (if not already focused), and MouseMove.
                        setFocusPosition(i, j); //we don't need feedback on whether or not the focus was successful
                        break; //stop looking, there can only be 1 match.
                    }
                    else if(eachBounds.left + eachBounds.width > mousePosition.x) //stop looking once we find a column that is definitely past the location. note that being past a single item vertically does not nexccessarily mean being past the entire row vertically so don't break the outer loop
                    {
                        break;
                    }
                }
            }
        }
        if (hasFocusedItem() && getFocusedItem().getBounds().contains(mousePosition.x, mousePosition.y))
        {
            if(event.subType == MouseEvent::SubType::ButtonPressed && event.button.button == sf::Mouse::Button::Left)
            {
                getFocusedItem().onEvent(MenuInputEvent(MenuInputEvent::SubType::Activate));
            }
            else
            {
                getFocusedItem().onEvent(event);
            }
        }
	}

	void Menu::onEvent(const MenuInputEvent& event)
	{
        int initialFocusedRow = focusedRow, initialFocusedColumn = focusedColumn; //points to the iterator/position that was focused before the navigation event started, used to help cycle through to find the next item to focus (to prevent infinite looping? when reaching the end)
        int tentativeRow = initialFocusedRow, tentativeColumn = initialFocusedColumn;
        int columnAdjustmentMin, columnAdjustmentMax;
        bool success = false;

		switch (MenuInputEvent::SubType subType = event.subType)
		{
		case MenuInputEvent::Up:
            if(hasFocusedItem())
            {
                if (items.size() > 1)
    			{
                    do
                    {
                        if (tentativeRow == -1|| tentativeRow == 0)
                        {
                            tentativeRow = items.size()-1;
                        }
                        else
                        {
                            --tentativeRow;
                        }
                        if(tentativeRow == initialFocusedRow)
                        {
                            break;
                        }
                        else
                        {
                            success = setFocusPosition(tentativeRow, tentativeColumn);
                            if(!success) //try the current item if not try the next closest, left, then right, expanding one on either side
                            {
                                columnAdjustmentMin = std::min((long unsigned int)tentativeColumn-1, items[tentativeRow].size()-1);
                                columnAdjustmentMax = tentativeColumn+1;
                                for(bool turnForMin = columnAdjustmentMin >= 0; !success && (columnAdjustmentMin >= 0 || columnAdjustmentMax < items[tentativeRow].size());) //if turnForMin check against the min and shift it if it fails, else do the max but for max
                                {
                                    if(turnForMin)
                                    {
                                        success = setFocusPosition(tentativeRow, columnAdjustmentMin);
                                        --columnAdjustmentMin;
                                        if(columnAdjustmentMax < items[tentativeRow].size())
                                        {
                                            turnForMin = false;
                                        }
                                    }
                                    else
                                    {
                                        success = setFocusPosition(tentativeRow, columnAdjustmentMax);
                                        ++columnAdjustmentMax;
                                        if(columnAdjustmentMin >= 0)
                                        {
                                            turnForMin = false;
                                        }
                                    }
                                }
                            }
                        }
                    } while(!success);
                }
                else
                {
                    getFocusedItem().onEvent(event); //pass the event if there's only the one row
                }
        	}
			else
			{
                //focus the first item focusable bottom-up, left-to-rght
                for(int i = items.size()-1; !success && i >= 0; ++i)
                {
                    for(int j = 0; !success && j < items[i].size(); ++j)
                    {
                        success = setFocusPosition(i, j);
                    }
                }
			}
			break;
		case MenuInputEvent::Down:
            if(hasFocusedItem())
            {
                if (items.size() > 1)
    			{
                    do
                    {
                        if (tentativeRow == -1|| tentativeRow == items.size()-1)
                        {
                            tentativeRow = 0;
                        }
                        else
                        {
                            ++tentativeRow;
                        }
                        if(tentativeRow == initialFocusedRow)
                        {
                            break;
                        }
                        else
                        {
                            success = setFocusPosition(tentativeRow, tentativeColumn);
                            if(!success) //try the current item if not try the next closest, left, then right, expanding one on either side
                            {
                                columnAdjustmentMin = std::min((long unsigned int)tentativeColumn-1, items[tentativeRow].size()-1);
                                columnAdjustmentMax = tentativeColumn+1;
                                for(bool turnForMin = columnAdjustmentMin >= 0; !success && (columnAdjustmentMin >= 0 || columnAdjustmentMax < items[tentativeRow].size());) //if turnForMin check against the min and shift it if it fails, else do the max but for max
                                {
                                    if(turnForMin)
                                    {
                                        success = setFocusPosition(tentativeRow, columnAdjustmentMin);
                                        --columnAdjustmentMin;
                                        if(columnAdjustmentMax < items[tentativeRow].size())
                                        {
                                            turnForMin = false;
                                        }
                                    }
                                    else
                                    {
                                        success = setFocusPosition(tentativeRow, columnAdjustmentMax);
                                        ++columnAdjustmentMax;
                                        if(columnAdjustmentMin >= 0)
                                        {
                                            turnForMin = false;
                                        }
                                    }
                                }
                            }
                        }
                    } while(!success);
                }
                else
                {
                    getFocusedItem().onEvent(event); //pass the event if there's only the one row
                }
        	}
			else
			{
                //focus the first item focusable top-down, left-to-rght
                for(int i = 0; !success && i < items.size(); ++i)
                {
                    for(int j = 0; !success && j < items[i].size(); ++j)
                    {
                        success = setFocusPosition(i, j);
                    }
                }
			}
			break;
        case MenuInputEvent::Left:
            if(hasFocusedItem())
            {
                if (items[tentativeRow].size() > 1)
    			{
                    do
                    {
                        //tentativeColumn == -1|| //can't be -1 if there is a focused item
                        if (tentativeColumn == 0)
                        {
                            tentativeColumn = items.size()-1;
                        }
                        else
                        {
                            --tentativeColumn;
                        }
                    } while(tentativeColumn != initialFocusedColumn && !setFocusPosition(tentativeRow, tentativeColumn));
            	}
    			else if (items[tentativeRow].size() == 1)
    			{
    				getFocusedItem().onEvent(event); //pass the event on if there's only the one in the row
    			}
            }
            else
            {
                //focus the first item focusable top-down, right-to-left
                for(int i = 0; !success && i < items.size(); ++i)
                {
                    for(int j =  items[i].size()-1; !success && j >= 0; --j)
                    {
                        success = setFocusPosition(i, j);
                    }
                }
            }
			break;
		case MenuInputEvent::Right:
            if(hasFocusedItem())
            {
                if (items[tentativeRow].size() > 1)
    			{
                    do
                    {
                        //tentativeColumn == -1|| //can't be -1 if there is a focused item
                        if (tentativeColumn == items.size()-1)
                        {
                            tentativeColumn = 0;
                        }
                        else
                        {
                            ++tentativeColumn;
                        }
                    } while(tentativeColumn != initialFocusedColumn && !setFocusPosition(tentativeRow, tentativeColumn));
            	}
    			else if (items[tentativeRow].size() == 1)
    			{
    				getFocusedItem().onEvent(event); //pass the event on if there's only the one in the row
    			}
            }
            else
            {
                //focus the first item focusable top-down, left-to-rght
                for(int i = 0; !success && i < items.size(); ++i)
                {
                    for(int j = 0; !success && j < items[i].size(); ++j)
                    {
                        success = setFocusPosition(i, j);
                    }
                }
            }
			break;
		default:
            if(hasFocusedItem())
            {
                getFocusedItem().onEvent(event); //pass the event if there's only the one row
            }
			break;
		}
	}

	void Menu::update()
	{
        for (auto rowIt = items.begin(); rowIt != items.end(); ++rowIt)
        {
            for(auto columnIt = (*rowIt).begin(); columnIt != (*rowIt).end(); ++columnIt)
            {
                (*columnIt)->update();
            }
        }
	}

	Menu::~Menu()
	{
	//	clearItems();
	}

    void Menu::updateGeometry()
    {
        sf::FloatRect eachBounds;
        sf::Vector2f eachBottomRight, currentOuterPosition = position; //used to set the next position where the next menu item goes
        int lowestY;
        for (auto rowIt = items.begin(); rowIt != items.end(); ++rowIt)
        {
            for(auto columnIt = (*rowIt).begin(); columnIt != (*rowIt).end(); ++columnIt)
            {
                (*columnIt)->setPosition(currentOuterPosition + PADDING);
                eachBounds = (*columnIt)->getBounds();
                eachBottomRight.x = eachBounds.left + eachBounds.width;
                eachBottomRight.y = eachBounds.top + eachBounds.height;
                if(eachBottomRight.x > currentOuterPosition.x)
                {
                    currentOuterPosition. x = eachBottomRight.x;
                }
                if(eachBottomRight.y > lowestY)
                {
                    lowestY = eachBottomRight.y;
                }
            }
            currentOuterPosition.x = position.x;
            currentOuterPosition.y = lowestY;
        }
    }

    int Menu::size() const
    {
        return itemCount;
    }
}
