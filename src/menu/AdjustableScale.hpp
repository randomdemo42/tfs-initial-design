/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
//class for a menu interface for a adjusting a scalable value (takes an onupdate function instead of onActivate)
#pragma once
#include "MenuItem.hpp"
#include "../gui/TextBox.hpp"
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/ConvexShape.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <functional>
namespace __GAME_NAMESPACE__
{
    class AdjustableScale: public MenuItem
    {
    public:
        //dictates the lower and upper bounds, and how much to step at a time - lower must be <= upper of course
        class ValueAdjustmentModifiers
        {
        public:
            float min, max, step;
            ValueAdjustmentModifiers(float,float,float);
        };
    private:
        ValueAdjustmentModifiers adjustments;
        float value;
        bool leftHighlighted, rightHighlighted; //dictates whether or not the left and right arrows are highlighted (used with mouse positioning)
        TextBox valueText;
        // sf::RectangleShape valueBoxBackground;
        // sf::RectangleShape valueBar;
        sf::ConvexShape leftArrow, rightArrow;
        std::function<void(AdjustableScale*)> onChangeFunc;

        void updateValueText();

        void updateValue(float newValue);
    protected:
        virtual void draw(sf::RenderTarget&, sf::RenderStates) const;
    public:

        //NOTE: for both constructors that the value (for now is expected to range from 0.0 to 1.0, values outside of this range may cause broken behaviour) - though this will probably change so take upper, lower & step values
        AdjustableScale(const sf::Font&, const ValueAdjustmentModifiers&, float initialValue, const std::function<void(AdjustableScale*)>& onChangeFunc = nullptr, const sf::Vector2f& position = sf::Vector2f(0, 0));

        virtual void update();
        virtual sf::Vector2f getPosition() const;
        virtual void setPosition(const sf::Vector2f&);

        //returns true if the item is focusable, and the item will react to having focus (if reaction is neccessary as dictated by the item). otherwise (default), false will be returned
        virtual bool focusIfFocusable();

        //returns true if the item is focusable, and the item will react to losing focus (if reaction is neccessary as dictated by the item). otherwise (default), false will be returned
        virtual bool blurIfFocusable();

        virtual bool isFocusable() const;

        virtual void onEvent(const MenuInputEvent&);
        virtual void onEvent(const MouseEvent&);

        void setOnChangeFunc(const std::function<void(AdjustableScale*)>&);
        const std::function<void(AdjustableScale*)>& getOnChangeFunc();

        float getValue() const;

        //note that changing it this way doesn't trigger the onChangeFunc.
        //setting values out of range are rounded to go back into range
        void setValue(float);

        sf::FloatRect getBounds() const;
    };
}
