/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "SettingsMenuState.hpp"
#include "NonFocusableMenuTextBox.hpp"
#include "SimpleTextButton.hpp"
#include "AdjustableScale.hpp"
#include <fstream>
namespace __GAME_NAMESPACE__
{

    //TODO: would probably be better to potentially allow the bindings file to dictate the menu structure for bindings - though we'll still need something different for the deadzones so how should we manage that?

    SettingsMenuState::BindingInputKeyRequest::BindingInputKeyRequest(const std::function<void(const InputKey&)>& callback): callback(callback)
    {}

    void SettingsMenuState::BindingInputKeyRequest::fulfill(const InputKey& key)
    {
        callback(key);
    }

    //I'm guessing I'll need to use reference-wrappersor something or at least references since copying is deep-copy by default?
    SettingsMenuState::SettingsMenuState(const sf::Font& defaultFont, const std::function<void()>& backFunc, InputHandler& inputHandler):
        //add to the backfunc behaviour to restore the last saved bindings
        MenuState(defaultFont), inputHandler(inputHandler), currentRequestCancelFunc(nullptr), backFunc(backFunc), isAtMainMenu(false)
    {
        //load the active bindings up, and add a menu item for each of them
        std::ifstream bindingsFileStream(__ACTIVE_BINDING_FILE_PATH__);
        Json::Reader reader;
        try
        {
            reader.parse(bindingsFileStream, previousBindings, false);
            tentativeBindings = previousBindings;
        }
        catch(Json::RuntimeError err)
        {
            std::cout << err.what();
        }

        inputHandler.setBindings(tentativeBindings);
        previousBindings = tentativeBindings;

        loadMainSettingsMenu();

}

    void SettingsMenuState::onEvent(const MenuInputEvent& event)
    {
        if(currentRequestCancelFunc == nullptr)
        {
    		if (event.subType == MenuInputEvent::Cancel)
    		{
                if(isAtMainMenu)
                {
                    backFunc();
                }
                else
                {
                    loadMainSettingsMenu();
                }
    		}
    		else
    		{
    			MenuState::onEvent(event);
    		}
        }
        else if(event.subType == MenuInputEvent::SubType::Cancel && currentRequestCancelFunc != nullptr)
        {
            currentRequestCancelFunc();
        }
    }

    // void SettingsMenuState::groupBindings()
    // {
    //     tentativeBindingGroups.clear();
    //     for(auto it = tentativeBindings["bindings"].begin(); it != tentativeBindings["bindings"].end(); ++it)
    //     {
    //         tentativeBindingGroups[(*it)["target"]["event_type"]].push_back(*it);
    //     }
    // }

    void SettingsMenuState::loadMainSettingsMenu()
    {
        revertTentativeBindings(); //redundant if the the settings weren't changed, less efficient but kind of easier since it's standardised behaviour this way?
        menu.clearItems();
        menu.addItem<NonFocusableMenuTextBox>(true,defaultFont, "Control Settings").setFontSize(25);
        menu.addItem<SimpleTextButton>(true, defaultFont, "Back to Main Menu", [&](SimpleTextButton*){this->backFunc();});
        menu.addItem<SimpleTextButton>(true, defaultFont, "Menu Controls", [&](SimpleTextButton*){this->loadBindingSubMenu("MenuInputEvent");});
        menu.addItem<SimpleTextButton>(true, defaultFont, "Character Controls", [&](SimpleTextButton*){this->loadBindingSubMenu("CharacterInputEvent");});
        menu.addItem<SimpleTextButton>(true, defaultFont, "Gamepad & Misc Settings", [&](SimpleTextButton*){this->loadBindingSubMenu("MiscInputEvent");});
        menu.addItem<SimpleTextButton>(true, defaultFont, "Restore defaults",
        [&](SimpleTextButton* activatedButton)
            {
                revertTentativeBindings();
            }
        );
        isAtMainMenu = true;
    }

    void SettingsMenuState::updateTentativeBindings()
    {
        tentativeBindings["bindings"] = Json::Value(); //clear out the old version.
        for(auto it = tentativeBindingGroups.begin(); it != tentativeBindingGroups.end(); ++it)
        {
            for(auto subIt = (*it).second.begin(); subIt != (*it).second.end(); ++subIt)
            {
                tentativeBindings["bindings"].append((*subIt));
            }
        }
    }

    void SettingsMenuState::revertTentativeBindings()
    {
        //incase we're leaving a submenu whilst waiting on input, etc
        if(currentRequestCancelFunc != nullptr)
        {
            currentRequestCancelFunc();
        };
        tentativeBindings = previousBindings;

        //do the grouping
        tentativeBindingGroups.clear();
        for(auto it = tentativeBindings["bindings"].begin(); it != tentativeBindings["bindings"].end(); ++it)
        {
            tentativeBindingGroups[(*it)["target"]["event_type"].asString()].push_back(*it);
        }

        inputHandler.setBindings(tentativeBindings);
    }

    void SettingsMenuState::loadBindingSubMenu(std::string eventType)
    {
        std::map<sf::String, std::vector<Json::Value>>::iterator targetMapEntry;

        menu.clearItems();
        menu.addItem<SimpleTextButton>(true, defaultFont, "Back to Control Settings", [&](SimpleTextButton*){loadMainSettingsMenu();});
        tentativeBindingGroups[eventType]; //ensure it exists even if there are none of that type
        targetMapEntry = tentativeBindingGroups.find(eventType);
        if(eventType == "MenuInputEvent")
        {
            menu.addItem<NonFocusableMenuTextBox>(true,defaultFont, "Menu Controls").setFontSize(25);
        }
        else if(eventType == "CharacterInputEvent")
        {
            menu.addItem<NonFocusableMenuTextBox>(true,defaultFont, "Character Controls").setFontSize(25);
        }
        else if(eventType == "MiscInputEvent")
        {
            menu.addItem<NonFocusableMenuTextBox>(true,defaultFont, "Gamepad & Misc Settings").setFontSize(25);
        }

        menu.addItem<SimpleTextButton>(true, defaultFont, "Restore defaults",
        [&, targetMapEntry, eventType](SimpleTextButton* activatedButton)
        {
                std::ifstream bindingsFileStream(__DEFAULT_BINDING_FILE_PATH__);
                Json::Reader reader;
                Json::Value defaultBindings;
                try
                {
                    reader.parse(bindingsFileStream, defaultBindings, false);
                }
                catch(Json::RuntimeError err)
                {
                    std::cout << err.what();
                }
                //restore only the subcomponent
                (*targetMapEntry).second.clear();
                for(auto it = defaultBindings["bindings"].begin(); it != defaultBindings["bindings"].end(); ++it)
                {
                    //grab from the defaults only the ones of the current type
                    if((*it)["target"]["event_type"].asString() == (*targetMapEntry).first)
                    {
                        (*targetMapEntry).second.push_back(*it);
                    }
                }

                //if we're doing gamepad settings, grab the old sfml_specific_settings too
                if(eventType == "MiscInputEvent")
                {
                    tentativeBindings["sfml_specific_settings"] = defaultBindings["sfml_specific_settings"];
                }
                saveTentativeBindings();
                loadBindingSubMenu(eventType);
            }
        );
        menu.addItem<SimpleTextButton>(false, defaultFont, "Apply Changes (Warning: Does not save)",
            [&](SimpleTextButton*)
            {
                applyTentativeBindings();
            }
        );

        //also applies them
        menu.addItem<SimpleTextButton>(false, defaultFont, "Save Changes",
            [&](SimpleTextButton*)
            {
                saveTentativeBindings();
            }
        );
        sf::String eachBindingInitialLabel;
        //create the title and instructions objects.
        //need to this-> reference some things because the names are also params
        menu.addItem<NonFocusableMenuTextBox>(true, defaultFont, "Click on the buttons below then enter input to update the binding.\nNote that for axies (e.g. up/down or left/right on your joystick) the axis should be at least 50% in either direction in order to update the binding.");
        //populate the bindings buttons

        for(auto it = (*targetMapEntry).second.begin(); it != (*targetMapEntry).second.end(); ++it)
        {
            eachBindingInitialLabel = "";

            //kind of easier to keep the defaults of the constructor and just update the font size immediately
            //NOTE: Attaching display-names to the JSON is starting to look a little redundant right now..?
            menu.addItem<NonFocusableMenuTextBox>(true, defaultFont, ((*it)["target"].isMember("display_name") ? (*it)["target"]["display_name"].asString() : "Unknown or Invalid In-Context Event")).setFontSize(18);

            for(auto sourcesIt = (*it)["sources"].begin(); sourcesIt != (*it)["sources"].end(); ++sourcesIt)
            {
                if(!(*sourcesIt).isNull() && (*sourcesIt).isMember("display_name"))
                {
                    eachBindingInitialLabel = (*sourcesIt)["display_name"].asString();
                }
                else
                {
                    eachBindingInitialLabel = "Unknown";
                }
                // std::cout << (*sourcesIt) << " " << eachBindingInitialLabel.toAnsiString() << std::endl;
                //copy i by value so that each button corresponds to a specific binding by copying i's value at the time the lambda is created?
                menu.addItem<SimpleTextButton>(false, this->defaultFont, eachBindingInitialLabel,
                    [&, sourcesIt](SimpleTextButton* triggeredButton)
                    {
                        sf::String previousButtonString = triggeredButton->getString();
                        this->currentRequestCancelFunc = [previousButtonString, triggeredButton, this]
                        {
                            this->inputHandler.cancelInputKeyRequest();
                            triggeredButton->setString(previousButtonString);
                            this->currentRequestCancelFunc = nullptr;
                        };
                        triggeredButton->setString("[Waiting for input]");
                        this->inputHandler.receiveInputKeyRequest(std::shared_ptr<InputKeyRequest>(new BindingInputKeyRequest(
                            [&, triggeredButton, sourcesIt](const InputKey& key)
                            {
                                (*sourcesIt) = key.jsonify();
                                triggeredButton->setString(key.toString());
                                menu.updateGeometry();
                                this->currentRequestCancelFunc = nullptr;
                            }
                        )));
                        menu.updateGeometry();
                    }
                );
            }
        }
        if(eventType == "MiscInputEvent")
        {
            for(auto it = tentativeBindings["sfml_specific_settings"]["axis_deadzones"].begin(); it != tentativeBindings["sfml_specific_settings"]["axis_deadzones"].end(); ++it)
            {
                eachBindingInitialLabel = "Deadzone radius for: ";
                for(auto subIt = (*it)["axis_codes"].begin(); subIt != (*it)["axis_codes"].end(); ++subIt)
                {
                    if(subIt != (*it)["axis_codes"].begin())
                    {
                        eachBindingInitialLabel += " & ";
                    }
                    eachBindingInitialLabel += (*subIt).asString();
                }
                menu.addItem<NonFocusableMenuTextBox>(true, defaultFont, eachBindingInitialLabel);
                menu.addItem<AdjustableScale>(true, defaultFont, AdjustableScale::ValueAdjustmentModifiers(0.0, 1.0, 0.01), (*it)["radius"].asFloat(),
                    [&, it](AdjustableScale* scale)
                    {
                        (*it)["radius"] = scale->getValue();
                    }
                );
            }
        }
        isAtMainMenu = false;
    }

    void SettingsMenuState::applyTentativeBindings()
    {
        if(currentRequestCancelFunc != nullptr)
        {
            currentRequestCancelFunc();
        }
        updateTentativeBindings();
        inputHandler.setBindings(tentativeBindings);
    }

    void SettingsMenuState::saveTentativeBindings()
    {
        std::ofstream bindingsFileStream(__ACTIVE_BINDING_FILE_PATH__);
        Json::StyledWriter writer;
        if(currentRequestCancelFunc != nullptr)
        {
            currentRequestCancelFunc();
        }
        updateTentativeBindings();
        bindingsFileStream << writer.write(tentativeBindings);
        previousBindings = tentativeBindings;
        inputHandler.setBindings(tentativeBindings);
    }
}
