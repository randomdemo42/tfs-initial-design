/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "SimpleTextButton.hpp"
namespace __GAME_NAMESPACE__
{

	void SimpleTextButton::updateGeometry()
	{
		sf::FloatRect bounds = textBox.getBounds();
		containerBox.setSize(sf::Vector2f(bounds.width, bounds.height) + INNER_PADDING*2.f);
	}

	void SimpleTextButton::initialize(const sf::Vector2f& position)
	{
		containerBox.setOutlineThickness(DEFAULT_BORDER_WIDTH);
		containerBox.setFillColor(DEFAULT_BG_COLOR);
		containerBox.setOutlineColor(DEFAULT_BORDER_COLOR);
		updateGeometry();
		setPosition(position);
	}

	SimpleTextButton::SimpleTextButton(const sf::Font& font,  const sf::String& text, const std::function<void(SimpleTextButton*)>& activateFunc, const sf::Vector2f& position)
		: isPressedIn(false), textBox(font, text), containerBox(), activateFunc(activateFunc)
	{
		initialize(position);
	}
	SimpleTextButton::SimpleTextButton(const sf::Font& font, const sf::String& text, const sf::Vector2f& position)
		: isPressedIn(false), textBox(font, text), containerBox(), activateFunc([](SimpleTextButton*){})
	{
		initialize(position);
	}

	sf::FloatRect SimpleTextButton::getBounds() const
	{
		return containerBox.getGlobalBounds();
	}
	sf::Vector2f SimpleTextButton::getPosition() const
	{
		//innards.getPosition isn't related to the boundaries in sf::text
		//sf::FloatRect bounds = getBounds();
		//return sf::Vector2f(bounds.left, bounds.top);
		return containerBox.getPosition();
	}
	void SimpleTextButton::setPosition(const sf::Vector2f& position)
	{
		containerBox.setPosition(position);
		textBox.setPosition(position + INNER_PADDING); //minus the border vector since its negative
	}

	void SimpleTextButton::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		target.draw(containerBox, states);
		target.draw(textBox, states);

#if RENDER_DEBUGGING
		GUIObject::drawDebug(target, states);
#endif
	}

	void SimpleTextButton::onEvent(const MenuInputEvent& event)
	{
		switch (MenuInputEvent::SubType subType = event.subType)
		{
		case MenuInputEvent::Activate:
			activateFunc(this);
			break;
		}
	}

	void SimpleTextButton::onEvent(const MouseEvent& event)
	{
		switch (MouseEvent::SubType subType = event.subType)
		{
			case MouseEvent::ButtonPressed:
				if (event.button.button == sf::Mouse::Button::Left)
				{
					isPressedIn = true;
					activateFunc(this);
				}
				break;
			case MouseEvent::ButtonReleased:

				if (event.button.button != sf::Mouse::Button::Left || !isPressedIn)
				{
					break; //stop here, the previous press event has nothing to do with this button.
				}
				else
				{
					isPressedIn = false;

					//continue on to activation.
				}
		}
	}

	sf::String SimpleTextButton::getString() const
	{
		return textBox.getString();
	}
	void SimpleTextButton::setString(const sf::String& string)
	{
		textBox.setString(string);
		updateGeometry();
	}

	void SimpleTextButton::update()
	{}

	void SimpleTextButton::setActivateFunc(const std::function<void(SimpleTextButton*)>& activateFunc)
	{
		this->activateFunc = activateFunc;
	}

    bool SimpleTextButton::focusIfFocusable()
    {
		containerBox.setOutlineThickness(DEFAULT_HIGHLIGHT_WIDTH);
		containerBox.setOutlineColor(DEFAULT_FOCUS_COLOR);
        return isFocusable();
    }

    bool SimpleTextButton::blurIfFocusable()
    {
        containerBox.setOutlineThickness(DEFAULT_BORDER_WIDTH);
        containerBox.setOutlineColor(DEFAULT_BORDER_COLOR);
        if (isPressedIn)
        {
            isPressedIn = false;
        }
        return isFocusable();
    }

    bool SimpleTextButton::isFocusable()
    {
        return true;
    }
}
