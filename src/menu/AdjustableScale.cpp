/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "AdjustableScale.hpp"
namespace __GAME_NAMESPACE__
{
    AdjustableScale::ValueAdjustmentModifiers::ValueAdjustmentModifiers(float min, float max, float step): min(min), max(max), step(step)
    {}

    AdjustableScale::AdjustableScale(const sf::Font& font, const ValueAdjustmentModifiers& adjustments, float initialValue, const std::function<void(AdjustableScale*)>& onChangeFunc, const sf::Vector2f& position): adjustments(adjustments), leftHighlighted(false), rightHighlighted(false), valueText(font), leftArrow(3), rightArrow(3), onChangeFunc(onChangeFunc)
    {
        setValue(initialValue);
        updateValueText();
        leftArrow.setPoint(0, sf::Vector2f(30,0));
        leftArrow.setPoint(1, sf::Vector2f(0, 15));
        leftArrow.setPoint(2, sf::Vector2f(30, 30));
        rightArrow.setPoint(0, sf::Vector2f(0,0));
        rightArrow.setPoint(1, sf::Vector2f(30, 15));
        rightArrow.setPoint(2, sf::Vector2f(0, 30));
        leftArrow.setOutlineThickness(DEFAULT_BORDER_WIDTH);
		leftArrow.setFillColor(DEFAULT_BG_COLOR);
		leftArrow.setOutlineColor(DEFAULT_BORDER_COLOR);
        rightArrow.setOutlineThickness(DEFAULT_BORDER_WIDTH);
		rightArrow.setFillColor(DEFAULT_BG_COLOR);
		rightArrow.setOutlineColor(DEFAULT_BORDER_COLOR);
        setPosition(position);
    }

    bool AdjustableScale::focusIfFocusable()
    {
        if(!leftHighlighted)
        {
            leftHighlighted = true;
            leftArrow.setOutlineColor(DEFAULT_FOCUS_COLOR);
        }
        if(!rightHighlighted)
        {
            rightHighlighted = true;
            rightArrow.setOutlineColor(DEFAULT_FOCUS_COLOR);
        }
        return isFocusable();
    }

    bool AdjustableScale::blurIfFocusable()
    {
        if(leftHighlighted)
        {
            leftHighlighted = false;
            leftArrow.setOutlineColor(DEFAULT_BORDER_COLOR);
        }
        if(rightHighlighted)
        {
            rightHighlighted = false;
            rightArrow.setOutlineColor(DEFAULT_BORDER_COLOR);
        }
        return isFocusable();
    }

    bool AdjustableScale::isFocusable() const
    {
        return true;
    }

    float AdjustableScale::getValue() const
    {
        return value;
    }

    sf::Vector2f AdjustableScale::getPosition() const
    {
        return leftArrow.getPosition()-PADDING;
    }

    void AdjustableScale::setPosition(const sf::Vector2f& position)
    {
        sf::FloatRect eachBounds;
        leftArrow.setPosition(position+PADDING);
        eachBounds = leftArrow.getGlobalBounds();
        valueText.setPosition(sf::Vector2f(eachBounds.left + eachBounds.width + PADDING.x, position.y+PADDING.y));
        eachBounds = valueText.getBounds();
        rightArrow.setPosition(sf::Vector2f(eachBounds.left + eachBounds.width + PADDING.x, position.y+PADDING.y));
        //currently probably poorly positioned etc - will fix during testing
    }

    void AdjustableScale::setOnChangeFunc(const std::function<void(AdjustableScale*)>& newFunc)
    {
        onChangeFunc = newFunc;
    }

    const std::function<void(AdjustableScale*)>& AdjustableScale::getOnChangeFunc()
    {
        return onChangeFunc;
    }


    void AdjustableScale::updateValueText()
    {
        std::stringstream stream;
        sf::FloatRect eachBounds;
        stream << value;
        valueText.setString(stream.str());
        eachBounds = valueText.getBounds();
        rightArrow.setPosition(sf::Vector2f(eachBounds.left + eachBounds.width + PADDING.x, eachBounds.top));
    }

    void AdjustableScale::setValue(float value)
    {
        this->value = std::max(std::min(adjustments.max, value), adjustments.min);
        updateValueText();
    }

    void AdjustableScale::updateValue(float newValue)
    {
        setValue(newValue);
        if(onChangeFunc != nullptr)
        {
            onChangeFunc(this);
        }
    }

    void AdjustableScale::onEvent(const MenuInputEvent& event)
    {
        switch (MenuInputEvent::SubType _subType = event.subType) {
            case MenuInputEvent::SubType::Left:
                if(value > adjustments.min)
                {
                    updateValue(value-adjustments.step);
                }
                break;
            case MenuInputEvent::SubType::Right:
                if(value < adjustments.max)
                {
                    updateValue(value+adjustments.step);
                }
                break;
            case MenuInputEvent::SubType::Activate:
                if(leftHighlighted && !rightHighlighted)
                {
                    if(value > adjustments.min)
                    {
                        updateValue(value-adjustments.step);
                    }
                }
                else if(rightHighlighted && !leftHighlighted)
                {
                    if(value < adjustments.max)
                    {
                        updateValue(value+adjustments.step);
                    }
                }
                break;
        }
    }

    void AdjustableScale::onEvent(const MouseEvent& event)
    {
        sf::Vector2f mousePosition;
        sf::FloatRect eachBounds;
		switch(MouseEvent::SubType subType = event.subType)
		{
			case MouseEvent::SubType::Move:
                mousePosition = sf::Vector2f(event.move.x, event.move.y);
				break;
			case MouseEvent::SubType::ButtonPressed: //continue to next case without breaking.
			case MouseEvent::SubType::ButtonReleased:
				mousePosition = sf::Vector2f(event.button.x, event.button.y);
				break;
		}
        eachBounds = leftArrow.getGlobalBounds();
        if(eachBounds.contains(mousePosition))
        {
            if(!leftHighlighted)
            {
                leftHighlighted = true;
                leftArrow.setOutlineColor(DEFAULT_FOCUS_COLOR);
            }
        }
        else
        {
            if(leftHighlighted)
            {
                leftHighlighted = false;
                leftArrow.setOutlineColor(DEFAULT_BORDER_COLOR);
            }
        }
        eachBounds = rightArrow.getGlobalBounds();
        if(eachBounds.contains(mousePosition))
        {
            if(!rightHighlighted)
            {
                rightHighlighted = true;
                rightArrow.setOutlineColor(DEFAULT_FOCUS_COLOR);
            }
        }
        else if(rightHighlighted)
        {
            rightHighlighted = false;
            rightArrow.setOutlineColor(DEFAULT_BORDER_COLOR);
        }
        if(event.subType == MouseEvent::SubType::ButtonPressed)
        {
            if(leftHighlighted)
            {
                onEvent(MenuInputEvent(MenuInputEvent::SubType::Left));
            }
            else if(rightHighlighted)
            {
                onEvent(MenuInputEvent(MenuInputEvent::SubType::Right));
            }
        }
    }

    void AdjustableScale::update()
    {}

    void AdjustableScale::draw(sf::RenderTarget& target, sf::RenderStates states) const
    {
        target.draw(leftArrow, states);
        target.draw(valueText, states);
        target.draw(rightArrow, states);
        #if RENDER_DEBUGGING
        		GUIObject::drawDebug(target, states);
        #endif
    }

    sf::FloatRect AdjustableScale::getBounds() const
    {
        sf::FloatRect result, eachBounds;
        result = leftArrow.getGlobalBounds();
        eachBounds = rightArrow.getGlobalBounds();
        result.left -= PADDING.x;
        result.top -= PADDING.y;
        result.width = (eachBounds.left + eachBounds.width) - result.left + PADDING.x;
        result.height = (eachBounds.top + eachBounds.height) - result.top + PADDING.y;
        return result;
    }
}
