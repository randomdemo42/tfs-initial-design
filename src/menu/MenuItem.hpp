/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "../definitions.hpp"
#include "../gui/GUIObject.hpp"
#include "../input/MenuInputEvent.hpp"
#include "../input/MouseEvent.hpp"
#include "../event/EventListener.hpp"
namespace __GAME_NAMESPACE__
{
    //since some items can be in the menu for positioning reasons but should not be focusable
    //forcing menu items to 'listen' to menu input events even if they ignore it generally wouldn't impact on surrounding items, so yeah.

	//not sure if they might need to keep a reference to the parent Menu such as buttons that change the menu? prolly not, or specific classes that do can be given it.
	class MenuItem : public GUIObject, public EventListener<MenuInputEvent>, public EventListener<MouseEvent>
	{
    public:
        static const sf::Color DEFAULT_TEXT_COLOR;
        static const sf::Color DEFAULT_BG_COLOR;
        static const sf::Color DEFAULT_FOCUS_COLOR;
        static const sf::Color DEFAULT_BORDER_COLOR;
        static const sf::Vector2f INNER_PADDING; //storing it into a vector is actually the easier way to do things, especially with the constructor, even if both x and y are the same.
        static const float DEFAULT_HIGHLIGHT_WIDTH;
        static const float DEFAULT_BORDER_WIDTH;
        static const sf::Vector2f DEFAULT_HIGHLIGHT_VECTOR;
        static const sf::Vector2f PADDING;

		virtual void update() = 0;
		virtual void setPosition(const sf::Vector2f&) = 0;
		virtual ~MenuItem() = default;
        //returns true if the item is focusable, and the item will react to having focus (if reaction is neccessary as dictated by the item). otherwise (default), false will be returned
        virtual bool focusIfFocusable();

        //returns true if the item is focusable, and the item will react to losing focus (if reaction is neccessary as dictated by the item). otherwise (default), false will be returned
        virtual bool blurIfFocusable();

        virtual bool isFocusable() const;

        virtual void onEvent(const MenuInputEvent&);
        virtual void onEvent(const MouseEvent&);
	};

}
