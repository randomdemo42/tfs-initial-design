/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "MenuItem.hpp"
#include "../gui/TextBox.hpp"
#include "../input/MouseEvent.hpp"
#include "../input/MenuInputEvent.hpp"
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <functional>
namespace __GAME_NAMESPACE__
{
	class SimpleTextButton :
		public MenuItem
	{
	protected:

		virtual void draw(sf::RenderTarget&, sf::RenderStates) const;
	private:
		bool isPressedIn; //whether or not the button is currently being pressed down on.
		TextBox textBox;
		sf::RectangleShape containerBox;
		bool focused;
		std::function<void(SimpleTextButton*)> activateFunc;

		//updates geometry as neccesary to adjust to changes that may have occured internally. (to be called by relevant internal methods only)
		void updateGeometry();

		//to be called by (and only by) constructors
		void initialize(const sf::Vector2f& position);
	public:
		/*
			the function for the button to execute when activated can be defined in one of 2 ways:
			1. by supplying an std::function, for derived classes, or
			2. overloading the virtual onActivation() method, which is used if the constructor without an activateFunc is chosen.

			TODO: the second is probably extremely niche, consider optimisding by removing and forcing affected classes to produce such behaviour on their own?

            NOTE: the signature of the activateFunc has changed so that it takes a reference to the activated button as a parameter (with non-const, non-friendly access e.g. public only access)
		*/
		SimpleTextButton(const sf::Font&, const sf::String&, const std::function<void(SimpleTextButton*)>& activateFunc, const sf::Vector2f& position = sf::Vector2f(0, 0));
		SimpleTextButton(const sf::Font&, const sf::String&, const sf::Vector2f& position = sf::Vector2f(0, 0));
		virtual sf::FloatRect getBounds() const;
		virtual void setPosition(const sf::Vector2f&);
		virtual void onEvent(const MenuInputEvent&);
		virtual void onEvent(const MouseEvent&);

        virtual bool focusIfFocusable();

        virtual bool blurIfFocusable();

        virtual bool isFocusable();

		virtual void update();
		virtual ~SimpleTextButton() = default;
		virtual sf::Vector2f getPosition() const;

		virtual void setActivateFunc(const std::function<void(SimpleTextButton*)>& activateFunc);
		virtual sf::String getString() const;

		virtual void setString(const sf::String&);

	};

}
