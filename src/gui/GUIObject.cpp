/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "GUIObject.hpp"
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
namespace __GAME_NAMESPACE__
{
#if RENDER_DEBUGGING
	void GUIObject::drawDebug(sf::RenderTarget& target, sf::RenderStates states) const
	{
		sf::FloatRect bounds = getBounds();
		sf::RectangleShape visualBoundary(sf::Vector2f(bounds.width, bounds.height));
		visualBoundary.setPosition(sf::Vector2f(bounds.left, bounds.top));
		visualBoundary.setFillColor(sf::Color::Transparent);
		visualBoundary.setOutlineColor(sf::Color(255, 0, 0, 255));
		visualBoundary.setOutlineThickness(1.f);
		target.draw(visualBoundary, states);

		sf::RectangleShape visualPos = sf::RectangleShape(sf::Vector2f(3, 3));
		visualPos.setFillColor(sf::Color(0, 0, 255, 255));
		visualPos.setOutlineColor(sf::Color(0, 0, 255, 255));
		visualPos.setPosition(getPosition());
		target.draw(visualPos, states);
	}
#endif
	GUIObject::~GUIObject(){};
}
