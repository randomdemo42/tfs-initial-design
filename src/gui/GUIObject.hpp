/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "../definitions.hpp"
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Color.hpp>
namespace __GAME_NAMESPACE__
{
	class GUIObject : public sf::Drawable
	{
	public:
		/*
		typedef Dimensions dimensions_type;
		typedef Position position_type;
		*/
	private:
		/*
		Dimensions dims;
		Position pos;
		*/
	protected:
		virtual void draw(sf::RenderTarget&, sf::RenderStates) const = 0;

#if RENDER_DEBUGGING
		void drawDebug(sf::RenderTarget&, sf::RenderStates) const;
#endif
	public:
		/*
		virtual Dimensions getSize() const = 0;
		virtual Position getPosition() const = 0;
		*/
		virtual sf::FloatRect getBounds() const = 0;
		virtual sf::Vector2f getPosition() const = 0;
		virtual ~GUIObject() = 0;

		//TODO: make setPosition() and Move() part of this interface?; Considering making this a full implimentation of transformable? (probably won't)
	};
}
