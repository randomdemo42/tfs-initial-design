/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "TextBox.hpp"
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
namespace __GAME_NAMESPACE__
{
	const FontSize TextBox::DEFAULT_FONT_SIZE = 15;
	const sf::Vector2f TextBox::PADDING = sf::Vector2f(5.f, 5.f);
	const sf::Color TextBox::DEFAULT_COLOR = sf::Color(255,255,255,255); //note that there is no copy constructor for color?

	//NOTE: sf::text seems to position really strangely, this class will attempt to offset these issues, probably should be checked every now and then.

	TextBox::TextBox(const sf::Font& font, const sf::String& string, const sf::Vector2f& position, const sf::Color& color, FontSize fontSize)
	{
		innards.setCharacterSize(fontSize);
		innards.setString(string);
		setPosition(position);

		innards.setFont(font);
		innards.setColor(color);
		updateGeometry();
	}

	sf::String TextBox::getString() const
	{
		return innards.getString();
	}

	void TextBox::setString(const sf::String& string)
	{

		//a very small change occurs, most efficient way seems to be to offset the origin of the internal object to offset the skewed locabounds?
		innards.setString(string);
		updateGeometry();
	}

	FontSize TextBox::getFontSize() const
	{
		return innards.getCharacterSize();
	}

	void TextBox::setFontSize(FontSize fontSize)
	{
		innards.setCharacterSize(fontSize);
		updateGeometry();
	}

	void TextBox::setPosition(const sf::Vector2f& position)
	{
		innards.setPosition(position+PADDING);
	}

	/*
	void TextBox::accept(GUIVisitor& v)
	{
		v.visit(*this);
	}
	*/

	void TextBox::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		target.draw(innards, states);
#if RENDER_DEBUGGING
		GUIObject::drawDebug(target, states);
#endif
	}

	sf::FloatRect TextBox::getBounds() const
	{

		sf::FloatRect globalBounds = innards.getGlobalBounds();

		return sf::FloatRect(sf::Vector2f(globalBounds.left, globalBounds.top)-PADDING, sf::Vector2f(globalBounds.width, globalBounds.height) + (PADDING*2.f));
	}

	sf::Vector2f TextBox::getPosition() const
	{
		//innards.getPosition isn't related to the boundaries in sf::text
		sf::FloatRect bounds = getBounds();
		return sf::Vector2f(bounds.left, bounds.top);
	}

	void TextBox::updateGeometry()
	{
		//the local bounds don't match the global bounds, the local seems to be more closely related to size of specific characters etc. shift the origin to offset this

		sf::FloatRect localBounds = innards.getLocalBounds();
		innards.setOrigin(sf::Vector2f(localBounds.left, localBounds.top)); //REMEMBER THIS OFFSET AGAINST THE BOUNDS
	}

	sf::Color TextBox::getColor()
	{
		return innards.getColor();
	}

	void TextBox::setColor(const sf::Color& color)
	{
		innards.setColor(color);
	}
}
