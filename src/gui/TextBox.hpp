/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "../definitions.hpp"
#include "GUIObject.hpp"
#include "../Text.hpp"
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Font.hpp>
namespace __GAME_NAMESPACE__
{

	//include move constructor etc later


	//basic class for now that can be expanded with more customisation later, e.g. ability to set colour etc, but still keeping the option to settle for defaults like this? Might also redundant wrapping level?
	//worry about that a little later once input event handling is ready?
	//Pretty sure we still want to wrap our GUI interface objects around underlying stuff purely due to the custom behaviour we want?
	class TextBox :
		public GUIObject
	{
	public:
		static const FontSize DEFAULT_FONT_SIZE;
		static const sf::Vector2f PADDING;
		static const sf::Color DEFAULT_COLOR;
	private:
		sf::Text innards;
		//updates geometry as neccesary to adjust to changes that may have occured internally. (to be called by relevant internal methods only)
		void updateGeometry();
	protected:

		//static const sf::Font getDefaultFont();
		virtual void draw(sf::RenderTarget&, sf::RenderStates) const;
	public:
		TextBox(const sf::Font&, const sf::String& string = "", const sf::Vector2f& position = sf::Vector2f(0, 0), const sf::Color& = DEFAULT_COLOR, FontSize = DEFAULT_FONT_SIZE);
		virtual sf::String getString() const;
		virtual void setString(const sf::String&);
		FontSize getFontSize() const;
		void setFontSize(FontSize);
		virtual void setPosition(const sf::Vector2f&);
		//virtual void accept(GUIVisitor&);
		virtual sf::FloatRect getBounds() const;
		virtual sf::Vector2f getPosition() const;

		sf::Color getColor();
		void setColor(const sf::Color&);

		virtual ~TextBox() = default;
	};
}
