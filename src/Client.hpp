/*
Copyright (C) 2016 Marcos Erich Cousens-Schulz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "definitions.hpp"
#include <SFML/Window/Window.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include "state/StateManager.hpp"
#include "state/State.hpp"
#include "state/PrimaryStateManager.hpp"
#include "menu/MainMenuState.hpp"
#include "console/GUIConsole.hpp"
#include "event/EventListener.hpp"
#include "input/MenuInputEvent.hpp"
#include "input/TextInputEvent.hpp"
#include "input/CentralInputHandler.hpp"
namespace __GAME_NAMESPACE__
{
	//only create one of these classes per program-instance, not neccessarily because it will break, but because it's not intended to be created multiple times,
	//however as such, there is no garauntee that it will not break

	//TODO: updating the string for a button changes it's size, gotta fix that properly somehow in the future?
	class Client: public PrimaryStateManager
	{
    private:
		bool running;
		sf::RenderWindow mainWindow;
		sf::Font defaultFont; //we need to have the one font stored here and pass it around due to issues with global fonts causing everything to crash haha, need to pass through a const reference to States etc too.
		GUIConsole console;
		GUIConsole::CommandSet globalCommands;
		CentralInputHandler centralInputHandler;
        SFMLEventHandler sfmlEventHandler;


		void exitApplication(); //differs from stop in that this is only called within the run function, etc, and is likely to involve stuff like saving preferences, etc.

		void processEvents(sf::Window& targWindow);
		void render();

        virtual void onEvent(const CharacterInputEvent&);
        virtual void onEvent(const MouseEvent&);
        virtual void onEvent(const MenuInputEvent&);
        virtual void onEvent(const MiscInputEvent&);
        virtual void onEvent(const TextInputEvent&);
	public:
		virtual void draw(sf::RenderTarget&, sf::RenderStates) const;
		virtual void update();
		Client();
		Client(const Client&) = delete;
		Client& operator= (const Client&) = delete;
		~Client();
		void run();
		virtual void quit();
	};
}
