#ifndef TSF_CLI
#define TSF_CLI
#include <vector>
#include <algorithm>
#include <utility>
#include <string>
#include <map>
#include <array>
#include <functional>
namespace client
{
	/*
		Does not alert to the failure of a command by throwing an error,
		or with any return values but rather only by adding a message to the end of the history.
		In fact almost all feedback given by this class is simply via the history.
		Because it is purely to be a human interface?


		NOTE: example on input distinguishing input in the history is to be preceded by a >. all outputs should instead precede with a space when logged normally for constrast/reference.
	*/

	//invariants: has a log which contains all input and output, an input history, and a 'current' input line. these invariants are for i/o purposes.
	class CLI {
	public:
		typedef std::vector<std::string> log_type;
		typedef std::map<std::string, std::function<void(std::string const &)> > commandSet_type;
	private:
		typedef std::vector<std::string> history_type;
		log_type log; //only edit internally, but access freely externally?
		history_type inputHistory;
		history_type::const_iterator inputHistoryIterator; //the current position in the input history.
		std::string tempLastInput;
		std::string input; //the line for the current input.
		
		/*
			pre: none.
			post: the inputHistoryPosition will be moved one closer to the beginning and the content at that new position will be set to the current input
			note: if the initial inputHistoryPosition is at the beginning of the inputHistory container, the position will be set to one position past the end and the current tempLastInput will be used as the new current input.
				in all other cases if the data in the container at the current inputHistoryPosition does not match the current input value, the current value will be stored in the temporary input.
		*/
		void ascendHistory();
		
		/*
			pre: none
			post: the inputHistoryPosition will be moved one closer to the end and the content at that new position will be set to the current input
			note: if the initial inputHistoryPosition is after the end of the inputHistory container nothing will happen, else:
				if the data in the container at the current inputHistoryPosition does not match the current input value, the current value will be stored in the temporary input.
				if the resulting position is after the end, the current temporary input will be used as the new current input.
		*/
		void descendHistory();
	protected:
		/*
		probs easier to write a simple html doc up for a command list/descriptions then mess around with internally outputting it in game since its only for us?
		Less overhead?
		*/
		commandSet_type commands;

	public:
		
		//default constructor.
		CLI();

		/*
			returns a reference to the content of the current input line.
			pre-condition: none.
		*/
		const std::string& getInput()  const;

		/*
			pre: none
			post: the current input line will be updated to match the provided argument
		*/
		void setInput(const std::string&);

		/*
			pre: none
			Adds the supplied message to the log (does not appear like user input)
		*/
		void logMessage(const std::string&);

		/*
			pre: none
			post: an attempt will be made to process the current input line as a command with arguments. The line itself and the result will be appended to the history (input will be ommited if empty), and both input and tempLastInput will be cleared.
			note: the user input line should contain something that seperates it from non-input
		
		*/
		void processInput();

		/*
			pre: none
			post: produces a reference to the log of input and output, which can then be displayed.
		*/
		const log_type& getLog() const;
		
		//virtual destructors ftw
		virtual ~CLI();
	};
}

#endif