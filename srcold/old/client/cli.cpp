#include "cli.hpp"
namespace client
{

	CLI::CLI() : inputHistoryIterator(inputHistory.cbegin())
	{

	}

	void CLI::ascendHistory()
	{
		if (inputHistoryIterator == inputHistory.cbegin())
		{
			inputHistoryIterator = inputHistory.cend();
			input = tempLastInput;
		}
		else
		{
			if ((*inputHistoryIterator) != input)
			{
				tempLastInput = input;
			}

			inputHistoryIterator--;
			input = *inputHistoryIterator;
		}
	}

	void CLI::descendHistory()
	{
		if (inputHistoryIterator == inputHistory.cend())
		{
			return; //do nothing
		}
		else
		{
			if ((*inputHistoryIterator) != input)
			{
				tempLastInput = input;
			}

			inputHistoryIterator++;
			if (inputHistoryIterator == inputHistory.cend())
			{
				input = tempLastInput;
			}
		}
	}

	const std::string& CLI::getInput() const
	{
		return input;
	}

	void CLI::setInput(const std::string& input)
	{
		this->input = input;
	}
	
	const CLI::log_type& CLI::getLog() const
	{
		return log;
	}

	void CLI::logMessage(const std::string& msg)
	{
		logMessage(msg);
	}

	void CLI::processInput()
	{
		std::string::size_type first_space = input.find_first_of(' ');
		std::string commandStr = input.substr(0, first_space);
		commandSet_type::const_iterator commandIt;
		std::string args;

		inputHistory.push_back(input);
		logMessage(">" + input.data);

		commandIt = commands.find(commandStr);
		if (commandIt == commands.end())
		{
			logMessage("Error: Invalid command \"" + commandStr + "\"");
		}
		else
		{
			if (first_space != std::string::npos)
			{
				args = input.substr(first_space + 1); //else defaults already to empty string
			}

			(commandIt->second)(args);
		}
		
		input.clear();
		tempLastInput.clear();

	}

	CLI::~CLI(){}; //does nothing atm
}