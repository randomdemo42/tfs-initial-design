#ifndef TSF_TICK_LISTENER
#define TFS_TICK_LISTENER
namespace client
{
	//does stuff on timed ticks.
	class TickListener
	{
	public:
		virtual void tick() = 0;
		virtual ~TickListener() = 0;
	};
}
#endif