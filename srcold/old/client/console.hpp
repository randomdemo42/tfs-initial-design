#ifndef TSF_CONSOLE
#define TSF_CONSOLE
#include "cli.hpp"
#include "renderable.hpp"
namespace client
{
	//may merge with CLI, idk. having a seperate CLI might be useful for the server? IDK
	class Console : public CLI, public TickListener, public Renderable
	{
		bool _open;

		//commands, stored privately?

		//these are just test commands

		/*
		just appends all of the provided argument to the log.
		*/
		void repeat(const std::string&);

	public:
		
		Console();
		/*
		Show and enable the console
		pre-condition: none
		post-condition: the console will be in the "open" state.
		*/
		void open();

		/*
		Hide and disable the console.
		pre-condition: none
		post-condition: the console will be in the "closed" state.

		//also a lower method possibly so allow
		*/
		void close();

		void tick();
		void render();
	} console;
}
#endif