#ifndef TSF_RENDERABLE
#define TSF_RENDERABLE
namespace client
{
	//has a function for rendering. possibly also in future must have a render precedance order for layering?
	class Renderable
	{
	public:
		virtual void render() = 0;
		virtual ~Renderable() = 0;
	};
}
#endif